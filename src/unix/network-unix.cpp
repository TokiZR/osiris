#include <stdint.h>
#include <stddef.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "osiris/exception.h"
#include "osiris/string.h"
#include "osiris/poll.h"
#include "osiris/object.h"
#include "osiris/stream.h"
#include "osiris/logging.h"
#include "osiris/network.h"

using namespace osiris;

/******************************************************************************
 *
 *	Auxiliary classes and functions.
 *
 */
class SocketStream: public InputStream, public OutputStream {
private:
	int sockfd;

public:
	SocketStream(int fd) :
			sockfd(fd) {
	}

	virtual ssize_t write(const uint8_t * data, size_t dlength) throw (exception&) {
#ifndef __APPLE__
		ssize_t result = ::send(sockfd, data, dlength, MSG_NOSIGNAL);
#else
		ssize_t result = ::write(sockfd, data, dlength);
#endif

		if (result < 0) {
			switch (errno) {
				case EBADFD:
					case EPIPE:
					throw NetworkException(NetworkException::Closed);
				case ECONNRESET:
					throw NetworkException(NetworkException::Reset);
				case EDESTADDRREQ:
					case ENOTCONN:
					throw NetworkException(NetworkException::Disconnected);
				default:
					networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno,
							strerror(errno));
					throw NetworkException(NetworkException::Unknown);
			}
		}

		return result;
	}

	virtual void flush() throw (exception) {
	}

	virtual void close() {
	}

	virtual ssize_t read(uint8_t * buffer, size_t buffsize) throw (exception&) {
#ifndef __APPLE__
		ssize_t result = ::recv(sockfd, buffer, buffsize, MSG_NOSIGNAL);
#else
		ssize_t result = ::read(sockfd, buffer, buffsize);
#endif

		if (result < 0) {
			switch (errno) {
				case EBADFD:
					case ECONNREFUSED:
					case EPIPE:
					throw NetworkException(NetworkException::Closed);
				case ECONNRESET:
					throw NetworkException(NetworkException::Reset);
				case EDESTADDRREQ:
					case ENOTCONN:
					throw NetworkException(NetworkException::Disconnected);
				default:
					networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno,
							strerror(errno));
					throw NetworkException(NetworkException::Unknown);
			}
		}

		return result;
	}
};

static sockaddr_info sockaddrForAddressType(const SocketAddress * addr) {
	sockaddr_info sf;

	if (dynamic_cast<const Inet4SocketAddress *>(addr)) {
		sockaddr_in * ad = (sockaddr_in *) malloc(sizeof(sockaddr_in));
		memset(ad, 0, sizeof(sockaddr_in));

		sf.sa = (sockaddr*) ad;
		sf.size = sizeof(sockaddr_in);
	} else if (dynamic_cast<const Inet6SocketAddress *>(addr)) {
		sockaddr_in6 * ad = (sockaddr_in6 *) malloc(sizeof(sockaddr_in6));
		memset(ad, 0, sizeof(sockaddr_in6));

		sf.sa = (sockaddr*) ad;
		sf.size = sizeof(sockaddr_in6);

	} else if (dynamic_cast<const UnixSocketAddress *>(addr)) {
		sockaddr_un * us = (sockaddr_un*) malloc(sizeof(sockaddr_un));
		memset(us, 0, sizeof(sockaddr_un));

		sf.sa = (sockaddr*) us;
		sf.size = sizeof(sockaddr_un);
	}

	return sf;
}

static sockaddr_info sockaddrForAddress(const SocketAddress * addr) {
	sockaddr_info sf;

	if (dynamic_cast<const Inet4SocketAddress *>(addr)) {
		const Inet4SocketAddress * iaddr = static_cast<const Inet4SocketAddress *>(addr);
		sockaddr_in * ad = (sockaddr_in *) malloc(sizeof(sockaddr_in));
		memset(ad, 0, sizeof(sockaddr_in));
		ad->sin_family = AF_INET;
		ad->sin_port = htons(iaddr->port);
		ad->sin_addr.s_addr = htonl(iaddr->addr.getAddress());

		sf.sa = (sockaddr*) ad;
		sf.size = sizeof(sockaddr_in);
	} else if (dynamic_cast<const Inet6SocketAddress *>(addr)) {
		const Inet6SocketAddress * iaddr = static_cast<const Inet6SocketAddress *>(addr);
		sockaddr_in6 * ad = (sockaddr_in6 *) malloc(sizeof(sockaddr_in6));
		memset(ad, 0, sizeof(sockaddr_in6));
		ad->sin6_family = AF_INET6;
		ad->sin6_port = htons(iaddr->port);

		in6addr addr = iaddr->addr.getAddress();
		memcpy((void *) &ad->sin6_addr, (void *) &addr, 16);
		sf.sa = (sockaddr*) ad;
		sf.size = sizeof(sockaddr_in6);

	} else if (dynamic_cast<const UnixSocketAddress *>(addr)) {
		const UnixSocketAddress * ua = static_cast<const UnixSocketAddress *>(addr);
		sockaddr_un * us = (sockaddr_un*) malloc(sizeof(sockaddr_un));
		memset(us, 0, sizeof(sockaddr_un));

		us->sun_family = AF_UNIX;
		memcpy((void *) us->sun_path, (void *) ua->getPath().data(), ua->getPath().length());

		sf.sa = (sockaddr *) us;
		sf.size = sizeof(sockaddr_un);
	}

	return sf;
}

static SocketAddress * addressForSockaddr(const sockaddr_info * info) {
	switch (info->sa->sa_family) {
		case AF_INET: {
			sockaddr_in * sin = (sockaddr_in *) info->sa;
			return new Inet4SocketAddress(InetAddress4(ntohl(sin->sin_addr.s_addr)), ntohs(sin->sin_port));
		}
		case AF_INET6: {
			sockaddr_in6 * sin6 = (sockaddr_in6 *) info->sa;
			return new Inet6SocketAddress(InetAddress6(in6addr(sin6->sin6_addr)), ntohs(sin6->sin6_port));
		}
		case AF_UNIX: {
			sockaddr_un * su = (sockaddr_un *) info->sa;
			return new UnixSocketAddress(su->sun_path);
		}
	}

	return NULL;
}

static void copyAddresFromSockaddr(SocketAddress * s, const sockaddr_info * info) {
	switch (info->sa->sa_family) {
		case AF_INET: {
			sockaddr_in * sin = (sockaddr_in *) info->sa;
			Inet4SocketAddress * asin = static_cast<Inet4SocketAddress *>(s);
			asin->addr.setAddress(sin->sin_addr.s_addr);
			asin->port = sin->sin_port;
		}
			break;
		case AF_INET6: {
			sockaddr_in6 * sin6 = (sockaddr_in6 *) info->sa;
			Inet6SocketAddress * asin6 = static_cast<Inet6SocketAddress *>(s);
			asin6->addr.setAddress(sin6->sin6_addr);
			asin6->port = sin6->sin6_port;
		}
			break;
		case AF_UNIX: {
			sockaddr_un * su = (sockaddr_un *) info->sa;
			UnixSocketAddress * asu = static_cast<UnixSocketAddress *>(s);

			asu->setPath(su->sun_path);
		}
			break;
	}
}

/******************************************************************************
 *
 *	Inet Address v4
 *
 */
osiris::InetAddress4::InetAddress4(const_string addr) {
	struct in_addr inaddr;
	int ret = inet_pton(AF_INET, addr, (void *) &inaddr);

	if (ret != 1) {
		throw NetworkException(NetworkException::InvalidAddress);
	}

	this->addr = inaddr.s_addr;
}

size_t osiris::InetAddress4::hashValue() const {
	return std::hash<uint32_t>()(this->addr);
}

std::string osiris::InetAddress4::toString() const {
	std::string s;
	s.resize(INET_ADDRSTRLEN);

	inet_ntop(AF_INET, (void *) &this->addr, &s[0], INET_ADDRSTRLEN);

	return s;
}

void osiris::InetAddress4::toString(OutputStream& stream) const {
	char buff[INET_ADDRSTRLEN] = {};

	inet_ntop(AF_INET, (void *) &this->addr, buff, INET_ADDRSTRLEN);

	stream.puts(buff);
}

const InetAddress4& osiris::InetAddress4::Any() {
	static InetAddress4 in4addr_any(INADDR_ANY);

	return in4addr_any;
}

const InetAddress4& osiris::InetAddress4::Loopback() {
	static InetAddress4 in4addr_lo(INADDR_LOOPBACK);
	return in4addr_lo;
}

/******************************************************************************
 *
 *	Inet Address v6
 *
 */
osiris::InetAddress6::InetAddress6(const_string addr) {
	struct in6_addr inaddr;
	int ret = inet_pton(AF_INET6, addr, (void *) &inaddr);

	if (ret != 1) {
		throw NetworkException(NetworkException::InvalidAddress);
	}

	memcpy((void *) &this->addr, (void *) &inaddr, 16);
}

size_t osiris::InetAddress6::hashValue() const {
	return std::_Hash_bytes((void *) &this->addr, sizeof(this->addr), 1073741827);
}

std::string osiris::InetAddress6::toString() const {
	std::string s;
	s.resize(INET6_ADDRSTRLEN);

	inet_ntop(AF_INET6, (void *) &this->addr, &s[0], INET6_ADDRSTRLEN);

	return s;
}

void osiris::InetAddress6::toString(OutputStream& stream) const {
	char buff[INET6_ADDRSTRLEN] = {};

	inet_ntop(AF_INET6, (void *) &this->addr, buff, INET6_ADDRSTRLEN);

	stream.puts(buff);
}

const InetAddress6& osiris::InetAddress6::Any() {
	static InetAddress6 in6_addr_any((in6addr(in6addr_any)));
	return in6_addr_any;
}

const InetAddress6& osiris::InetAddress6::Loopback() {
	static InetAddress6 in6_addr_lo((in6addr(in6addr_loopback)));
	return in6_addr_lo;
}

/******************************************************************************
 *
 *	Unix Socket Address
 *
 */

size_t osiris::UnixSocketAddress::hashValue() const {
	return std::hash<std::string>()(this->pathName);
}

std::string osiris::UnixSocketAddress::toString() const {
	return this->pathName;
}

void osiris::UnixSocketAddress::toString(OutputStream& stream) const {
	stream.puts(this->pathName);
}

/******************************************************************************
 *
 *	Stream Socket
 *
 */

osiris::Socket::Socket(SocketAddress* local, SocketAddress* remote) :
		localAddr(local), remoteAddr(remote) {
}

osiris::Socket::Socket(const SocketAddress* addr) throw (NetworkException) :
		remoteAddr(NULL) {

	if (addr == NULL) {
		throw NetworkException(NetworkException::InvalidAddress);
	}

	localAddr = static_cast<SocketAddress*>(addr->clone());

	int stype = -1;
	if (dynamic_cast<const Inet4SocketAddress *>(addr)) {
		stype = AF_INET;
	} else if (dynamic_cast<const Inet6SocketAddress *>(addr)) {
		stype = AF_INET6;
	} else if (dynamic_cast<const UnixSocketAddress *>(addr)) {
		stype = AF_UNIX;
	}

	if (stype < 0) {
		throw NetworkException(NetworkException::Unsupported);
	}

	sockfd = ::socket(stype, SOCK_STREAM, 0);
#ifdef __APPLE__
	int set = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
#endif

	if (sockfd < 0) {
		switch (errno) {
			case EACCES:
				throw NetworkException(NetworkException::PermissionDenied);
			case EAFNOSUPPORT:
				case EINVAL:
				case EPROTONOSUPPORT:
				throw NetworkException(NetworkException::Unsupported);
			case ENOBUFS:
				case ENOMEM:
				case ENFILE:
				case EMFILE:
				throw NetworkException(NetworkException::Exhausted);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno, strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	this->sockaddr = sockaddrForAddress(addr);
	if (::bind(sockfd, this->sockaddr.sa, this->sockaddr.size) < 0) {
		switch (errno) {
			case EACCES:
				case EADDRNOTAVAIL:
				case EROFS:
				throw NetworkException(NetworkException::PermissionDenied);
			case EADDRINUSE:
				throw NetworkException(NetworkException::AddrInUse);
			case ELOOP:
				case ENOENT:
				case ENOTDIR:
				throw NetworkException(NetworkException::InvalidPath);
			case ENOMEM:
				throw NetworkException(NetworkException::Exhausted);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno, strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	getsockname(sockfd, this->sockaddr.sa, &this->sockaddr.size);

	copyAddresFromSockaddr(localAddr, &this->sockaddr);

	this->sstream = new SocketStream(sockfd);
}

osiris::Socket::~Socket() {
	delete localAddr;
	if (remoteAddr) delete remoteAddr;
	close();
	delete sstream;
}

bool osiris::Socket::connect(const SocketAddress * addr) throw (NetworkException) {
	if (sockfd < 0) {
		throw NetworkException(NetworkException::Closed);
	}

	if (remoteAddr) {
		delete remoteAddr;
		disconnect();
	}

	sockaddr_info sif = sockaddrForAddress(addr);
	int result = ::connect(sockfd, sif.sa, sif.size);

	if (result < 0) {
		switch (errno) {
			case EACCES:
				case EPERM:
				throw NetworkException(NetworkException::PermissionDenied);
			case EADDRINUSE:
				throw NetworkException(NetworkException::AddrInUse);
			case ECONNREFUSED:
				return false;
			case ENETUNREACH:
				throw NetworkException(NetworkException::Unreachable);
			case ETIMEDOUT:
				throw NetworkException(NetworkException::Timeout);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno, strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	if (this->remoteAddr == NULL) {
		this->remoteAddr = static_cast<SocketAddress *>(addr->clone());
	}

	return true;
}

bool osiris::Socket::isConnected() const {
	return this->remoteAddr != NULL;
}

InputStream* osiris::Socket::getInputStream() const {
	if (sockfd < 0) {
		return NULL;
	} else {
		return sstream;
	}
}

OutputStream* osiris::Socket::getOutputStream() const {
	if (sockfd < 0) {
		return NULL;
	} else {
		return sstream;
	}
}

void osiris::Socket::close() {
	if (sockfd >= 0) {
		disconnect();
		::close(sockfd);
		sockfd = -1;
	}
}

void osiris::Socket::disconnect() {
	if (sockfd >= 0 && this->remoteAddr) {
		::shutdown(sockfd, SHUT_RDWR);
		delete remoteAddr;
		this->remoteAddr = NULL;
	}
}

/******************************************************************************
 *
 *	Server Socket-
 *
 */

osiris::ServerSocket::ServerSocket(const SocketAddress* addr, uint32_t queueSize)
		throw (NetworkException) {

	if (addr == NULL) {
		throw NetworkException(NetworkException::InvalidAddress);
	}

	boundAddr = static_cast<SocketAddress*>(addr->clone());

	int stype = -1;
	if (dynamic_cast<const Inet4SocketAddress *>(addr)) {
		stype = AF_INET;
	} else if (dynamic_cast<const Inet6SocketAddress *>(addr)) {
		stype = AF_INET6;
	} else if (dynamic_cast<const UnixSocketAddress *>(addr)) {
		stype = AF_UNIX;
	}

	if (stype < 0) {
		throw NetworkException(NetworkException::Unsupported);
	}

	sockfd = ::socket(stype, SOCK_STREAM, 0);
#ifdef __APPLE__
	int set = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
#endif

	if (sockfd < 0) {
		switch (errno) {
			case EACCES:
				throw NetworkException(NetworkException::PermissionDenied);
			case EAFNOSUPPORT:
				case EINVAL:
				case EPROTONOSUPPORT:
				throw NetworkException(NetworkException::Unsupported);
				break;
			case ENOBUFS:
				case ENOMEM:
				case ENFILE:
				case EMFILE:
				throw NetworkException(NetworkException::Exhausted);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno, strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	this->sockaddr = sockaddrForAddress(addr);
	if (::bind(sockfd, this->sockaddr.sa, this->sockaddr.size) < 0) {
		switch (errno) {
			case EACCES:
				case EADDRNOTAVAIL:
				case EROFS:
				throw NetworkException(NetworkException::PermissionDenied);
			case EADDRINUSE:
				throw NetworkException(NetworkException::AddrInUse);
			case ELOOP:
				case ENOENT:
				case ENOTDIR:
				throw NetworkException(NetworkException::InvalidPath);
			case ENOMEM:
				throw NetworkException(NetworkException::Exhausted);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno, strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	getsockname(sockfd, this->sockaddr.sa, &this->sockaddr.size);

	copyAddresFromSockaddr(boundAddr, &this->sockaddr);

	if (::listen(sockfd, queueSize) < 0) {
		networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno, strerror(errno));
		throw NetworkException(NetworkException::Unknown);
	}

}

osiris::ServerSocket::~ServerSocket() {
	close();
	delete boundAddr;
}

Socket * osiris::ServerSocket::accept() const throw (NetworkException) {
	sockaddr_info sif = sockaddrForAddressType(boundAddr);

	int nsfd = ::accept(sockfd, sif.sa, &sif.size);

	if (nsfd < 0) {
		switch (errno) {
			case ECONNABORTED:
				case EINTR:
				return NULL;
			case ENOBUFS:
				case ENOMEM:
				case ENFILE:
				case EMFILE:
				throw NetworkException(NetworkException::Exhausted);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno, strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	SocketAddress * remoteAddress = addressForSockaddr(&sif);

	Socket * s = new Socket(static_cast<SocketAddress *>(this->boundAddr->clone()), remoteAddress);

	s->sockaddr = sif;
	s->sockfd = nsfd;
	s->sstream = new SocketStream(nsfd);

	return s;
}

void osiris::ServerSocket::close() {
	if (sockfd >= 0) {
		::close(sockfd);
		sockfd = -1;
	}
}

/******************************************************************************
 *
 *	Datagram Socket
 *
 */

osiris::DatagramSocket::DatagramSocket(const SocketAddress* addr) throw (NetworkException) {
	defaultRoute = NULL;

	if (addr == NULL) {
		throw NetworkException(NetworkException::InvalidAddress);
	}

	this->laddr = static_cast<SocketAddress*>(addr->clone());

	int stype = -1;
	if (dynamic_cast<const Inet4SocketAddress *>(addr)) {
		stype = AF_INET;
	} else if (dynamic_cast<const Inet6SocketAddress *>(addr)) {
		stype = AF_INET6;
	} else if (dynamic_cast<const UnixSocketAddress *>(addr)) {
		stype = AF_UNIX;
	}

	if (stype < 0) {
		throw NetworkException(NetworkException::Unsupported);
	}

	sockfd = ::socket(stype, SOCK_STREAM, 0);
#ifdef __APPLE__
	int set = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
#endif

	if (sockfd < 0) {
		switch (errno) {
			case EACCES:
				throw NetworkException(NetworkException::PermissionDenied);
			case EAFNOSUPPORT:
				case EINVAL:
				case EPROTONOSUPPORT:
				throw NetworkException(NetworkException::Unsupported);
			case ENOBUFS:
				case ENOMEM:
				case ENFILE:
				case EMFILE:
				throw NetworkException(NetworkException::Exhausted);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno, strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	this->sockaddr = sockaddrForAddress(addr);
	if (::bind(sockfd, this->sockaddr.sa, this->sockaddr.size) < 0) {
		switch (errno) {
			case EACCES:
				case EADDRNOTAVAIL:
				case EROFS:
				throw NetworkException(NetworkException::PermissionDenied);
			case EADDRINUSE:
				throw NetworkException(NetworkException::AddrInUse);
			case ELOOP:
				case ENOENT:
				case ENOTDIR:
				throw NetworkException(NetworkException::InvalidPath);
			case ENOMEM:
				throw NetworkException(NetworkException::Exhausted);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno, strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	getsockname(sockfd, this->sockaddr.sa, &this->sockaddr.size);

	copyAddresFromSockaddr(this->laddr, &this->sockaddr);

	this->sstream = new SocketStream(sockfd);
}

osiris::DatagramSocket::~DatagramSocket() {
	close();
	delete sstream;
	delete this->laddr;
	if (this->defaultRoute) delete this->defaultRoute;
}

void osiris::DatagramSocket::setDefaultRoute(const SocketAddress* addr) {
	if (this->defaultRoute) delete this->defaultRoute;
	this->defaultRoute = static_cast<SocketAddress *>(addr->clone());
}

void osiris::DatagramSocket::close() {
	::close(sockfd);
}

uint32_t osiris::DatagramSocket::recv(void* buffer, uint32_t* size, SocketAddress * remote) throw (NetworkException) {
	sockaddr_info sif = sockaddrForAddressType(this->laddr);
	ssize_t received;

#ifndef __APPLE__
	received = recvfrom(sockfd, buffer, (size_t) size, MSG_NOSIGNAL, sif.sa, &sif.size);
#else
	received = recvfrom(sockfd, buffer, (size_t) size, 0, sif.sa, &sif.size);
#endif

	if (received < 0) {
		switch (errno) {
			case EBADFD:
				case EPIPE:
				throw NetworkException(NetworkException::Closed);
			case ECONNRESET:
				throw NetworkException(NetworkException::Reset);
			case EDESTADDRREQ:
				case ENOTCONN:
				throw NetworkException(NetworkException::Disconnected);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno,
						strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	return received;
}

uint32_t osiris::DatagramSocket::send(void* buffer, uint32_t bsize, const SocketAddress * dest)
		throw (NetworkException) {
	if (dest == NULL) dest = defaultRoute;

	sockaddr_info sif = sockaddrForAddress(dest);
	ssize_t sent;

#ifndef __APPLE__
	sent = sendto(sockfd, buffer, (size_t) bsize, MSG_NOSIGNAL, sif.sa, sif.size);
#else
	sent = sendto(sockfd, buffer, (size_t) bsize, 0, sif.sa, sif.size);
#endif

	if (sent < 0) {
		switch (errno) {
			case EBADFD:
				case ECONNREFUSED:
				case EPIPE:
				throw NetworkException(NetworkException::Closed);
			case ECONNRESET:
				throw NetworkException(NetworkException::Reset);
			case EDESTADDRREQ:
				throw NetworkException(NetworkException::Disconnected);
			default:
				networkLogger.log(LogLevel::Error, "Unknown error %d: %s", errno,
						strerror(errno));
				throw NetworkException(NetworkException::Unknown);
		}
	}

	return sent;
}

/******************************************************************************
 *
 *	Name resolution.
 *
 */

InetAddressResolution osiris::lookupName(const_string name) {
	// TODO: Implement name lookup.
}

