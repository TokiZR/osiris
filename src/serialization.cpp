#include "osiris.h"
#include "osiris/serialization.h"

using namespace osiris;
using namespace std;

SerializationManager * osiris::serialManager = NULL;

Logger osiris::serializationLogger("Serialization");

#define logErr(fmt, ...) serializationLogger.log(LogLevel::Error, fmt, ##__VA_ARGS__)

/******************************************************************************
 *
 * Serialization manager.
 *
 */

void osiris::SerializationManager::registerClass(ISerialDef& classInfo) {
	defininitionMap.insert(pair<const type_t *, ISerialDef *>(classInfo.getType(), &classInfo));
}

ISerialDef* osiris::SerializationManager::getSerialDef(const type_t * type) {
	if (defininitionMap.count(type) > 0) {
		return defininitionMap[type];
	} else {
		return NULL;
	}
}

const type_t * osiris::SerializationManager::getSimpleType(std::type_index type) {
	if (typeMap.count(type) == 0) {
		typeMap.insert(pair<type_index, type_t *>(type, new SimpleType(type)));
	}
	return typeMap[type];
}

type_t* osiris::SerializationManager::getRegisteredType(std::type_index type) {
	if (typeMap.count(type) == 0) {
		return NULL;
	} else {
		return typeMap[type];
	}
}

void osiris::SerializationManager::registerBackend(SerializationBackend* backend) {
	serializationLogger.log(LogLevel::Info, "Back-end %s registered.", backend->getName().data());
	backends.push_back(backend);
}

const SerializationBackend* osiris::SerializationManager::getDefaultBackend() {
	if (backends.size()) {
		return serialManager->getBackendList()[0];
	} else {
		return NULL;
	}
}

const template_t* osiris::SerializationManager::getTemaplate(std::type_index type,
                                                             unsigned num_args) {

	/* Get name */
	std::string name;
	std::string dname = demangleName(type.name());
	const char * tname = dname.data();

	while (*tname != '<' && *tname != '\0') {
		name += *tname++;
	}

	auto range = templates.equal_range(name);
	for (auto it = range.first; it != range.second; ++it) {
		const template_t * tm = it->second;
		if (tm->getArgumentCount() == num_args) {
			return tm;
		}

	}

	template_t * tm = new template_t(name, num_args);

	templates.insert(pair<const_string, template_t *>(tm->name(), tm));

	return tm;
}

const std::vector<SerializationBackend*> osiris::SerializationManager::getBackendList() const {
	return backends;
}

const SerializationBackend* osiris::SerializationManager::getBackend(const_string backendName) {
	for (SerializationBackend * back : backends) {
		if (back->getName() == backendName) {
			return back;
		}
	}

	return NULL;
}

void osiris::SerializationManager::dumpTypeInfo() {
	serializationLogger.log(LogLevel::Info,
	                "------------------------- Dumping registered templates -------------------------");

	for (auto & t : templates) {
		serializationLogger.log(LogLevel::Info, "%s", t.second->name().data());
	}

	serializationLogger.log(LogLevel::Info,
	                "--------------------------- Dumping registered types ---------------------------");

	for (auto & t : typeMap) {
		serializationLogger.log(LogLevel::Info, "%s %c%c", t.second->name().data(),
		                dynamic_cast<TemplatedType *>(t.second) ? 'T' : ' ',
		                getSerialDef(t.second) ? 'S' : ' ');
	}
}

/******************************************************************************
 *
 * ISerialDef
 *
 */

void osiris::ISerialDef::serialize(SerializationContext& ctxt, void * source) const
                                                   throw (SerializationException&) {
	if (getFields()) {
		ctxt.beginObject();
		for (const SerialField * f : *getFields()) {
			ctxt.addField(f->getName());
			ctxt.writeSerializable(f->getType(), (void *) ((uint8_t *) source + f->getOffset()));
		}
		ctxt.endObject();
	} else {
		serializationLogger.log(LogLevel::Warning,
		                "Using default serializer for a class with no serializable fields.");
	}
}

void osiris::ISerialDef::deserialize(DeserializationContext& ctxt, void * dest) const
                                                     throw (SerializationException&) {
	if (getFields()) {
		if (ctxt.getType() != SerialType::Object) {
			logErr("Expect object node. But instead node is %s", toString(ctxt.getType()).data());
			throw SerializationException(getType(), "Expect object node.");
		} else {
			ctxt.enterObject();
			const vector<SerialField *> & fields = *getFields();
			for (const SerialField * f : fields) {
				if (ctxt.getFieldName() != f->getName()) {
					logErr("Expected field named %s. But instead the name is %s",
					                f->getName().data(), ctxt.getFieldName().data());
					throw SerializationException(getType(), "Field name mismatch.");
				} else if (ctxt.getFieldType() && ctxt.getFieldType() != f->getType()) {
					logErr("Expected field with type %s. But instead the type is %s",
					                f->getType()->name().data(),
					                ctxt.getFieldType()->name().data());
					throw SerializationException(getType(), "Field type mismatch.");
				}
				ctxt.deserializeField(f->getType(), (void *) ((uint8_t *) dest + f->getOffset()));
			}
			ctxt.leaveObject();
		}
	} else {
		serializationLogger.log(LogLevel::Warning,
		                "Using default de-serializer for a class with no serializable fields.");
	}
}

bool osiris::SimpleType::isPlain() const {
	if (getSerialDef(this)) {
		return getSerialDef(this)->getFields() && getSerialDef(this)->getFields()->size();
	} else {
		return true;
	}
}

bool osiris::TemplatedType::isPlain() const {
	if (getSerialDef(this)) {
		return getSerialDef(this)->getFields() && getSerialDef(this)->getFields()->size();
	} else {
		return true;
	}
}

/******************************************************************************
 *
 * Functions.
 *
 */

void osiris::registerClass(ISerialDef& classInfo) {
	if (!serialManager) {
		serialManager = new SerializationManager();
	}
	serialManager->registerClass(classInfo);
}

ISerialDef* osiris::getSerialDef(const type_t * type) {
	if (!serialManager) {
		serialManager = new SerializationManager();
	}
	return serialManager->getSerialDef(type);
}

void osiris::registerBackend(SerializationBackend* backend) {
	if (!serialManager) {
		serialManager = new SerializationManager();
	}
	serialManager->registerBackend(backend);
}

void osiris::serializeToStream(OutputStream& stream, const type_t* type, void* object)
                                               throw (SerializationException&) {
	const SerializationBackend * deflt = serialManager->getDefaultBackend();

	if (deflt == NULL) {
		logErr("No default serialization backend registered.");
		throw SerializationException(type, "No backend");
	}
	SerializationContext * ctxt = deflt->getSerializationContext();
	ctxt->serializeToStream(stream, type, object);

	delete ctxt;
}

void osiris::deserializeFromStream(InputStream& stream, const type_t* type, void* object)
                                                   throw (SerializationException&) {
	const SerializationBackend * deflt = serialManager->getDefaultBackend();

	if (deflt == NULL) {
		logErr("No default serialization backend registered.");
		throw SerializationException(type, "No backend");
	}

	DeserializationContext * ctxt = deflt->getDeserializationContext();
	ctxt->deserializeFromStream(stream, type, object);

	delete ctxt;
}

/******************************************************************************
 *
 * Context methods.
 *
 */

void osiris::SerializationContext::serializeToStream(OutputStream& stream, const type_t* type,
                                                     void* object) throw (SerializationException&) {
	this->begin(stream);
	writeSerializable(type, object);
	this->finish();
}

void osiris::DeserializationContext::deserializeFromStream(
                InputStream& stream, const type_t* type, void* object)
                                throw (SerializationException&) {
	this->begin(stream);
	deserializeField(type, object);
}
