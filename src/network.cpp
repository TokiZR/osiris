#include <stdint.h>
#include <stddef.h>

#include "osiris/exception.h"
#include "osiris/string.h"
#include "osiris/poll.h"
#include "osiris/object.h"
#include "osiris/stream.h"
#include "osiris/logging.h"
#include "osiris/network.h"

using namespace osiris;

Logger osiris::networkLogger("Network");

/******************************************************************************
 *
 *	Network exception
 *
 */

const char* osiris::NetworkException::what() const noexcept {
	switch(cause) {
		case Unknown:
			return "Unknown error";
			break;
		case PermissionDenied:
			return "Permission denied";
			break;
		case InvalidAddress:
			return "Invalid address";
			break;
		case Unsupported:
			return "Unsupported protocol or socket type";
			break;
		case AddrInUse:
			return "Address In use by another process";
			break;
		case Exhausted:
			return "System resource exhausted";
			break;
		case Unreachable:
			return "Remote host unreachable";
			break;
		case Timeout:
			return "Connection timed out";
			break;
		case InvalidPath:
			return "Invalid path";
			break;
		case Reset:
			return "Connection reset";
			break;
		case Disconnected:
			return "Not connected";
		case Closed:
			return "The socket is closed";
	}
	return "";
}
