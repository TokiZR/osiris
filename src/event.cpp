/*******************************************************************************
 * event.cpp
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "osiris.h"
#include "osiris/event.h"

using namespace osiris;

Interactive::Interactive() :
		listeners(NULL) {
}

Interactive::~Interactive() {
	if (listeners)
		delete listeners;
}

void Interactive::addEventListener(EventType type, EventListener listener,
		void * data, int32_t priority) {
	Listener l = { *listener, data, priority };

	if (!listeners) {
		listeners = new ListenerMap();
	}

	ListenerVector& vec = (*listeners)[type];

	if (vec.empty()) {
		vec.insert(vec.begin(), l);
	} else {
		ListenerVector::iterator it, end;
		for (it = vec.begin(), end = vec.end(); it != end; it++) {
			if (it->priority < priority) {
				break;
			}
		}
		vec.insert(it, l);
	}

	listener->ref();
}

void Interactive::removeEventListener(EventType type, EventListener listener) {
	/* If no vector then return. */
	if (!listeners || listeners->count(type) < 1) {
		return;
	}

	ListenerVector& vec = (*listeners)[type];
	ListenerVector::iterator it, end;
	for (it = vec.begin(), end = vec.end(); it != end;) {
		if (it->iel == *listener) {
			it = vec.erase(it);
			listener->unref();
		} else {
			it++;
		}
	}
}

EventStatus Interactive::fireEvent(Event * e) {
	EventStatus s;

	e->target = this;

	/* If no vector then return. */
	if (listeners && listeners->count(e->type) == 1) {
		ListenerVector& vec = listeners->at(e->type);

		ListenerVector::iterator it, end;
		for (it = vec.begin(), end = vec.end(); it != end;) {
			/* If the handler removes this listener this will keep us safe. */
			Listener l = *it;
			it++;

			l.iel->ref();
			l.iel->callback(e, l.data);
			l.iel->unref();

			if (e->canceled)
				s.canceled = true;
			if (e->killed)
				break;
		}

		s.noPropagate = !e->bubbles;
	}

	return s;
}

size_t osiris::Event::hashValue() const {
	return std::_Hash_impl().hash((void *) this, sizeof(Event));
}

void osiris::Event::toString(OutputStream& stream) const {
	stream.printf("Event{type: \"%s\"; bubbles: %s; cancel: %s;}", type,
			bubbles ? "true" : "false", canceled ? "true" : "false");
}

Object* osiris::Event::clone() const {
	return new Event(this->type);
}
