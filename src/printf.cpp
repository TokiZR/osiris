#include "osiris.h"
#include "osiris/util.h"
#include "osiris/math.h"

#include <string.h>

#include <stdarg.h>

#include <ctype.h>

#include <unordered_map>

using namespace osiris;

#define USE_OWN_PRINTF 1

ssize_t osiris::OutputStream::printf(const char* fmt, ...) throw (exception&) {
	va_list va;
	va_start(va, fmt);
	ssize_t prt = this->vprintf(fmt, va);
	va_end(va);

	return prt;
}

#ifdef USE_OWN_PRINTF

#define BLEN 100

struct FormatContext {
	OutputStream & os;
	const char * fmt;
	va_list & args;

	FormatContext(OutputStream & stream, const char * format, va_list & args) :
			os(stream), fmt(format), args(args) {
	}

	~FormatContext() {
		flush();
	}

	/* Buffer handling */

	char buffer[BLEN];
	int ptr = 0;

	void putc(char c) {
		if (ptr == BLEN) {
			flush();
		}
		buffer[ptr++] = c;

		count++;
	}

	void pad(int size) {
		if(alignLeft) return;
		while(size-- > 0) {
			if(zeroPad) putc('0');
			else putc(' ');
		}
	}

	void pad_after(int size) {
		if(!alignLeft) return;
		while(size-- > 0) {
			putc(' ');
		}
	}

	void flush() {
		if (os.write((uint8_t*) buffer, ptr) < ptr) {
			throw osiris::exception("Could not write to stream");
		}
		ptr = 0;
	}

	int format() {
		for(const char * c = fmt; *c != '\0'; c++) {
			if(*c == '%') {
				c++;
				if(*c == '%') {
					putc('%');
					continue;
				}

				/* Non-sequential flags */
				check_flag:;
				unsigned matched = 0;
				for(int i = 0; i < 3; i++) {
					if(bits[i]->match(*c)) {
						if(matched & (1 << i)) /* repeated flag */
							return -count;
						c += bits[i]->parse(*this, c);
						matched |= 1 << i;
						goto check_flag; /* If a flag is found we go back to checking */
					}
				}

				/* Sequential flags: [width][.precision][size] */
				for(int i = 3; i < 6; i++) {
					if(bits[i]->match(*c)) {
						c += bits[i]->parse(*this, c);
					}
				}

				unsigned index = *c - 0x41;

				if(convs[index]) {
					convs[index]->convert(*this);
				}

				resetFlags();
			} else {
				putc(*c);
			}
		}

		return count;
	}

	/* Parser bits and flags */

	struct pbit {
		virtual ~pbit(){}
		virtual bool match(char c) = 0;
		virtual int parse(FormatContext & fc, const char * bs) = 0;
	};

	bool zeroPad = false;
	bool forceSign = false;
	bool alignLeft = false;

	unsigned width = 0;
	unsigned precision = 0;

	unsigned size = 4; // in bytes

	void resetFlags() {
		zeroPad = false;
		forceSign = false;
		alignLeft = false;
		width = 0;
		precision = 0;
		size = 4;
		count = 0;
	}

	unsigned count = 0;

	static pbit * bits[];

	/* Conversions */

	struct conv {
		virtual ~conv(){}
		virtual char id() = 0;
		virtual void convert(FormatContext & fc) = 0;
	};

	static conv * convs[0x7B - 0x41];
};

#define D_pbit(name, _match, _parse) \
	struct _##name : public FormatContext::pbit {\
		bool match (char c) _match\
		int parse(FormatContext & fc, const char * bs) _parse\
	} name;

D_pbit(zero, {return c == '0';}, {fc.zeroPad = true; return 1;});
D_pbit(sign, {return c == '+';}, {fc.forceSign = true; return 1;});
D_pbit(left, {return c == '-';}, {fc.alignLeft = true; return 1;});
D_pbit(width, {return isdigit(c);}, {
	fc.width = 0;

	int i = 0;
	for(; isdigit(*bs); bs++, i++) {
		fc.width *= 10;
		fc.width += (*bs - '0');
	}

	return i;
});
D_pbit(precision, {return c == '.';}, {
	fc.precision = 0;

	int i = 0;
	for(bs++; isdigit(*bs); bs++, i++) {
		fc.precision *= 10;
		fc.precision += (*bs - '0');
	}

	return i + 1;
});
D_pbit(size, {return strchr("hlLjzt", c) != nullptr;}, {
	switch(*bs){
		case 'h':
			if(*(bs + 1) == 'h') {
				fc.size = sizeof(char);
				return 2;
			} else {
				fc.size = sizeof(short);
			}
			break;
		case 'l':
			if(*(bs + 1) == 'l') {
				fc.size = sizeof(long long);
				return 2;
			} else {
				fc.size = sizeof(long);
			}
			break;
		case 'L':
			fc.size = sizeof(long long);
			break;
		case 'j':
			fc.size = sizeof(intmax_t);
			break;
		case 'z':
			fc.size = sizeof(size_t);
			break;
		case 't':
			fc.size = sizeof(ptrdiff_t);
			break;
	};

	return 1;
});

FormatContext::pbit* FormatContext::bits[] = {
		&::zero,
		&::sign,
		&::left,
		&::width,
		&::precision,
		&::size
};

/******************************************************************************
 *
 *	Conversions
 *
 */

#define D_conv(name, _id, _convert)\
	struct _##name : public FormatContext::conv {\
		char id() {return _id;}\
		void convert(FormatContext & fc) _convert\
	} name##_conv;

#if __WORDSIZE == 32
typedef unsigned argtype_t;
#elif __WORDSIZE == 64
typedef unsigned long argtype_t;
#endif

template <typename T1, typename T2>
struct max_type {
	typedef typename std::conditional<(sizeof(T1) > sizeof(T2)), T1, T2>::type type;
};

uint64_t getNo(va_list & l, size_t size) {
	uint64_t no;

#define idtty(...) __VA_ARGS__

	switch(size) {
		case 1:
			no = va_arg(l, idtty(max_type<uint8_t, argtype_t>::type));
			break;
		case 2:
			no = va_arg(l, idtty(max_type<uint16_t, argtype_t>::type));
			break;
		case 4:
			no = va_arg(l, idtty(max_type<uint32_t, argtype_t>::type));
			break;
		case 8:
			no = va_arg(l, idtty(max_type<uint64_t, argtype_t>::type));
			break;
		default:
			assert(!"Invalid string size");
	}

	return no;
}

void writeNo(FormatContext & fc, bool negAllowed) {
	int64_t val = getNo(fc.args, fc.size);
	bool neg = false;

	char dbuff[20];
	int len = 0;

	if(negAllowed && val < 0) {
		neg = true;
		val = -val;
	}

	uint64_t uval = val;

	while(uval > 0) {
		dbuff[len++] = (uval % 10) + '0';
		uval /= 10;
	}

	bool sign = fc.forceSign || neg;
	if(sign) {
		fc.putc(neg ? '-' : '+');
		fc.pad(fc.width - len -1);
	} else {
		fc.pad(fc.width - len);
	}

	while(len-- > 0) {
		fc.putc(dbuff[len]);
	}

	fc.pad_after((fc.width - len - sign) ? 1 : 0);
}

/* Decimal */
D_conv(decimal, 'd', {
	writeNo(fc, true);
});

/* Integer */
D_conv(integer, 'i', {
	writeNo(fc, true);
});

/* Unsigned */
D_conv(unsigned_decimal, 'u', {
	writeNo(fc, false);
});

/* Octal */
D_conv(octal, 'o', {
	uint64_t val = getNo(fc.args, fc.size);

	unsigned len = m::div_ceil(m::lg_floor(val) + 1, UINT64_C(3));
	fc.pad(fc.width - len);
	for(unsigned i = 0; i < len; i++) {
		fc.putc(((val >> ((len - 1)*3)) & 0x7) + '0');
		val <<= 3;
	}
	fc.pad_after(fc.width - len);
});

/* hexa utility */
char i2x(unsigned val, bool cap) {
	val &= 0xF;

	if(val < 10) {
		return '0' + val;
	} else {
		return val - 10 + (cap ? 'A' : 'a');
	}
}

/* Hexadecimal */
D_conv(hexadecimal, 'x', {
	uint64_t val = getNo(fc.args, fc.size);

	unsigned len = m::div_ceil(m::lg_floor(val) + 1, UINT64_C(4));
	fc.pad(fc.width - len);
	for(unsigned i = 0; i < len; i++) {
		fc.putc(i2x((val >> ((len - 1)*4)), false));
		val <<= 4;
	}
	fc.pad_after(fc.width - len);
});

/* Upper Case Hexadecimal */
D_conv(Hexadecimal, 'X', {
	uint64_t val = getNo(fc.args, fc.size);

	unsigned len = m::div_ceil(m::lg_floor(val) + 1, UINT64_C(4));
	fc.pad(fc.width - len);
	for(unsigned i = 0; i < len; i++) {
		fc.putc(i2x((val >> (len - 1)*4), true));
		val <<= 4;
	}
	fc.pad_after(fc.width - len);
});

/* String */
D_conv(string, 's', {
	const char * str = va_arg(fc.args, const char *);

	unsigned len = strlen(str);

	fc.pad(fc.width - len);

	for(unsigned i = 0; i < len; i++, str++)
		fc.putc(*str);

	fc.pad_after(fc.width - len);
});

/******************************************************************************
 *
 * Conversion lookup table.
 *
 */
FormatContext::conv * FormatContext::convs[0x7B - 0x41] = {
		// A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
 		&Hexadecimal_conv,
 		// Y, Z, [, \, ], ^, _, `
 		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, // a, b, c
		&decimal_conv,
		0, // e
		0, // float goes here
		0, // pretty float goes here
		0, // h
		&integer_conv,
		0, 0, 0, 0, 0, // j, k, l, m, n
		&octal_conv,
		0, 0, 0, //p, q, r
		&string_conv,
		0, // t
		&unsigned_decimal_conv,
		0, 0, //v, w
		&hexadecimal_conv,
		0, 0 // y, z
};

#define ARRSIZE(arr) (sizeof(arr) / sizeof(arr[0]))

ssize_t osiris::OutputStream::vprintf(const char * fmt, va_list & va) throw (std::exception&) {
	FormatContext ctx(*this, fmt, va);

	return ctx.format();
}

#else

ssize_t osiris::OutputStream::vprintf(const char * fmt, va_list va) throw(exception&) {
	char * data;
	ssize_t written = vasprintf(&data, fmt, va);
	puts(data);
	free(data);

	return written;
}

#endif
