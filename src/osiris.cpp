#include "osiris.h"

#include "binary-serializer.h"
#include "osiris/network.h"

using namespace osiris;

Logger osiris::osirisLogger("Osiris");

void osiris::init() {
	static bool osirisReady = false;
	if (!osirisReady) {
		/* Logging */
		osirisLogger.addForwarding(getGlobalLogger());
		serializationLogger.addForwarding(osirisLogger);
		networkLogger.addForwarding(osirisLogger);

		if (!serialManager) {
			serialManager = new SerializationManager();
		}

		/* Register built in backends. */
		serialManager->registerBackend(new BinarySerializer());

		osirisLogger.log(LogLevel::Message, "Osiris initialised.");

		osirisReady = true;
	}
}
