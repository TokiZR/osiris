/*******************************************************************************
 * string.cpp
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "osiris/string.h"

#include <stdarg.h>

#ifndef __STDC_LIMIT_MACROS
#define __STDC_LIMIT_MACROS 1
#endif
#include <stdint.h>

using std::string;

string osiris::stringFormat(const char * fmt, ...) {
	char * s;
	std::string str;
	va_list l;

	va_start(l, fmt);
	vasprintf(&s, fmt, l);
	va_end(l);
	str = std::string(s);
	free(s);

	return str;
}

static uint32_t strmod(const char * str, uint32_t mod) {
	uint32_t len = strlen(str);
	uint64_t res;

	/* Seeing our little string as a very big number
	 * Since we implement a sort of Horner rule we must start from the most
	 * significant chunk. */
	uint64_t * bignum = (uint64_t *) (str + len - 8);
	uint64_t powermod;

	res = 0;

	powermod = (UINT64_MAX - mod + 1) % mod;

	while (len >= 8) {
		res = ((res * powermod) + (*bignum % mod)) % mod;
		bignum--;
		len -= 8;
	}

	uint64_t cluster = 0;

	memcpy((void *) &cluster, (void *) str, len);

	uint32_t i;
	uint64_t mask = ~0L;
	for (i = 8; i > len; i--) {
		mask >>= 8;
	}
	cluster &= mask;

	res = ((res * ((mask + 1) % mod)) + (cluster % mod)) % mod;

	return res;
}

size_t osiris::hashString::operator ()(const char* str) const {
	return strmod(str, (unsigned) -1);
}

#include <cxxabi.h>

std::string osiris::demangleName(const_string name) {
	int status = 0;

	char * demangled = abi::__cxa_demangle(name, NULL, NULL, &status);

	std::string ret(demangled);
	free(demangled);

	return ret;
}
