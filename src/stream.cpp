#include "osiris.h"

#include <string.h>

#include <stdarg.h>

#ifdef __unix__
#include <poll.h>
#endif

using namespace osiris;
using namespace std;

static short pollEvt2Flag(PollableEvent e) {
	switch (e) {
		case PollableEvent::In:
			return POLLIN;
		case PollableEvent::Out:
			return POLLOUT;
		default:
			return 0;
	}
}

static int whence2C(Whence w) {
	switch (w) {
		case Whence::Start:
			return SEEK_SET;
			break;
		case Whence::End:
			return SEEK_END;
			break;
		case Whence::Current:
			return SEEK_CUR;
			break;
	}
	return -1;
}

/******************************************************************************
 *
 * 	Handy print/read.
 *
 */

ssize_t osiris::InputStream::scanf(const char* fmt, ...) throw (exception &) {
	return 0;
}

std::string osiris::InputStream::readString(size_t maxLen) throw (exception&) {
	std::string s;
	char c;

	while (this->read((uint8_t *) &c, 1) > 0) {
		if (c != '\0') {
			s += c;
		} else {
			break;
		}
	}

	return s;
}

int osiris::InputStream::getChar() throw (exception&) {
	char c;
	if (this->read((uint8_t *) &c, 1) > 0) {
		return c;
	}

	return -1;
}

void osiris::InputStream::skip(size_t offt) throw (exception&) {
	Seekable * s = dynamic_cast<Seekable *>(this);
	if (s) {
		s->seek(offt, Whence::Current);
	} else {
		uint8_t buff[1024];
		while (offt) {
			this->read(buff, std::min((size_t)1024, offt));
			offt = offt > 1024 ? 0 : offt - 1024;
		}

	}
}

ssize_t osiris::OutputStream::puts(const_string string) throw (exception &) {
	return this->write((const uint8_t *) string.data(), string.length());
}

ssize_t osiris::OutputStream::putchar(char ch) throw (exception &) {
	return this->write((uint8_t *) &ch, 1);
}

/******************************************************************************
 *
 * 	File Output Stream
 *
 */
osiris::FileOutputStream::FileOutputStream(const_string path, const char* mode)
												   throw (FileException) :
		owns(true) {
	file = fopen(path, mode);
	if (!file) {
		throw FileException(errno);
	}
}

osiris::FileOutputStream::FileOutputStream(FILE* file, bool owned) :
		file(file), owns(owned) {
}

osiris::FileOutputStream::~FileOutputStream() {
	if (file && owns)
		fclose(file);
}

ssize_t osiris::FileOutputStream::write(const uint8_t* data, size_t dlength) throw (exception &) {
	ssize_t result = fwrite(data, 1, dlength, file);

	if (result < 0) {
		FileException fe = FileException(errno);
		clearerr(file);
		throw fe;
	}

	return result;
}

void osiris::FileOutputStream::flush() throw (exception) {
	ssize_t result = fflush(file);

	if (result < 0) {
		FileException fe = FileException(errno);
		clearerr(file);
		throw fe;
	}
}

void osiris::FileOutputStream::close() {
	if (file && owns) {
		fclose(file);
		file = NULL;
	}
}

size_t osiris::FileOutputStream::seek(ssize_t offset, Whence w) throw (exception &) {
	ssize_t result = fseek(file, offset, whence2C(w));

	if (result < 0) {
		FileException fe = FileException(errno);
		clearerr(file);
		throw fe;
	}

	return tell();
}

size_t osiris::FileOutputStream::tell() throw (exception &) {
	ssize_t result = ftell(file);
	if (result < 0) {
		FileException fe = FileException(errno);
		clearerr(file);
		throw fe;
	}

	return result;
}

size_t osiris::FileOutputStream::size() throw (exception &) {
	size_t pos = tell();

	ssize_t result = fseek(file, 0, SEEK_END);

	if (result < 0) {
		FileException fe = FileException(errno);
		clearerr(file);
		throw fe;
	}

	fseek(file, pos, SEEK_SET);

	return result;
}

bool osiris::FileOutputStream::wouldblock(PollableEvent e) {
	struct pollfd pfd;
	pfd.events = pollEvt2Flag(e);
	pfd.revents = 0;
	pfd.fd = fileno(this->file);

	::poll(&pfd, 1, 0);
	return pfd.revents & pollEvt2Flag(e);
}

/******************************************************************************
 *
 * 	File Input Stream
 *
 */

osiris::FileInputStream::FileInputStream(const_string path, const char* mode) throw (FileException) :
		owns(true) {
	file = fopen(path, mode);
	if (!file) {
		throw FileException(errno);
	}
}

osiris::FileInputStream::FileInputStream(FILE* file, bool owned) :
		file(file), owns(owned) {
}

osiris::FileInputStream::~FileInputStream() {
	if (file && owns)
		fclose(file);
}

void osiris::FileInputStream::close() {
	if (file && owns) {
		fclose(file);
		file = NULL;
	}
}

ssize_t osiris::FileInputStream::read(uint8_t * buffer, size_t buffsize) throw (exception &) {
	ssize_t result = fread(buffer, 1, buffsize, file);

	if (result < 0) {
		if (ferror(file)) {
			clearerr(file);
			FileException fe = FileException(errno);
			throw fe;
		} else {
			result = 0;
		}
	}

	return result;
}

size_t osiris::FileInputStream::seek(ssize_t offset, Whence w) throw (exception &) {
	ssize_t result = fseek(file, offset, whence2C(w));

	if (result < 0) {
		FileException fe = FileException(errno);
		clearerr(file);
		throw fe;
	}

	return tell();
}

size_t osiris::FileInputStream::tell() throw (exception &) {
	ssize_t result = ftell(file);
	if (result < 0) {
		FileException fe = FileException(errno);
		clearerr(file);
		throw fe;
	}

	return result;
}

size_t osiris::FileInputStream::size() throw (exception &) {
	size_t pos = tell();

	ssize_t result = fseek(file, 0, SEEK_END);

	if (result < 0) {
		FileException fe = FileException(errno);
		clearerr(file);
		throw fe;
	}

	fseek(file, pos, SEEK_SET);

	return result;
}

bool osiris::FileInputStream::wouldblock(PollableEvent e) {
	struct pollfd pfd;
	pfd.events = pollEvt2Flag(e);
	pfd.revents = 0;
	pfd.fd = fileno(this->file);

	::poll(&pfd, 1, 0);
	return pfd.revents & pollEvt2Flag(e);
}

/******************************************************************************
 *
 * 	Byte Buffer Output Stream
 *
 */

ssize_t osiris::ByteBufferOutputStream::write(const uint8_t* data, size_t dlength) throw (exception&) {
	if (!dataBuffer) {
		dataBuffer = new vector<uint8_t>();
	}

	size_t osize = dataBuffer->size();

	dataBuffer->resize(osize + dlength);

	memcpy(dataBuffer->data() + osize, data, dlength);

	return dlength;
}

void osiris::ByteBufferOutputStream::flush() throw (exception) {
}

void osiris::ByteBufferOutputStream::close() {
}

/******************************************************************************
 *
 *	Byte Buffer Input Stream
 *
 */
ssize_t osiris::ByteBufferInputStream::read(uint8_t* buffer, size_t buffsize) throw (exception&) {
	if (size - cursor > buffsize) {
		memcpy(buffer, this->buffer + cursor, sizeof(uint8_t) * buffsize);
		cursor += buffsize;
		return buffsize;
	} else if (cursor != size) {
		size_t sz = size - cursor;
		memcpy(buffer, this->buffer + cursor, sizeof(uint8_t) * sz);
		cursor = size;
		return sz;
	} else {
		return 0;
	}
}

void osiris::ByteBufferInputStream::close() {
}

/******************************************************************************
 *
 *	Capped Input Stream
 *
 */

ssize_t osiris::CappedInputStream::read(uint8_t * buffer, size_t buffsize) throw (exception&) {
	ssize_t read = 0;
	if (limit) {
		read = src.read(buffer, std::min(buffsize, limit));
		limit = buffsize >= limit ? 0 : limit - buffsize;

		if (limit == 0) {
			callback(userCbData, this);
		}
	}

	return read;
}

void osiris::CappedInputStream::close() {
	if (limit) {
		limit = 0;
		callback(userCbData, this);
	}
}


/******************************************************************************
 *
 *	Built-In Streams
 *
 */

static FileInputStream __fstdin(::stdin);
static FileOutputStream __fstdout(::stdout);
static FileOutputStream __fstderr(::stderr);

InputStream & osiris::stdin = __fstdin;
OutputStream & osiris::stdout = __fstdout;
OutputStream & osiris::stderr = __fstderr;

