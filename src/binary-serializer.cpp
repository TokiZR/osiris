#include "binary-serializer.h"

using namespace osiris;
using namespace std;

//using BinaryDeserializationContext::State;

#define logErr(fmt, ...) serializationLogger.log(LogLevel::Error, fmt, ##__VA_ARGS__)

#ifndef NDEBUG
#undef logDebug
#define logDebug(fmt, ...) serializationLogger.log(LogLevel::Debug, fmt, ##__VA_ARGS__)
#endif

const_string osiris::toString(MagicByte b) {
	switch (b) {
		case MagicByte::LittleEndianFlag:
			return "LittleEndian";
		case MagicByte::BigEndianFlag:
			return "BigEndian";
		case MagicByte::BeginObjectFlag:
			return "BeginObject";
		case MagicByte::EndObjectFlag:
			return "EndObject";
		case MagicByte::BeginArrayFlag:
			return "BeginArray";
		case MagicByte::EndArrayFlag:
			return "EndArray";
		case MagicByte::BeginFlatFlag:
			return "BeginFlat";
		case MagicByte::BeginNameFlag:
			return "BeginName";
		default:
			return "Invalid flag";
	}
}

/******************************************************************************
 *
 * Binary Serializer
 *
 */

#define rawSerialiser(type, ...) ({\
	RawCopySerializer<type, ## __VA_ARGS__> * __serial = new RawCopySerializer<type, ## __VA_ARGS__>();\
	pair<const type_t *, ISerialDef *>(__serial->getType(), __serial);\
})

#define serializer(serial) ({\
		ISerialDef * __serial = serial;\
		pair<const type_t *, ISerialDef *>(__serial->getType(), __serial);\
	})

std::unordered_map<const type_t *, ISerialDef *> osiris::BinarySerializer::primitiveMappings;

osiris::BinarySerializer::BinarySerializer() {
	primitiveMappings.insert(rawSerialiser(int8_t));
	primitiveMappings.insert(rawSerialiser(int16_t));
	primitiveMappings.insert(rawSerialiser(int32_t));
	primitiveMappings.insert(rawSerialiser(int64_t));

	primitiveMappings.insert(rawSerialiser(uint8_t));
	primitiveMappings.insert(rawSerialiser(uint16_t));
	primitiveMappings.insert(rawSerialiser(uint32_t));
	primitiveMappings.insert(rawSerialiser(uint64_t));

	primitiveMappings.insert(rawSerialiser(float, false));
	primitiveMappings.insert(rawSerialiser(double, false));

	primitiveMappings.insert(rawSerialiser(bool));

	primitiveMappings.insert(serializer(new stringSerializer()));
}

SerializationContext*
osiris::BinarySerializer::getSerializationContext() const {
	return new BinarySerializationContext();
}

DeserializationContext*
osiris::BinarySerializer::getDeserializationContext() const {
	return new BinaryDeserializationContext();
}

/******************************************************************************
 *
 * Binary Serializer — Serialization Context
 *
 */

void BinarySerializationContext::checkWrite(bool naming)
		throw (SerializationException &) {
	if (!serializing) {
		logErr("Not currently serializing an object.");
		throw SerializationException(getType(), "No object.");
	} else if (!hasParent() && fieldSet) {
		serializationLogger.log(LogLevel::Warning,
				"Attempting to add more data to an already finished object.");
	} else if (!naming && hasParent() && parent.top() == OBJECT && !named) {
		logErr("Attempt to set an unnamed field.");
		throw SerializationException(getType(),
				"Attempt to set an unnamed field.");
	}
}

osiris::BinarySerializationContext::BinarySerializationContext() :
		serializing(false), fieldSet(false), named(false) {
}

void osiris::BinarySerializationContext::begin(OutputStream& stream) {
	if (serializing) {
		serializationLogger.log(LogLevel::Warning,
				"Initialising a new serialisation before the previous one was finished.");
	} else {
		serializing = true;
	}

	fieldSet = false;
	named = false;

	parent = stack<bool>();
	eSerials = stack<SerialPoint>();

	this->stream = &stream;
	logDebug("Began serializing object.");
#if ____BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
	stream.putchar((char) MagicByte::LittleEndianFlag);
#else
	stream.putchar((char) MagicByte::LittleEndianFlag);
#endif
}

void BinarySerializationContext::writeValue(uint8_t * data, uint32_t len) {
	checkWrite();

	uint32_t sz = (uint32_t) len;
	logDebug("Writing %zu bytes of binary data.", (size_t ) len);

	stream->putchar((char) MagicByte::BeginFlatFlag);
	stream->write((uint8_t *) &sz, sizeof(uint32_t));
	stream->write(data, len);

	if (hasParent()) {
		if (parent.top() == OBJECT) named = false;
		//stream->puts(", ");
	}
}

void BinarySerializationContext::writeSerializable(const type_t * type,
		void * object) throw (SerializationException&) {

	logDebug("Writing serializable value of type '%s'", type->name().data());

	ISerialDef * def;

	if (BinarySerializer::primitiveMappings.count(type) > 0) {
		def = BinarySerializer::primitiveMappings[type];
	} else {
		def = getSerialDef(type);
	}

	if (def == NULL) {
		logErr("No serial Definition for type %s.", type->name().data());
		throw SerializationException(type, "No serial definition.");
	}

	eSerials.push(SerialPoint(type, parent.size()));
	def->serialize(*this, object);
	eSerials.pop();
}

OutputStream & BinarySerializationContext::getFieldStream(uint32_t length) {
	return *stream;
}

void BinarySerializationContext::beginObject() {
	checkWrite();

	parent.push(OBJECT);

	named = false;
	fieldSet = false;

	logDebug("Began writing object");
	stream->putchar((char) MagicByte::BeginObjectFlag);
}

void BinarySerializationContext::endObject() {
	if (!hasParent()) {
		logErr("Attempt to end an object when at top level.");
		throw SerializationException(getType(),
				"Attempt to end an object when at top level.");
	} else if (parent.top() != OBJECT) {
		logErr("Attempt to end an object inside an array.");
		throw SerializationException(getType(),
				"Attempt to end an object inside an array.");
	}

	parent.pop();

	logDebug("Finished writing object");
	stream->putchar((char) MagicByte::EndObjectFlag);
	if (hasParent()) {
		if (parent.top() == OBJECT) named = false;
	}
}

void BinarySerializationContext::addField(const_string name) {
	checkWrite(true);

	named = true;

	logDebug("Adding field named '%s'", name.data());
	uint32_t nameLen = name.length();
	stream->putchar((char) MagicByte::BeginNameFlag);
	stream->write((uint8_t *) &nameLen, sizeof(uint32_t));
	stream->write((uint8_t *) name.data(), nameLen);
}

void BinarySerializationContext::beginArray(unsigned size) {
	checkWrite();

	parent.push(ARRAY);

	named = false;
	fieldSet = false;

	logDebug("Begin writing array of size %u", size);
	stream->putchar((char) MagicByte::BeginArrayFlag);
	stream->write((uint8_t *) &size, sizeof(size));
}

void BinarySerializationContext::endArray() {
	if (!hasParent()) {
		logErr("Attempt to end an array when at top level.");
		throw SerializationException(getType(),
				"Attempt to end an an array when at top level.");
	} else if (parent.top() != ARRAY) {
		logErr("Attempt to end an array inside an object.");
		throw SerializationException(getType(),
				"Attempt to end an array inside an object.");
	}

	parent.pop();

	logDebug("Finished writing array");
	stream->putchar((char) MagicByte::EndArrayFlag);
	if (hasParent()) {
		if (parent.top() == OBJECT) named = false;
	}
}

void BinarySerializationContext::finish() {
	logDebug("Finished serializing object");
	serializing = false;
}

/******************************************************************************
 *
 * Binary Serializer — De-serialization Context
 *
 */

#define throwEx(message) throw SerializationException(getObjectType(), message)

#define assertedRead(where, size) ({\
	size_t read = stream->read((uint8_t*) where, size);\
	if(read != size) {\
		logErr("Requested to read %zu bytes but got served only %zu.", (size_t) size, read);\
		throwEx("Stream ended prematurely.");\
	}\
})\

void osiris::BinaryDeserializationContext::readFieldMeta(bool name) {
	char tp;
	bool field = false;

	assertedRead(&tp, sizeof(tp));

	magicByte = (MagicByte) tp;

	switch ((MagicByte) tp) {
		case MagicByte::BeginObjectFlag:
			field = true;
			type = SerialType::Object;
			break;
		case MagicByte::EndObjectFlag:
			if (!nodes.size() || nodes.top().type != OBJECT) {
				logError(
						"Invalid object end mark inside an array or at top level.");
				throwEx(
						"Invalid object end mark inside an object or at top level.");
			}
			fieldOver = true;
			type = SerialType::Unset;
			break;
		case MagicByte::BeginArrayFlag:
			field = true;
			type = SerialType::Array;
			break;
		case MagicByte::EndArrayFlag:
			if (!nodes.size() || nodes.top().type != ARRAY) {
				logError(
						"Invalid array end mark inside an object or at top level.");
				throwEx(
						"Invalid array end mark inside an object or at top level.");
			}
			fieldOver = true;
			type = SerialType::Unset;
			break;
		case MagicByte::BeginFlatFlag:
			field = true;
			type = SerialType::Flat;
			break;
		case MagicByte::BeginNameFlag:
			if (!name) {
				logError("Unexpected name mark on unnamed field.");
				throwEx("Unexpected name mark on unnamed field.");
			}
			break;
		default:
			logError("Invalid field marker '%c'.", tp);
			throwEx("Invalid field marker.");
	}

	if (!name) {
		logDebug("Encountered field of type %s.", toString(type).data());
	} else {
		logDebug("Encountered name field.");
	}

	if (name && field) {
		logError("Expected a name mark but got field mark %s.",
				toString(magicByte).data());
		throwEx("Expected a name mark but got a field mark.");
	}

	name = magicByte == MagicByte::BeginNameFlag;

	if (type == SerialType::Flat || name) {
		uint32_t size;
		assertedRead(&size, sizeof(size));
		currentFieldSize = size;
	}

	if (name) {
		currentName.resize(currentFieldSize);
		assertedRead(&currentName[0], currentFieldSize);
	}
}

void osiris::BinaryDeserializationContext::checkRead(bool leave)
		throw (SerializationException&) {
	if (!deserializing) {
		logErr("Not currently de-serializing an object.");
		throwEx("No object.");
	} else if (!hasParent() && fieldOver && !leave) {
		serializationLogger.log(LogLevel::Warning,
				"Attempting to read more data from an already finished object.");
	} else if (awaitingStream) {
		logErr(
				"Stream is locked, awaiting user to finish reading member stream.");
		throwEx(
				"Stream is locked, awaiting user to finish reading member stream.");
	}
}

void osiris::BinaryDeserializationContext::begin(InputStream& s) {
	if (this->deserializing) {
		throwEx("Serialization already in progress.");
	}

	this->stream = &s;

	char endianess;
	assertedRead(&endianess, sizeof(endianess));

	if (endianess == (char) MagicByte::LittleEndianFlag) {
		littleEndian = true;
	} else if (endianess == (char) MagicByte::BigEndianFlag) {
		littleEndian = false;
	} else {
		logErr(
				"Invalid endianess flag '0x%hhX'. Serialised object is most-likely corrupted.",
				endianess);
		throwEx(
				"Invalid endianess flag. Serialised object is most-likely corrupted.");
	}

	type = SerialType::Unset;
	deserializing = true;
	fieldOver = false;
	awaitingStream = false;

	logDebug("Began reading serialised data with endianess %c.", endianess);

	readFieldMeta();
}

SerialType BinaryDeserializationContext::getType() {
	checkRead();

	return type;
}

const_string BinaryDeserializationContext::getFieldName() {
	checkRead();

	if (!hasParent() || nodes.top().type != OBJECT) {
		logErr("Attempt to read a name when outside an object's field.");
		throwEx("Attempt to read a name when outside an object's field.");
	}

	return currentName;
}

uint32_t BinaryDeserializationContext::getFieldSize() {
	checkRead();

	return currentFieldSize;
}

type_t * BinaryDeserializationContext::getFieldType() {
	/* TODO: Store type information */
	return NULL;
}

size_t BinaryDeserializationContext::readField(void * buffer,
		uint32_t bufferSize) {
	checkRead();

	/* Cache this value that changes when the next field is read. */
	uint32_t read = currentFieldSize;

	if (hasParent() && nodes.top().type == OBJECT)
		logDebug("Reading field %s which holds %u bytes.", currentName.data(),
				currentFieldSize);
	else
		logDebug("Reading field which holds %u bytes.", currentFieldSize);

	assertedRead(buffer, std::min(bufferSize, currentFieldSize));
	if (bufferSize < currentFieldSize) {
		stream->skip(currentFieldSize - bufferSize);
	}

	fieldOver = true;

	/* Pass on to the next field */
	if (nodes.size()) {
		scanNext();
	}

	return read;
}

namespace osiris {
    void fieldStreamCallback(BinaryDeserializationContext * ctxt);
}

void osiris::fieldStreamCallback(BinaryDeserializationContext * ctxt) {
	ctxt->awaitingStream = false;

	if (ctxt->userStream->getLimit() > 0) {
		ctxt->stream->skip(ctxt->userStream->getLimit());
	}

	ctxt->fieldOver = true;

	/* Pass on to the next field */
	if (ctxt->nodes.size()) {
		ctxt->scanNext();
	}

	delete ctxt->userStream;
}

InputStream & BinaryDeserializationContext::getFieldStream() {
	checkRead();

	userStream = new CappedInputStream(*stream, currentFieldSize,
			(CappedInputStream::Callback) fieldStreamCallback, this);
	awaitingStream = true;
	return *userStream;
}

void BinaryDeserializationContext::deserializeField(const type_t * type,
		void * field) throw (SerializationException&) {
	logDebug("Reading serialized value of type '%s'", type->name().data());

	ISerialDef * def;

	if (BinarySerializer::primitiveMappings.count(type) > 0) {
		def = BinarySerializer::primitiveMappings[type];
	} else {
		def = getSerialDef(type);
	}

	if (def == NULL) {
		logErr("No serial Definition for type %s.", type->name().data());
		throw SerializationException(type, "No serial definition.");
	}

	eSerials.push(SerialPoint(type, nodes.size()));

	fieldOver = false;
	def->deserialize(*this, field);
	fieldOver = true;

	eSerials.pop();
}

void BinaryDeserializationContext::scanNext() {
	nodes.top().membSoFar++;
	if (nodes.top().type == OBJECT) {
		readFieldMeta(true);
	}
	/* If the object did not end */
	if (magicByte != MagicByte::EndObjectFlag) {
		readFieldMeta();
	}
}

void BinaryDeserializationContext::enterObject() {
	checkRead();

	if (type != SerialType::Object) {
		logErr("Attempt to enter fields of non-object node.");
		throw SerializationException(getObjectType(),
				"Attempt to enter fields of non-object node.");
	}

	logDebug("Entering Object.");

	nodes.push(SNode());

	/* Read first field meta. */
	readFieldMeta(true);
	readFieldMeta();
}

void BinaryDeserializationContext::leaveObject() {
	checkRead(true);

	if (magicByte != MagicByte::EndObjectFlag) {
		logError("Attempt to leave when object was not finished.");
		throw SerializationException(getObjectType(),
				"Attempt to leave when object was not finished.");
	}

	logDebug("Leaving Object.");

	nodes.pop();

	/* Pass on to the next field */
	if (nodes.size()) {
		scanNext();
	}
}

unsigned BinaryDeserializationContext::getArrayLength() {
	checkRead();

	if (!hasParent() || nodes.top().type != ARRAY) {
		return 0;
	}

	return nodes.top().numMemb;
}

void BinaryDeserializationContext::enterArray() {
	checkRead();

	if (type != SerialType::Array) {
		logErr("Attempt to enter elements of non-array node.");
		throw SerializationException(getObjectType(),
				"Attempt to enter elements of non-array node.");
	}
	logDebug("Entering Array.");

	uint32_t asize;

	assertedRead(&asize, sizeof(asize));

	nodes.push(SNode(asize));

	/* Read first field meta. */
	readFieldMeta();
}

void BinaryDeserializationContext::leaveArray() {
	checkRead(true);

	if (magicByte != MagicByte::EndArrayFlag) {
		logError("Attempt to leave when array was not finished.");
		throw SerializationException(getObjectType(),
				"Attempt to leave when array was not finished.");
	}

	logDebug("Leaving array.");

	nodes.pop();

	/* Pass on to the next field */
	if (nodes.size()) {
		scanNext();
	}
}

/******************************************************************************
 *
 * Binary Serializer — Base Serialisers
 *
 */

void osiris::stringSerializer::serialize(SerializationContext& ctxt,
		void* source) const throw (SerializationException&) {
	std::string * s = (std::string *) source;
	ctxt.writeValue((uint8_t *) s->data(), s->size());
}

void osiris::stringSerializer::deserialize(DeserializationContext& ctxt,
		void* dest) const throw (SerializationException&) {
	std::string * s = (std::string *) dest;
	s->resize(ctxt.getFieldSize());
	ctxt.readField((void *) s->data(), s->capacity());
}
