#ifndef __BINARY_SERIALIZER_H__
#define __BINARY_SERIALIZER_H__

#include "osiris.h"
#include "osiris/serialization.h"

#include <byteswap.h>

#include <stack>

#define ARRAY false
#define OBJECT true

namespace osiris {

enum class MagicByte
	: unsigned {
		LittleEndianFlag = 'L',
	BigEndianFlag = 'B',

	BeginObjectFlag = '{',
	EndObjectFlag = '}',

	BeginArrayFlag = '[',
	EndArrayFlag = ']',

	BeginFlatFlag = 'D',

	BeginNameFlag = 'N'
};

const_string toString(MagicByte b);

/******************************************************************************
 *
 * Binary Serializer
 *
 */

class BinarySerializer: public SerializationBackend {
public:
	static std::unordered_map<const type_t *, ISerialDef *> primitiveMappings;

	BinarySerializer();

	virtual const_string getName() const {
		return "BinarySerializer";
	}

	virtual SerializationContext * getSerializationContext() const;
	virtual DeserializationContext * getDeserializationContext() const;

	friend class BinarySerializationContext;
	friend class BinaryDeserializationContext;
};

/******************************************************************************
 *
 * Binary Serializer — Serialization Context
 *
 */

class BinarySerializationContext: public SerializationContext {
private:
	OutputStream * stream = NULL;

	/**
	 * Weather serialization of an object is currently under way.
	 */
	bool serializing :1;

	/**
	 * Weather the current field is set.
	 */
	bool fieldSet :1;

	/**
	 * Weather a name has been provided for the field.
	 */
	bool named :1;

	/**
	 * Stack of parent container types.
	 */
	std::stack<bool> parent;

	/**
	 * Weather the currently de-serializing type has a parent container.
	 */
	bool hasParent() {
		return parent.size() && (!eSerials.size() || eSerials.top().depth != parent.size());
	}

	struct SerialPoint {
		const type_t * type;
		unsigned depth;

		SerialPoint(const type_t * type, unsigned depth) :
				type(type), depth(depth) {
		}
	};

	/**
	 * Stack of types explicitly de-serialized.
	 */
	std::stack<SerialPoint> eSerials;

	const type_t * getType() {
		return eSerials.size() ? eSerials.top().type : NULL;
	}

	void checkWrite(bool naming = false) throw (SerializationException &);

public:
	BinarySerializationContext();

	void begin(OutputStream & stream);

	virtual void writeValue(uint8_t * data, uint32_t len);

	virtual void writeSerializable(const type_t * type, void * object)
			throw (SerializationException&);

	virtual OutputStream & getFieldStream(uint32_t length);

	virtual void beginObject();

	virtual void endObject();

	virtual void addField(const_string name);

	virtual void beginArray(unsigned size);

	virtual void endArray();

	virtual void finish();
};

/******************************************************************************
 *
 * Binary Serializer — De-serialization Context
 *
 */

class BinaryDeserializationContext: public DeserializationContext {
private:
	InputStream * stream = NULL;

	CappedInputStream * userStream;

	struct SerialPoint {
		const type_t * type;
		unsigned depth;

		SerialPoint(const type_t * type, unsigned depth) :
				type(type), depth(depth) {
		}
	};

	/**
	 * Stack of types explicitly de-serialized.
	 */
	std::stack<SerialPoint> eSerials;

	struct SNode {
		/**
		 * Number of member total for array.
		 */
		uint32_t numMemb;

		/**
		 * Number of members so far for object and array.
		 */
		uint32_t membSoFar;

		/**
		 * OBJECT or ARRAY.
		 */
		bool type :1;

		SNode(uint32_t numMemb) :
				numMemb(numMemb), membSoFar(0), type(ARRAY) {
		}

		SNode() :
				numMemb(0), membSoFar(0), type(OBJECT) {
		}
	};

	std::stack<SNode> nodes;

	/**
	 * Weather the serialized object is little endian.
	 */
	bool littleEndian :1;

	/**
	 * Weather de-serialization of an object is currently under way.
	 */
	bool deserializing :1;

	/**
	 * Type of the current data.
	 */
	SerialType type :2;

	/**
	 * Weather the current compound field (array or object) is over.
	 */
	bool fieldOver :1;

	/**
	 * Weather the user has taken a stream and left the context locked.
	 */
	bool awaitingStream :1;

	/**
	 * The last read magic byte number.
	 */
	MagicByte magicByte :7;

	/**
	 * Name of the current field. If the field is inside an object.
	 */
	std::string currentName;

	/**
	 * Size of the current field.
	 */
	uint32_t currentFieldSize;

	/**
	 * Read the fields type and relevant metadata.
	 *
	 * Also sets up the internal state.
	 */
	void readFieldMeta(bool name = false);

	const type_t * getObjectType() {
		return eSerials.size() ? eSerials.top().type : NULL;
	}

	void checkRead(bool leave = false) throw (SerializationException &);

	/**
	 * Weather the currently de-serializing type has a parent container.
	 */
	bool hasParent() {
		return nodes.size() && (!eSerials.size() || eSerials.top().depth < nodes.size());
	}

	void scanNext();

public:

	void begin(InputStream & stream);

	virtual SerialType getType();

	virtual const_string getFieldName();

	virtual uint32_t getFieldSize();

	virtual size_t readField(void * buffer, uint32_t bufferSize);

	virtual InputStream & getFieldStream();

	virtual type_t * getFieldType();

	virtual void deserializeField(const type_t * type, void * field) throw (SerializationException&);

	virtual void enterObject();

	virtual void leaveObject();

	virtual unsigned getArrayLength();

	virtual void enterArray();

	virtual void leaveArray();

	bool isLittleEndian() {
		return littleEndian;
	}

	friend void fieldStreamCallback(BinaryDeserializationContext * ctxt);
};

void fieldStreamCallback(BinaryDeserializationContext * ctxt);

/******************************************************************************
 *
 * Binary Serializer — Base Serializers
 *
 */

template<typename Type, bool word = true>
class RawCopySerializer: public ISerialDef {
	const type_t * type = NULL;

public:
	RawCopySerializer() :
			type(typeOf<Type>()) {
	}

	virtual const type_t * getType() const {
		return type;
	}

	virtual void serialize(SerializationContext & ctxt, void * source) const
			throw (SerializationException&) {
		ctxt.writeValue((uint8_t *) source, sizeof(Type));
	}

	virtual void deserialize(DeserializationContext & ctxt, void * dest) const
			throw (SerializationException&) {
		ctxt.readField(dest, sizeof(Type));

		if (word) {
			/* Byte order conversion */
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
			if (((BinaryDeserializationContext &) ctxt).isLittleEndian()) {
				if (sizeof(Type) == 2) {
					bswap_16(*(uint16_t* )dest);
				} else if (sizeof(Type) == 4) {
					bswap_32(*(uint32_t* )dest);
				} else if (sizeof(Type) == 8) {
					bswap_64(*(uint64_t* )dest);
				}
			}
#else
			if (!((BinaryDeserializationContext &) ctxt).isLittleEndian()) {
				if (sizeof(Type) == 2) {
					bswap_16(*(uint16_t* )dest);
				} else if (sizeof(Type) == 4) {
					bswap_32(*(uint32_t* )dest);
				} else if (sizeof(Type) == 8) {
					bswap_64(*(uint64_t* )dest);
				}
			}
#endif
		}
	}

	virtual void constructInstance(void * field) const {
		*(Type *) field = Type();
	}
};

/**
 * std::string
 */
class stringSerializer: public ISerialDef {
	const type_t * type = NULL;

public:
	stringSerializer() :
			type(typeOf<std::string>()) {
	}

	virtual const type_t * getType() const {
		return type;
	}

	virtual void serialize(SerializationContext & ctxt, void * source) const
			throw (SerializationException&);
	virtual void deserialize(DeserializationContext & ctxt, void * dest) const
			throw (SerializationException&);

	virtual void constructInstance(void * field) const {
		*(std::string *) field = std::string();
	}
};

}

#endif
