#include <osiris/string.h>

#include "osiris/logging.h"

#include <stdarg.h>
#include <ctype.h>
#include <unordered_map>

using namespace osiris;

#ifdef __unix

#ifdef	NDEBUG
static Logger * globalLogger = new UnixTerminalLogger("Default Logger", stderr, LogLevel::Error);
#else
static Logger * globalLogger = new UnixTerminalLogger("Default Logger", stdout, LogLevel::Debug);
#endif

#else

#ifdef	NDEBUG
static Logger * globalLogger = new FileLogger("Default Logger", stderr, LogLevel::Error);
#else
static Logger * globalLogger = new FileLogger("Default Logger", stdout, LogLevel::Debug);
#endif

#endif

void osiris::Logger::log(LogLevel lvl, const char* format, ...) {
	va_list va;

	/* Build the message. */
	va_start(va, format);
	logv(lvl, format, va);
	va_end(va);
}

void osiris::Logger::logv(LogLevel lvl, const char* format, va_list va) {
#ifdef	NDEBUG
	if (lvl == LogLevel::Debug) return;
#endif

	char * data;
	vasprintf(&data, format, va);

	if (!data) {
		data = (char *) "Cannot log message, out of memory!";
	}

	LogMessage message(data, lvl);

	propagateMessage(message);

	free(data);
}

void osiris::Logger::addForwarding(Logger& logger) {
	forwarders.push_back(&logger);
	logger.sources.push_back(this);
}

osiris::Logger::Logger(const_string name) :
				name(name) {
}

osiris::Logger::~Logger() {
	std::list<Logger *>::iterator it;

	for (Logger * l : forwarders) {
		for (it = l->sources.begin(); it != l->sources.end(); it++) {
			if (*it == this) {
				l->sources.erase(it);
				break;
			}
		}
	}

	for (Logger * l : sources) {
		for (it = l->forwarders.begin(); it != l->forwarders.end(); it++) {
			if (*it == this) {
				l->forwarders.erase(it);
				break;
			}
		}
	}
}

void osiris::Logger::propagateMessage(LogMessage& message) {
	if (forwarders.size()) {
		processMessage(message);
		for (Logger * l : forwarders) {
			LogMessage copy = message;
			copy.chain.push_front(this);
			l->processMessage(copy);
			l->propagateMessage(copy);
		}
	} else {
		deliver(message);
	}
}

void osiris::Logger::removeForwarding(Logger& logger) {
	std::list<Logger *>::iterator it;

	for (it = forwarders.begin(); it != forwarders.end(); it++) {
		if (*it == &logger) {
			forwarders.erase(it);
			break;
		}
	}

	for (it = logger.sources.begin(); it != logger.sources.end(); it++) {
		if (*it == this) {
			logger.sources.erase(it);
			break;
		}
	}
}

Logger& osiris::getGlobalLogger() {
	return *globalLogger;
}

void osiris::setGlobalLogger(Logger& logger) {
	std::list<Logger *>::iterator it;

	for (Logger * l : globalLogger->sources) {
		for (it = l->forwarders.begin(); it != l->forwarders.end(); it++) {
			if (*it == globalLogger) {
				*it = &logger;
				break;
			}
		}
	}

	globalLogger = &logger;
}

void osiris::logCritical(const char* fmt, ...) {
	va_list va;
	va_start(va, fmt);
	globalLogger->logv(LogLevel::Critical, fmt, va);
	va_end(va);
}

void osiris::logError(const char* fmt, ...) {
	va_list va;
	va_start(va, fmt);
	globalLogger->logv(LogLevel::Error, fmt, va);
	va_end(va);
}

void osiris::logWarning(const char* fmt, ...) {
	va_list va;
	va_start(va, fmt);
	globalLogger->logv(LogLevel::Warning, fmt, va);
	va_end(va);
}

void osiris::logInfo(const char* fmt, ...) {
	va_list va;
	va_start(va, fmt);
	globalLogger->logv(LogLevel::Info, fmt, va);
	va_end(va);
}

void osiris::logMessage(const char* fmt, ...) {
	va_list va;
	va_start(va, fmt);
	globalLogger->logv(LogLevel::Message, fmt, va);
	va_end(va);
}

static const char * getTimestamp() {
	time_t rawtime;
	struct tm * timeinfo;
	static char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, 80, "%Y-%m-%d %I:%M:%S", timeinfo);

	return buffer;
}

void osiris::FileLogger::deliver(LogMessage& message) {
	if (message.level <= level) {
		fprintf(file, "[%s]", getTimestamp());
		for (Logger * l : message.chain) {
			fprintf(file, "[%s]", l->getName().data());
		}
		fprintf(file, " %s: ", logLevelName(message.level));
		fputs(message.message, file);
		fputc('\n', file);
	}
}

osiris::FileLogger::FileLogger(std::string name, const char* path, LogLevel level)
                                               throw (std::exception) :
				Logger(name), level(level) {
	file = fopen(path, "a");

	if (file == NULL) {
		throw std::exception();
	}
}

osiris::FileLogger::FileLogger(std::string name, FILE* file, LogLevel level) :
				Logger(name), file(file), level(level) {
}

#ifdef __unix__

#define RESET		0
#define BRIGHT 		1
#define DIM			2
#define UNDERLINE 	3
#define BLINK		4
#define REVERSE		7
#define HIDDEN		8

#define BLACK 		0
#define RED			1
#define GREEN		2
#define YELLOW		3
#define BLUE		4
#define MAGENTA		5
#define CYAN		6
#define	WHITE		7

void textcolor(FILE * file, int attr, int fg, int bg) {
	char command[13];

	/* Command is the control command to the terminal */
	sprintf(command, "%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
	fprintf(file, "%s", command);
}

#include <unistd.h>

void osiris::UnixTerminalLogger::deliver(LogMessage& message) {
	if (message.level <= level) {
		if (isATTY) {
			switch (message.level) {
				case LogLevel::Critical:
					fputs("\x1B[1;31m", file);
					break;
				case LogLevel::Error:
					fputs("\x1B[31m", file);
					break;
				case LogLevel::Warning:
					fputs("\x1B[33m", file);
					break;
				case LogLevel::Info:
					fputs("\x1B[34m", file);
					break;
				case LogLevel::Message:
					fputs("\x1B[36m", file);
					break;
				case LogLevel::Debug:
					fputs("\x1B[32m", file);
					break;
			}
		}

		fprintf(file, "[%s]", getTimestamp());
		for (Logger * l : message.chain) {
			fprintf(file, "[%s]", l->getName().data());
		}
		fprintf(file, " %s: ", logLevelName(message.level));
		fputs(message.message, file);
		if (isATTY) {
			fputs("\033[0m", file);
		}
		fputc('\n', file);
	}
}

osiris::UnixTerminalLogger::UnixTerminalLogger(std::string name, const char* path, LogLevel level)
                                                               throw (std::exception) :
				FileLogger(name, path, level) {
	isATTY = isatty(fileno(file));
}

osiris::UnixTerminalLogger::UnixTerminalLogger(std::string name, FILE* file, LogLevel level) :
				FileLogger(name, file, level) {
	isATTY = isatty(fileno(file));
}

#endif
