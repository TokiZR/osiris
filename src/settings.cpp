#include "osiris.h"
#include "osiris/settings.h"

#include <memory>

#include <uchar.h>
#include <errno.h>

using namespace std;

namespace osiris {

typedef SimpleSettings::field field;

struct SettingsParser {
	enum class State {
		KeySeek,
		KeyRead,
		ValueSeek,
		ValueRead,
		String
	};

	State state = State::KeySeek;
	Logger log;
	std::unique_ptr<SimpleSettings> stt;
	field * f = nullptr;
	field * parent = nullptr;
	bool wasString = false;
	unsigned depth = 0;
	char quote = ' ';

	SettingsParser(const_string file = nullptr) :
					log(file == nullptr ? "Settings" : stringFormat("Settings:%s", file)), stt(
					                new SimpleSettings()), f() {
		log.addForwarding(osirisLogger);
	}

	template<typename ... Types>
	void error(const char * fmt, Types ... Args) throw (ParseException *) {
		std::string msg = stringFormat(fmt, Args...);

		log.error("%s", msg.data());

		throw new ParseException(msg);
	}

	void error(const char * s) {
		log.error("%s", s);
		throw ParseException(s);
	}

	void addChild() {
		if (parent) {
			parent->children[f->key] = f;
			f->parent = parent;
		} else {
			stt->data[f->key] = f;
			f->parent = nullptr;
		}

		parent = f;
		wasString = false;
	}

	SimpleSettings * parse(std::string source, unsigned tabwidth = 4) throw (ParseException *) {
		char c;
		for (const char *it = source.data(); *it != 0; it++) {
			c = *it;

			/* The settings format does not accept control characters */
			if (iscntrl(c) && !isspace(c)) {
				error("Unexpected control character 0x%X", c);
			}

			/* Check for line continuation */
			if (c == '\\' && state != State::String && (*(it + 1) == '\n' || *(it + 1) == '\r')) {
				it++;
				continue;
			}

			/* Check for comment */
			if (c == '#') {
				char n;
				do {
					n = *it++;
				} while (n != '\n' && n != '\r' && n != 0);
				it--;
				continue;
			}

			/* Do the parse */
			switch (state) {
				case State::KeySeek: {
					if (c == ' ') {
						depth++;
					} else if (c == '\t') {
						/* This calculates the right tab position */
						depth += tabwidth - depth % tabwidth;
					} else if (!isspace(c)) {
						while (parent != nullptr && depth <= parent->depth) {
							parent = parent->parent;
						}

						f = new field();
						f->depth = depth;
						depth = 0;

						it--;
						state = State::KeyRead;
					}
				}
					break;
				case State::KeyRead: {
					if (c == ':') {
						addChild();
						state = State::ValueSeek;
					} else if (c == '\r' || c == '\n') {
						error("Key '%s' is not followed by a colon and values.", f->key.data());
					} else {
						f->key.append(1, c);
					}
				}
					break;

				case State::ValueSeek: {
					if (!isspace(c)) {
						if (!wasString)
							f->values.emplace_back("");
						if (c == '\'' || c == '"') {
							quote = c;
							state = State::String;
						} else {
							state = State::ValueRead;
							it--;
						}
					} else if (c == '\n' || c == '\r') {
						state = State::KeySeek;
					}
				}
					break;

				case State::ValueRead: {
					if (c == ',') {
						if (f->values.size() == 0 || f->values.back().length() == 0) {
							error("Stray list separator.");
						} else {
							wasString = false;
							state = State::ValueSeek;
						}
					} else if (c == '\n' || c == '\r') {
						state = State::KeySeek;
					} else if (c == '\'' || c == '"') {
						quote = c;
						state = State::String;
					} else {
						f->values.back().append(1, c);
					}
				}
					break;

				case State::String: {
					if (c == quote) {
						wasString = true;
						state = State::ValueSeek;
					} else if (c == '\\') {
						it = hadleEscape(it + 1, f->values.back()) - 1;
					} else {
						f->values.back().append(1, c);
					}
				}
					break;
			}
		}

		if (state == State::KeyRead) {
			log.warn("Ignoring incomplete key '%s'.", f->key.data());
			delete f;
		}

		return stt.release();
	}

	/**
	 * This method handles specifically unicode escape sequences (\u and \U) in a string.
	 * @param  escape A pointer to an escape sequence in a string, this should be the character *after* `\`.
	 * @param  store  Where to store the resulting character.
	 * @return        The position in the string where the escape ended.
	 */
	const char * handleUnicodeEscape(const char * escape, std::string & store, unsigned size = 4) {
		int i;
		int len = 0;
		for (i = 0; i < size; i++)
			if (escape[i] == 0)
				break;

		if (i != size) {
			log.warn("Premature end of stream while parsing short unicode escape");
			escape += len;
			return escape;
		}

		unsigned val = 0;

		for (i = 0; i < size; i++) {
			if (!isxdigit(escape[i])) {
				if (size == 4)
					log.warn("Invalid unicode escape \\u%c%c%c%c.", escape[0], escape[1], escape[2],
					                escape[3]);
				else log.warn("Invalid unicode escape \\U%c%c%c%c%c%c%c%c.", escape[0], escape[1],
				                escape[2], escape[3], escape[4], escape[5], escape[6], escape[7]);

				return escape + i;
			}

			unsigned dig = escape[i];

			if (isdigit(dig)) {
				dig -= '0';
			} else if (isupper(dig)) {
				dig -= 'A' - 10;
			} else {
				dig -= 'a' - 10;
			}

			val = (val << 4) | dig;
		}

		char str[8];

		mbstate_t s = { 0 };

		len = c32rtomb(str, val, &s);
		if (len > 0) {
			str[len] = '\0';
			store.append(str);
		} else {
			log.error(
			                "Error translating unicode character U+%X: %s.\n\t This may be caused by misconfigured locales.",
			                val, strerror(errno));
		}

		return escape + size;
	}

	/**
	 * This method handles escape sequences in a string.
	 * @param  escape A pointer to an escape sequence in a string, this should be the character *after* `\`.
	 * @param  store  Where to store the resulting character.
	 * @return        The position in the string where the escape ended.
	 */
	const char * hadleEscape(const char * escape, std::string& store) {
		switch (*escape++) {
			case 'a':
				store.append(1, (char) 0x07);
				break;
			case 'b':
				store.append(1, (char) 0x08);
				break;
			case 'f':
				store.append(1, (char) 0x0C);
				break;
			case 'n':
				store.append(1, (char) 0x0A);
				break;
			case 'r':
				store.append(1, (char) 0x0D);
				break;
			case 't':
				store.append(1, (char) 0x09);
				break;
			case 'v':
				store.append(1, (char) 0x0B);
				break;
			case '\\':
				store.append(1, (char) 0x5C);
				break;
			case '\'':
				store.append(1, (char) 0x27);
				break;
			case '"':
				store.append(1, (char) 0x22);
				break;
			case '?':
				store.append(1, (char) 0x3F);
				break;

			case '0' ... '7': {
				unsigned value = *(escape - 1) - '0';

				if (isdigit(*(escape))) {
					value = (value << 3) | (*escape - '0');

					escape++;
					if (isdigit(*(escape))) {
						value = (value << 3) | (*escape - '0');
						escape++;
					}
				}

				store.append(1, (char) value);

				break;
			}

			case 'x': {
				char value = 0;
				char dig;

				while (true) {
					dig = *escape++;
					if (isxdigit(dig)) {
						if (isdigit(dig)) {
							dig -= '0';
						} else if (isupper(dig)) {
							dig -= 'A' - 10;
						} else {
							dig -= 'a' - 10;
						}

						value = (value << 4) | dig;
					} else {
						store.append(1, (char) value);
						escape--;
						break;
					}
				}

				break;
			}

			case 'u': {
				escape = handleUnicodeEscape(escape, store, 4);
				break;
			}

			case 'U': {
				escape = handleUnicodeEscape(escape, store, 8);
				break;
			}

		}

		return escape;
	}
};

SimpleSettings * SimpleSettings::parse(std::string source, unsigned tabwidth)
                                                       throw (ParseException *) {
	return SettingsParser().parse(source, tabwidth);
}

}
