#include "osiris.h"

using namespace osiris;

std::string osiris::Object::toString() const {
	return stringFormat("%p", this);
}

void osiris::Object::toString(OutputStream& stream) const {
	stream.printf("%p", this);
}

