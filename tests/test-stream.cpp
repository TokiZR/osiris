#include <limits.h>

#include <osiris.h>

using namespace osiris;

#include <chrono>
using namespace std::chrono;

#define TEST_SIZE 1000000

#define TEST_INT_FORMAT "%d,%o,%u,%x,%X", INT_MAX, INT_MAX, UINT_MAX, UINT_MAX, UINT_MAX
#define TEST_INT_FORMAT_PADDED "%10d,%10o,%10u,%10x,%10X", 255, 255, 255, 255, 255
#define TEST_STRING "foo" //"%s%s%s", "fooooooooooooooooooooooooooooo", "biiiiiiiiiiiiiiiiiiig fooooooooooooooooooooooo", "f"

int main(void) {
	FILE * f = fopen("/dev/null", "w");
	FileOutputStream fos(f);

	time_point<system_clock> start, end;
	double libctime, osiristime;

	puts("Timing libc printf...");

	start = system_clock::now();

	for (unsigned i = 0; i < TEST_SIZE; i++) {
		fprintf(f, TEST_INT_FORMAT);
		fprintf(f, TEST_INT_FORMAT_PADDED);
		fprintf(f, TEST_STRING);
	}

	end = system_clock::now();

	libctime = ((duration<double> ) (end - start)).count();

	puts("Testing osiris time...");

	start = system_clock::now();

	for (unsigned i = 0; i < TEST_SIZE; i++) {
		fos.printf(TEST_INT_FORMAT);
		fos.printf(TEST_INT_FORMAT_PADDED);
		fos.printf(TEST_STRING);
	}

	end = system_clock::now();

	osiristime = ((duration<double> ) (end - start)).count();

	printf("libc: %gs\nosiris: %gs\n", libctime, osiristime);

	return 0;
}
