/*******************************************************************************
 * test-hello.cpp
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <osiris.h>
#include <osiris/event.h>

using namespace osiris;
using namespace std;
namespace o=osiris;

int main(void) {

	class HelloListener: public IEventListener {
		void callback(Event * e, void * data) const {
			e->toString(osiris::stdout);
			o::stdout.printf("; Data: %p\n", data);
		}
	} hello;

	Interactive interactive;

	interactive.addEventListener("foo",
			ListenerAdaptor([](Event * e, void * data) {
				printf("Hello world(Foo)!\n");
			}));

	interactive.addEventListener("bar",
			ListenerAdaptor([](Event * e, void * data) {
				printf("Hello world(Bar)!\n");
			}), NULL, 10);

	for (int i = 0; i < 5; i++) {
		interactive.addEventListener("bar", &hello, (void *) (size_t) i, i);
	}

	interactive.fireEvent(new Event("foo"));
	interactive.fireEvent(new Event("bar"));

	return 0;
}
