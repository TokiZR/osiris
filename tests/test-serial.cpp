/*******************************************************************************
 * test-serial.cpp
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <osiris.h>
#include <iostream>
#include <vector>
#include <deque>

using namespace osiris;
using namespace std;

struct foo: public Serializable<foo> {
	int i;
	float f;
	string s;

	foo() :
			i(0), f(0.0), s("") {
	}

	foo(int i, float f, string s) :
			i(i), f(f), s(s) {
	}
};

SimpleSerialClassDef<foo> fooInfo( {
		SimpleSerialField(foo, int, i),
		SimpleSerialField(foo, float, f),
		SimpleSerialField(foo, string, s)
});

class bar: public Serializable<bar> {
public:
	foo f;
	double boo;

	bar() :
			boo(0.0) {
	}

	bar(foo f, double boo) :
			f(f), boo(boo) {
	}
};

SimpleSerialClassDef<bar> barInfo( {
		SimpleSerialField(bar, foo, f),
		SimpleSerialField(bar, double, boo)
});

void printTabs(unsigned numTabs) {
	while (numTabs-- > 0) {
		putchar('\t');
	}
}

void printType(const type_t * t, unsigned tabLevel = 0) {
	printTabs(tabLevel);
	if (t->isPlain()) {
		printf("%s", t->name().data());
	} else {
		printf("class %s {\n", t->name().data());
		const vector<SerialField *> & fields = *getSerialDef(t)->getFields();
		for (SerialField * f : fields) {
			//printTabs(tabLevel);
			printType(f->getType(), tabLevel + 1);
			printf(" %s;\n", f->getName().data());
		}
		printTabs(tabLevel);
		printf("}");
	}
}

void serializeDeserialize() {
	bar boo(foo(42, 3.1415, "Vader"), 1.6180);

	FileOutputStream fs("boo.dump", "w");

	try {
		serializeToStream(fs, boo);
	} catch (SerializationException & e) {
		fprintf(stderr, "Serialization Error: %s\n", e.what());
	} catch (FileException & e) {
		fprintf(stderr, "IO Error: %s\n", e.what());
	}

	fs.close();

	bar bob;

	FileInputStream fis("boo.dump", "r");

	try {
		deserializeFromStream(fis, bob);
	} catch (SerializationException & e) {
		fprintf(stderr, "Serialization Error: %s\n", e.what());
	} catch (FileException & e) {
		fprintf(stderr, "IO Error: %s\n", e.what());
	}

	printf("bob: {f : {%d, %f, %s}, boo: %lf}\n", bob.f.i, bob.f.f, bob.f.s.data(), bob.boo);
}

void serializeVector() {
	vector<deque<string>> ints({
		{"Hello", "World", "!"},
		{"How", "are", "you", "man", "?"},
		{"I'm", "just", "baffled", "this", "works", "so", "well", "."}
	});

	ByteBufferOutputStream bos;

	try {
		serializeToStream(bos, ints);
	} catch (SerializationException & e) {
		fprintf(stderr, "Serialization Error: %s\n", e.what());
	} catch (FileException & e) {
		fprintf(stderr, "IO Error: %s\n", e.what());
	}

	bos.close();

	vector<deque<string>> desints;

	ByteBufferInputStream bis(bos);

	try {
		deserializeFromStream(bis, desints);
	} catch (SerializationException & e) {
		fprintf(stderr, "Serialization Error: %s\n", e.what());
	} catch (FileException & e) {
		fprintf(stderr, "IO Error: %s\n", e.what());
	}

	for(deque<string> & i : desints) {
		for(string & j : i) {
			printf("%s ", j.data());
		}
		putchar('\n');
	}
}

void types() {
	typeOf<vector<int>>();
	typeOf<vector<double>>();
	typeOf<string>();
	typeOf<ostream>();

	serialManager->dumpTypeInfo();
}

int main(void) {
	osiris::init();

	serializeVector();
	types();

	return 0;
}
