/*******************************************************************************
 * test-string.cpp
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <osiris.h>
#include <iostream>
#include <typeinfo>

#include <osiris/unicode.h>

namespace o = osiris;
using namespace std;

int main(void) {
	int cp = 'c';
	int len = o::utf8_calc_len(cp);

	char buff[len +1];

	o::utf8_encode(buff, cp);

	printf("%s\n", buff);

	o::UTF8Status s = o::utf8_decode(buff, cp, nullptr);

	if(s.isOk()) {
		printf("0x%X\n", cp);
	} else {
		printf ("Error decoding UTF-8 encoded code point: %s\n", s.toString());
	}

	return 0;
}
