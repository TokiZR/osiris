#include <osiris.h>

#include <stdlib.h>
#include <time.h>

using namespace osiris;

void testBasic() {
	logMessage("Just Chillin'.");
	logInfo("Super cool.");
	logWarning("I have a bad feeling.");
	logError("Oh boy.");
	logCritical("Danger! Danger!");
	logDebug("Debug Message");
}

void testHierarchy() {
	srand(time(NULL));

	Logger game("Game");
	Logger game_graphics("Graphics");
	game_graphics.addForwarding(game);
	Logger game_physics("Physics");
	game_physics.addForwarding(game);
	Logger game_ui("UI");
	game_ui.addForwarding(game);

	game.addForwarding(getGlobalLogger());

	Logger wordProcessor("Word Processor");
	Logger wordProcessor_view("View");
	wordProcessor_view.addForwarding(wordProcessor);
	Logger wordProcessor_chart("Chart");
	wordProcessor_chart.addForwarding(wordProcessor);
	Logger wordProcessor_table("Table");
	wordProcessor_table.addForwarding(wordProcessor);

	wordProcessor.addForwarding(getGlobalLogger());

	UnixTerminalLogger eLogger("Errors", ::stderr);

	game.addForwarding(eLogger);
	wordProcessor.addForwarding(eLogger);

	Logger * loggers[] = {&game, &game_graphics, &game_physics, &game_ui, &wordProcessor, &wordProcessor_view,
			&wordProcessor_chart, &wordProcessor_table};

	for (int i = 0; i < 10; i++) {
		loggers[rand() % 8]->log((LogLevel) (rand() % 6), "A random log.");
	}
}

int main(void) {
	testHierarchy();
	return 0;
}
