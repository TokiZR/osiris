/*******************************************************************************
 * test-format.cpp
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <osiris.h>

#include <iostream>

using namespace osiris;
using namespace std;

namespace o = osiris;

template<typename T>
void toStream(OutputStream & ostream, T & t) {
	t.toString(ostream);
}

template<>
void toStream(OutputStream & ostream, int & t) {
	ostream.printf("%d", t);
}

template<>
void toStream(OutputStream & ostream, unsigned & t) {
	ostream.printf("%u", t);
}

template<>
void toStream(OutputStream & ostream, const char * & t) {
	ostream.printf("%s", t);
}

class arghelper {
	OutputStream & ostream;

public:
	arghelper(OutputStream & ostream) :
			ostream(ostream) {
	}

	void printArg(unsigned index) {
		throw "Invalid index!";
	}

	template<typename T, typename ... Args>
	void printArg(unsigned index, T arg0, Args ... args) {
		if (index == 0) {
			toStream(ostream, arg0);
		} else {
			printArg(index - 1, args...);
		}
	}
};

template<typename ... Types>
void format(OutputStream & ostream, const char * fmt, Types ... args) {

	arghelper argh(ostream);

	int i = 0;
	for (; *fmt != '\0'; fmt++) {
		if (*fmt == '%') {
			if (*(fmt + 1) != '%') {
				argh.printArg(i++, args...);
			} else {
				ostream.putchar('%');
				fmt++;
			}
		} else {
			ostream.putchar(*fmt);
		}
	}
}

int main(void) {
	format(o::stdout, "%: %\n", "foo", 42);

	return 0;
}
