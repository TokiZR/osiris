#include <osiris.h>

using namespace osiris;
using namespace std;

void testConnect() {
	try {
		Inet6SocketAddress any(InetAddress6::Any(), 0);
		Inet6SocketAddress localhost(InetAddress6::Loopback(), 80);

		Socket s(&any);

		if (s.connect(&localhost)) {

			InputStream * sin = s.getInputStream();
			OutputStream * sout = s.getOutputStream();

			sout->puts("GET http://localhost:80/ HTTP/1.0\n\r\n\r");
			sout->flush();

			pipe(*sin, osiris::stdout);
			osiris::stdout.putchar('\n');

		} else {
			osiris::stderr.printf("Connection refused\n");
		}

	} catch (exception& e) {
		osiris::stderr.printf("%s\n", e.what());
	}
}

void testServer() {
	try {
		Inet6SocketAddress any(InetAddress6::Any(), 6667);

		ServerSocket svr(&any);

		while (1) {
			Socket * guy = svr.accept();

			osiris::stdout.printf("Got new connection: ");
			osiris::stdout.write(guy->getRemoteAddress());
			osiris::stdout.putchar('\n');

			guy->getOutputStream()->puts("Booooooooooo.....ya!\n");

			guy->close();
			delete guy;
		}

	} catch (exception& e) {
		osiris::stderr.printf("%s\n", e.what());
	}

	perror("Test");
}

int main(void) {
	osiris::init();

	testConnect();

	return 0;
}
