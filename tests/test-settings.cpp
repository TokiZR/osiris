#include <osiris.h>
#include <osiris/settings.h>

using namespace std;

namespace o = osiris;

using o::SimpleSettings;
typedef o::SimpleSettings::field field;

const char * src = R"FOO(
backend:
	name: 'Pluto:Backend:**\n'
	pattern: 'Event: .*\t'
	transform: 
		match: 'Event: (.*)\12\060'
		replace: 'Ev: $0\u2014'
	out: $stdout, foo.log
	format: '[$time]$chain: $type: $message'
	time-format: '%Y-%m-%d %I:%M:%S\xF0\x9F\x8D\xB4'
	chain-format:
		name: '\u03C0$name lowes \U01F355'
		separator: ':'
)FOO";

void pindent(unsigned cnt) {
	for (cnt++; cnt > 0; cnt--)
		putchar('\t');
}

void printField(field & f, unsigned indent = 0) {
	pindent(indent);
	printf("%s: ", f.key.data());
	if (f.values.size() == 0) {
		if (f.children.size() == 0) {
			puts("null");
		} else {
			puts("");
			for (auto ch : f.children) {
				printField(*ch.second, indent + 1);
			}
		}
	} else if (f.values.size() == 1) {
		printf("'%s'\n", f.values[0].data());
	} else {
		auto it = f.values.begin();

		printf("'%s'", it->data());

		for (it++; it != f.values.end(); it++) {
			printf(", '%s'", it->data());
		}
		putchar('\n');
	}
}

int main(void) {
	o::init();

	setlocale(LC_CTYPE, "");

	try {
		SimpleSettings * settings = SimpleSettings::parse(src, 4);

		for (auto f : settings->data) {
			printField(*f.second, -1);
		}

	} catch (exception * e) {
		puts(e->what());
	}

	return 0;
}
