#ifndef __OSIRIS_MATH_H__
#define __OSIRIS_MATH_H__ 1

#include <type_traits>

namespace osiris {

namespace __internal {

inline constexpr signed char __lg(signed char __n) {
	return sizeof(signed char) * __CHAR_BIT__ - 1 - __builtin_clz(__n);
}

inline constexpr unsigned char __lg(unsigned char __n) {
	return sizeof(char) * __CHAR_BIT__ - 1 - __builtin_clz(__n);
}

inline constexpr short __lg(short __n) {
	return sizeof(short) * __CHAR_BIT__ - 1 - __builtin_clz(__n);
}

inline constexpr unsigned short __lg(unsigned short __n) {
	return sizeof(short) * __CHAR_BIT__ - 1 - __builtin_clz(__n);
}

inline constexpr int __lg(int __n) {
	return sizeof(int) * __CHAR_BIT__ - 1 - __builtin_clz(__n);
}

inline constexpr unsigned __lg(unsigned __n) {
	return sizeof(int) * __CHAR_BIT__ - 1 - __builtin_clz(__n);
}

inline constexpr long __lg(long __n) {
	return sizeof(long) * __CHAR_BIT__ - 1 - __builtin_clzl(__n);
}

inline constexpr unsigned long __lg(unsigned long __n) {
	return sizeof(long) * __CHAR_BIT__ - 1 - __builtin_clzl(__n);
}

inline constexpr long long __lg(long long __n) {
	return sizeof(long long) * __CHAR_BIT__ - 1 - __builtin_clzll(__n);
}

inline constexpr unsigned long long __lg(unsigned long long __n) {
	return sizeof(long long) * __CHAR_BIT__ - 1 - __builtin_clzll(__n);
}
}

namespace m {

template<typename Integer>
inline constexpr Integer lg_floor(Integer val) {
	static_assert(std::is_integral<Integer>::value, "lg_floor() only accepts integer arguments!");
	return __internal::__lg(val);

}

template<typename Integer>
inline constexpr Integer lg_ceil(Integer val) {
	static_assert(std::is_integral<Integer>::value, "lg_floor() only accepts integer arguments!");
	Integer lg = __internal::__lg(val);
	return (1 << lg) > val ? lg + 1 : lg;
}

template<typename Integer>
inline Integer div_ceil(Integer dividend, Integer divider) {
	static_assert(std::is_integral<Integer>::value, "div_ceil() only accepts integer arguments!");
	return (dividend + divider - 1) / divider;
}

}

}

#endif
