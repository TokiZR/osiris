/*******************************************************************************
 * object.h
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSIRIS_OBJECT_H__
#define __OSIRIS_OBJECT_H__ 1

namespace osiris {
#ifndef __OSIRIS_STREAM_H__
class OutputStream;
#endif

/**
 * The object base class is simply a more helpful base class for objects much
 * like java's objects.
 *
 * It standardizes methods for string conversion and hashing.
 *
 * This base class implies in the creation of a vtable pointer which may not
 * be desirable for more nimble and fast copyable objects such as small
 * structs of integers or floats. Specially if many instances of these objects
 * will be created.
 *
 * For those scenarios there are global methods that can be overloaded as a
 * convention.
 */
class Object {
public:
	virtual ~Object() {
	}

	/**
	 * @brief Compute a hash value.
	 *
	 * This method computes a hash value for this object.
	 *
	 * Hash values are used on unordered containers.
	 */
	virtual size_t hashValue() const = 0;

	/**
	 * @brief Convert to string.
	 *
	 * Produce a human readable representation of this object as a string.
	 */
	virtual std::string toString() const;

	/**
	 * @brief Write to stream.
	 *
	 * Write a human readable representation of this object to an output
	 * stream.
	 *
	 * This method allows objects to be dumped to streams without any expensive
	 * memory allocations that would be involved in first converting to a
	 * std::string.
	 *
	 * @param stream The stream to write to.
	 */
	virtual void toString(OutputStream & stream) const = 0;

	/**
	 * Create a copy of this object.
	 */
	virtual Object * clone() const = 0;

	template <typename Type>
	static Object * stdClone(const Type * o) {
		return new Type(*o);
	}
};

// TODO: Document and add default to string.
template <typename T>
std::string toString(T & t) {
	return t.toString();
}

}

namespace std {

template <>
struct hash <osiris::Object&> {
	size_t operator()(osiris::Object & obj) {
		return obj.hashValue();
	}
};

template <>
struct hash <osiris::Object *> {
	size_t operator()(osiris::Object * obj) {
		return obj->hashValue();
	}
};

}

#endif
