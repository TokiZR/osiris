/*******************************************************************************
 * poll.h
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSIRIS_POLL_H__
#define __OSIRIS_POLL_H__ 1

#include <vector>

/*********************
 * Polling includes.
 */

namespace osiris {

struct PollObject;

enum class PollableEvent
	: uint8_t {
	/**
	 * Reading would not block.
	 *
	 * This does not mean the process would not sleep while the data is read.
	 * It means that there is data immediately available.
	 */
	In,

	/**
	 * Writing would not block.
	 *
	 * This does not mean the process will not stay trapped in a system call,
	 * rather it means that writing would begin immediately.
	 */
	Out,

	/**
	 * An error happened with the provided pollable object.
	 *
	 * This flag is ignored upon listening.
	 */
	Error,

	/**
	 * The pollable object was closed or the other end of a connection or pipe has been closed.
	 */
	Hangup
};

/**
 * Pollable data streams.
 *
 * A _pollable_ is a data stream which can be checked for activity.
 *
 * For instance a network socket can be polled for available data in a
 * non-blocking fashion.
 *
 * Pollable objects may also be polled in a group to the likes of the `select`
 * POSIX call. This enables for overhead free waiting.
 */
class Pollable {
public:
	virtual ~Pollable() {
	}

	/**
	 * Weather reading from the Pollable object would cause the reader to block.
	 */
	virtual bool wouldblock(PollableEvent e) = 0;

protected:

#ifdef __unix__
	int __pollfd;
#elif _WIN32
		// TODO : Win32 stuff, no idea, just know it is possible
#endif

	friend bool poll(PollObject * plist, unsigned len, double timeout);
	friend bool poll(std::vector<PollObject> plist, double timeout);
};

inline PollableEvent operator |(PollableEvent e1, PollableEvent e2) {
	return (PollableEvent) ((unsigned) e1 | (unsigned) e2);
}

/*class PollGroup{
private:
	public
};*/

struct PollObject {
	Pollable * source;
	PollableEvent listen;
	PollableEvent received;
};

bool poll(PollObject * plist, unsigned len, double timeout);

bool poll(std::vector<PollObject> plist, double timeout);

}

#endif
