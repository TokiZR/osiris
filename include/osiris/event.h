/*******************************************************************************
 * event.h
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSIRIS_EVENT_H__
#define __OSIRIS_EVENT_H__ 1

#include <string>
#include <list>
#include <unordered_map>
#include <memory>

#ifndef __OSIRIS_STRING_H__
#include <osiris/string.h>
#endif

#ifndef __OSIRIS_OBJECT_H__
#include <osiris/object.h>
#endif

namespace osiris {

/* Forward declarations */
class Event;
class Interactive;

/**
 * @ingroup event
 * Identifies a particular event type for an event class.
 */
typedef const char * EventType;

/**
 * @ingroup event
 * The status of a event after being handled.
 */
struct EventStatus {
	/**
	 * Weather event response cancellation was requested.
	 */
	bool canceled :1;

	/**
	 * Weather the handler requested the event to stop being processed at the
	 * current target.
	 */
	bool abort :1;

	/**
	 * Weather the handler requested the event not to propagate to the parent
	 * elements.
	 */
	bool noPropagate :1;

	EventStatus() :
			canceled(false), abort(false), noPropagate(false) {
	}
};

/**
 * @ingroup event
 *
 * @brief A uniform abstraction of the event driven model.
 *
 * When certain conditions are met and object may fire and event which will
 * invoke handlers if any on the listening objects.
 *
 * Events describe 'classes' of events, each consisting of multiple event
 * 'types'. For instance: mouse events are abstracted by the MouseEvent
 * class, but there are multiple mouse event types such as MOUSE_MOVE or
 * MOUSE_DOWN.
 *
 * Types are defined as string constants. When an event handler is
 * registered on a Interactive object it is important to use the constant
 * reference from the event class since the type is checked comparing
 * pointers and not string contents.
 */
class Event: public Object {
private:
	/**
	 * Weather the event processing has been requested to end.
	 */
	bool killed :1;

	/**
	 * Weather the event default reaction has been requested to be ignored.
	 */
	bool canceled :1;

	friend class Interactive;

public:
	/**
	 * Weather or not this event will bubble.
	 *
	 * Bubbling is the concept of, in a hierarchy of objects, the event being
	 * processed by an object being later propagated to the parent object.
	 *
	 * This makes the most sense on Elements since they are organised in a
	 * hierarchy.
	 *
	 * Weather or not an event bubbles is set by this flag. Some events may set
	 * different initial values for it.
	 */
	bool bubbles :1;

	/**
	 * The object which originates this event.
	 *
	 * Sometimes there is only one, non abstracted, source for the event (for
	 * instance the keyboard and mouse). In those cases the source may be null.
	 */
	Interactive * target;

	/**
	 * Type pf the event.
	 */
	EventType type;

	/**
	 * Constructor.
	 */
	Event(EventType _type) :
			killed(false), canceled(false), bubbles(true), target(NULL), type(
					_type) {
	}

	/**
	 * Destructor.
	 */
	virtual ~Event() {
	}

	/**
	 * Converts the information about this event into a printable string.
	 *
	 * @return string representation of this Event.
	 */
	std::string toString() const {
		return stringFormat("Event{type: \"%s\"; bubbles: %s; cancel: %s;}\n",
				type, bubbles ? "true" : "false", canceled ? "true" : "false");
	}

	virtual size_t hashValue() const;
	virtual void toString(OutputStream & stream) const;
	virtual Object * clone() const;

	/**
	 * Some events are notifiers of conditions and are followed by default
	 * reactions (for instance on network communications when a packet arrive
	 * an event is fired (notifying listeners) and a response is sent). In
	 * those cases if this may be called to prevent that reaction.
	 *
	 * It is important to notice that this is a hint and it is reported to the
	 * event source which may or may not cancel the event response.
	 */
	inline void cancel() {
		this->canceled = true;
	}

	/**
	 * Most useful for the first handler of the event.
	 *
	 * This prevents any further propagation of the event including any other
	 * handlers for the same target.
	 *
	 * If used together with cancel it will effectively cause the event to be
	 * ignored.
	 */
	inline void kill() {
		this->killed = true;
	}
};

/**
 * Base class for event callbacks.
 *
 * Event listeners can be implemented through inheritance or through utility
 * subclasses that allow methods, functions or event lambdas to be used as
 * listeners.
 *
 * Because those subclasses instantiate listeners that call third party code
 * they need to be instantiated at runtime and have proper memory management.
 * To that amount the base class provides reference counting. The count starts
 * at 1 but the standard subclasses have it start as 0 (also know as a floating
 * reference) which means the listener is owned by the first object that
 * references it.
 */
class IEventListener {
	friend class EventListener;
	friend class Interactive;

protected:
	uint32_t refs;

public:
	IEventListener() :
			refs(1) {
	}

	virtual ~IEventListener() {
	}

	virtual void callback(Event * e, void * data) const = 0;

	/**
	 * Explicitly reference this listener.
	 *
	 * It is recommended that one use the EventLIstener utility container to
	 * automate the memory management instead.
	 */
	void ref() {
		refs++;
	}

	/**
	 * Explicitly un-reference this listener.
	 *
	 *
	 * @return
	 */
	bool unref() {
		refs--;
		if (refs <= 0) {
			delete this;
			return true;
		}
		return false;
	}
};

/**
 * @brief Safe handler for event listeners.
 *
 * This class facilitates the memory management of event listener objects.
 *
 * It does automatic management of the reference counting mechanism built into
 * the IEventListener abstract class.
 */
class EventListener {
private:
	IEventListener * listener;

	friend class Interactive;

	void operator=(EventListener & l) {
	}
public:
	EventListener(IEventListener * listener) :
			listener(listener) {
		listener->refs++;
	}
	~EventListener() {
		listener->refs--;
		if (listener->refs <= 0) {
			delete listener;
		}
	}

	IEventListener * operator ->() const {
		return listener;
	}

	IEventListener * operator *() const {
		return listener;
	}

	bool operator ==(EventListener & el) const {
		return listener == el.listener;
	}
};

/**
 * Event callback for methods.
 */
template<typename A>
class MethodEventCallback: public IEventListener {
	typedef void (A::*Method)(Event *, void *);

	A * object;
	Method method;

public:
	MethodEventCallback(A * a, Method m) :
			object(a), method(m) {
		refs = 0;
	}

	void callback(Event * e, void * data) const {
		(((A*) object)->*method)(e, data);
	}
};

/**
 * Callback for functions.
 */
class FunctionEventCallback: public IEventListener {
public:
	typedef void (*Function)(Event *, void *);

	FunctionEventCallback(Function f) :
			function(f) {
		refs = 0;
	}

	void callback(Event * e, void * data) const {
		function(e, data);
	}

private:
	Function function;
};

/**
 * Event callback for lambdas.
 */
class LambdaEventCallback: public IEventListener {
public:
	typedef std::function<void(Event *, void *)> Lambda;

	Lambda lambda;

	LambdaEventCallback(Lambda l) :
			lambda(l) {
		refs = 0;
	}

	void callback(Event * e, void * data) const {
		lambda(e, data);
	}
};

/**
 * Generate an event listener for a method call.
 *
 * @param object the object to call the method on.
 * @param method The method to call.
 */
template<typename Object, typename Method>
inline EventListener ListenerAdaptor(Object * object, Method method) {
	return new MethodEventCallback<Object>(object, method);
}

/**
 * Generate an event listener for the provided function.
 *
 * @param function The function to call.
 */
template<typename Function>
inline typename std::enable_if<std::is_function<Function>::value, EventListener>::type ListenerAdaptor(
		Function function) {
	return new FunctionEventCallback(function);
}

/**
 * Generate an event listener for the provided callable object.
 *
 * This method is best used for lambdas.
 *
 * @param lambda The callable to call.
 */
inline EventListener ListenerAdaptor(
		std::function<void(Event *, void *)> lambda) {
	return new LambdaEventCallback(lambda);
}
/**
 * @ingroup event
 * Interactive is an interface that defines objects which have events.
 */
class Interactive {
public:
	/**
	 * Registers a new event listener for a specific event type.
	 *
	 * EventListeners are queued according to their priority and insertion
	 * order. It is recommended that any internal response to an event be set
	 * as an event listener with priority 10. That way applications may prevent
	 * the event handling with a high priority cancelling handler in a
	 * standardised way.
	 *
	 * In cases where cancellation should not be possible the developer should
	 * explicitly document it.
	 *
	 * The same EventListener may be added multiple times. The result is that
	 * that listener will also be called multiple times.
	 *
	 * @param type The type of the event to listen to.
	 * @param data An arbitrary data pointer passed to the listener.
	 * @param listener The function to call when the event happens.
	 * @param priority The priority of the event handler.
	 */
	void addEventListener(EventType type, EventListener listener, void * data =
	NULL, int32_t priority = 0);

	/**
	 * Removes the first occurrence of listener for the provided event type.
	 *
	 * @param type The type the listener has been registered for.
	 * @param listener The event listener to remove.
	 */
	void removeEventListener(EventType type, EventListener listener);

	/**
	 * Trigger an Event on the object.
	 *
	 * @param e The Event to be triggered.
	 * @return The status of the event with regards to it's propagation as
	 * requested by the handlers.
	 */
	virtual EventStatus fireEvent(Event * e);

	/**
	 * Constructor.
	 */
	Interactive();

	/**
	 * Destructor.
	 */
	virtual ~Interactive();

private:
	struct Listener {
		IEventListener * iel;
		void * data;
		int priority;
	};

	typedef std::vector<Listener> ListenerVector;
	typedef std::unordered_map<EventType, ListenerVector> ListenerMap;

	ListenerMap * listeners;
};

}

#endif
