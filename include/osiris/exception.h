#ifndef __OSIRIS_EXCEPTION_H__
#define __OSIRIS_EXCEPTION_H__ 1

#include <exception>

#ifndef __OSIRIS_STRING_H__
#include "string.h"
#endif

namespace osiris {

/**
 * Simple exception class that takes a custom message.
 */
class exception: public std::exception {
private:
	string message;

public:
	exception() _GLIBCXX_USE_NOEXCEPT:
	message("") {
	}

	exception(string message) _GLIBCXX_USE_NOEXCEPT:
	message(message) {
	}

	virtual ~exception() _GLIBCXX_USE_NOEXCEPT {}

	virtual const char* what() const _GLIBCXX_USE_NOEXCEPT {
		return message.data();
	}
};

}

#endif
