#ifndef __OSIRIS__UNICODE_H__
#define __OSIRIS__UNICODE_H__ 1

/**
 * @file Misc functions for handling UTF-8 encoding.
 *
 * This file defines functions that can be used to manipulate UTF-8 encoded
 * characters and additionally any UTF-8 like encoding of 31-bit values (using
 * the extended byte sequences that were removed from UTF-8 via RFC-3629).
 */

namespace osiris {

namespace __utf8 {

/**
 * Get the value of an intermediate byte in a UTF-8 encoded character.
 * @param byte
 * @return
 */
inline int get_intermediate_byte(uint8_t byte) {
	if ((byte & 0xC0) != 0x80) {
		return -1;
	} else {
		return byte & 0x3F;
	}
}

}

struct UTF8Status {
	enum Code {
		OK = 0,
		WARN_OVERLONG = 1,
		ERROR_INVALID_OCTET,
		ERROR_INVALID_CODE_POINT,
		ERROR_TRUNCATED
	} code;
	size_t offendingIndex;

	bool isOk() {
		return code == OK;
	}

	UTF8Status() :
			code(OK), offendingIndex(0) {
	}

	UTF8Status(Code c, size_t index) :
			code(c), offendingIndex(index) {
	}

	static UTF8Status ok() {
		return UTF8Status();
	}

	static UTF8Status overlong(size_t offending) {
		return UTF8Status(WARN_OVERLONG, offending);
	}

	static UTF8Status invalid_octet(size_t offending) {
		return UTF8Status(ERROR_INVALID_OCTET, offending);
	}

	static UTF8Status invalid_code_point(size_t offending) {
		return UTF8Status(ERROR_INVALID_CODE_POINT, offending);
	}

	static UTF8Status truncated(size_t offending) {
		return UTF8Status(ERROR_TRUNCATED, offending);
	}

	const char * toString() const {
		switch (code) {
			case OK:
				return "ok";
			case WARN_OVERLONG:
				return "overlong encoding";
			case ERROR_INVALID_OCTET:
				return "invalid octet";
			case ERROR_INVALID_CODE_POINT:
				return "invalid code point";
			case ERROR_TRUNCATED:
				return "truncated character";
		}

		return "none";
	}
};

/**
 * Return the number of bytes a UTF-8 encoded symbol takes.
 *
 * This function checks the validity of the encoding, but only validates if the
 * encoded code point is valid when `validate` is true.
 *
 * @param  character Pointer to the first byte of the symbol.
 * @param len Where the actual length of the code point is stored. If the
 * encoding has an error the expected length is stored instead.
 * @param maxlen The maximun length of bytes to be read from the string. If a
 * character is missing bytes it is going to be considered invalid.
 * @param validate Validates the character with respect to overlong encodings
 * and invalid code points.
 *
 * @return The length of the symbol in bytes or a negative error code.
 */
inline UTF8Status utf8_len(const char * character, unsigned & len, int maxlen = 6) noexcept(true) {
	int nbytes = 0;
	uint8_t c = *character;

	if (c < 0x80) {
		len = 1;
		return UTF8Status::ok();
	} else if (c < 0xE0) {
		nbytes = 1;
	} else if (c < 0xF0) {
		nbytes = 2;
	} else if (c < 0xF8) {
		nbytes = 3;
	} else if (c < 0xFC) {
		nbytes = 4;
	} else {
		nbytes = 5;
	}

	if (nbytes + 1 > maxlen) return UTF8Status::truncated(0);

	for (int i = 0; i < nbytes; i++) {
		if (__utf8::get_intermediate_byte(character[i + 1]) < 0) {
			return UTF8Status::invalid_octet(i + 1);
		}
	}

	len = nbytes + 1;
	return UTF8Status::ok();
}

/**
 * Calculate the number of bytes a given code point would occupy if encoded in
 * UTF-8.
 *
 * @param  codePoint The code point (any value between 0 and 2^31-1 is valid).
 * @return           The size of the encoded code point or -1 in case the codePoint value was negative.
 */
inline ssize_t utf8_calc_len(int32_t codePoint) {
	if (codePoint < 0)
		return -1;

	if (codePoint < 0x80) {
		return 1;
	} else if (codePoint < 0x800) {
		return 2;
	} else if (codePoint < 0x10000) {
		return 3;
	} else if (codePoint < 0x200000) {
		return 4;
	} else if (codePoint < 0x4000000) {
		return 5;
	} else {
		return 6;
	}
}

/**
 * Return the raw number encoded in UTF-8 Style.
 *
 * Here we say UTF-8 Style because this function does not validate the symbol
 * and it does translate successfully values in the original UTF-8 range of
 * 0&ndash;2^31-1.
 *
 * Beware that this function does not check for boundaries. The caller should
 * first check that with utf8_len().
 *
 * @param  character A pointer to the first byte of the symbol.
 * @param len An optional location  where the length of the symbol is to be
 *            recorded.
 * @return           The value of the symbol or a negative error code.
 */
inline UTF8Status utf8_decode(const char * character, int32_t & codePoint, size_t * __len = nullptr) {
	unsigned length, i;
	UTF8Status stat = utf8_len(character, length);

	if (stat.code != UTF8Status::OK)
		return stat;

	int cp;

	if (length == 1) {
		codePoint = *character;
		return UTF8Status::ok();
	}

	/* Character is ok */

	/* Get the leading byte */
	unsigned mask = (0x1F >> (length - 2));
	cp = ((unsigned)*character++) & mask;

	/* Get the remaining bytes */
	for (i = 1; i < length; i++) {
		cp <<= 6;
		cp |= __utf8::get_intermediate_byte(*character++);
	}

	codePoint = cp;

	if (__len) *__len = length;

	if ((unsigned) utf8_calc_len(cp) == length) {
		return UTF8Status::ok();
	} else {
		return UTF8Status::overlong(0);
	}
}

/**
 * Encode a UCS code point as UTF-8.
 *
 * This function does not validate the code point before encoding.
 *
 * @see utf8_calc_len()
 *
 * @param  buffer    A buffer to store the UTF-8 encoded code point.
 * @param  codePoint The UCS code point to encode.
 * @return           The number of bytes for the encoded symbol.
 */
inline size_t utf8_encode(char * buffer, int32_t codePoint) {
	ssize_t len = utf8_calc_len(codePoint);
	if (len < 0) return 0;

	if(len == 1) {
		*buffer++ = codePoint;
		return len;
	}

	/* Compute leading byte */
	int8_t byte = 0x80;

	byte >>= len - 1;

	/* Set the first few bits on the leading byte. */
	byte |= (codePoint >> ((len - 1) * 6)) & (0x1F >> (len - 2));

	*buffer++ = byte;

	for (len--; len > 0; len--) {
		*buffer++ = 0x80 | ((codePoint >> ((len - 1) * 6)) & 0x3F);
	}

	return len;
}

}

#endif
