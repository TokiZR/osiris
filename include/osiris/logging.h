/*******************************************************************************
 * logging.h
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSIRIS_LOGGING_H__
#define __OSIRIS_LOGGING_H__ 1

#include <assert.h>
#include <stdarg.h>

#include <forward_list>
#include <list>
#include <string>

#ifndef __OSIRIS_EXCEPTION_H__
#include <osiris/exception.h>
#endif

namespace osiris {

/**
 * @addtogroup logging
 * @{
 */

/**
 * @ingroup logging
 * @brief Levels of logging.
 *
 * The log level is the type of the message being logged.
 *
 * Log levels are incremental, enabling a level also enables all levels
 * beneath it.
 */
enum class LogLevel {
	/**
	 * Critical failure.
	 *
	 * Usually only one critical message is thrown as they tend to be caused
	 * by fatal problems that trigger a crash.
	 */
	Critical, //!< Critical

	/**
	 * A recoverable error. Not critical but serious, perhaps performance
	 * detrimental.
	 */
	Error,   //!< Error

	/**
	 * A potentially dangerous or erroneous condition was detected.
	 */
	Warning, //!< Warning

	/**
	 * Helpful status change information.
	 */
	Info,    //!< Info

	/**
	 * A message that would be usually irrelevant to the end user but may be relevant when
	 * debugging.
	 *
	 * Message logs are disabled at compile time when not compiling a debug/dev
	 * build.
	 */
	Message, //!< Message

	/**
	 * A debug message.
	 *
	 * Debug messages are mostly verbose information about the intrinsics of
	 * individual modules and can even reveal vulnerabilities on production
	 * systems.
	 *
	 * For that reason non-debug versions of the library automatically ignore
	 * debug messages.
	 */
	Debug    //!< Debug
};

/**
 * @ingroup logging
 * @brief LogLevel to string.
 *
 * Produces a pretty string representing a log level.
 *
 * @param lvl The level.
 */
inline const char * logLevelName(LogLevel lvl) {
	switch (lvl) {
		case LogLevel::Critical:
			return "Critical";
			break;
		case LogLevel::Error:
			return "Error";
			break;
		case LogLevel::Warning:
			return "Warning";
			break;
		case LogLevel::Info:
			return "Info";
			break;
		case LogLevel::Message:
			return "Message";
			break;
		case LogLevel::Debug:
			return "Debug";
			break;
	}
	assert(!"Invalid Log Level!");
	return NULL;
}

/**
 * @ingroup logging
 * @brief A object that handles information logging.
 *
 * # Logger
 *
 * A logger is an abstract base class for a channel of propagation of log
 * information.
 *
 * # Levels
 *
 * Loggers filter their messages through levels, making sure only messages
 * bellow a given level are reported.
 *
 * The level ordering is as follows:
 *  - Critical
 *  - Error
 *  - Warning
 *  - Info
 *  - Message
 *
 * Where unblocking one level automatically unblocks the levels before it.
 *
 * # Hierarchy
 *
 * Loggers can be organised hierarchically where a message pushed to the bottom
 * level is passed forward to the levels above it. This allows for messages to
 * be filtered on a per module, per library basis.
 *
 * # Multicasting
 *
 * Loggers work much like streams in the end. And to that end they can also be
 * feed to more than one parent logger.
 *
 * This allows for instance for some log levels to be dumped to a file and
 * also have the most imported errors displayed to the user for instance.
 *
 * # Defaults.
 *
 * The standard defined logger is the file logger which simply dumps all
 * messages under a given level to a specified file.
 *
 * It can be retrieved through the getGlobalLogger() method.
 *
 * It can also be reached through the log*() family of global functions.
 *
 * ## Dumb Logger
 *
 * This class does nothing on the processMessage() or on the deliver() methods.
 * This means messages logged through it are never delivered.
 *
 * For that reason the base class is intended to be used as a connection to
 * other higher level loggers.
 */
class Logger {
protected:
	std::string name;

	/**
	 * @brief A log message.
	 *
	 * A log message in propagation.
	 *
	 * When a message is added to the log it is propagated
	 *
	 */
	struct LogMessage {
		/**
		 * @brief The message.
		 *
		 * The message on the log.
		 */
		const_string message;

		/**
		 * @brief List of previous loggers.
		 *
		 * This is a list of all the loggers from the moment the message was
		 * provided till it arrived at the current node.
		 *
		 * This can be used to filter loggers by chain members.
		 */
		std::forward_list<Logger *> chain;

		/**
		 * @brief The level of the message.
		 *
		 * The level of the message.
		 */
		LogLevel level;

		LogMessage(const_string message, LogLevel lvl) :
				message(message), level(lvl) {
		}
	};

	/**
	 * @brief loggers.
	 *
	 * List of loggers who will have the log messages of this logger forwarded
	 * to them.
	 */
	std::list<Logger *> forwarders;

	/**
	 * @brief Filter a message.
	 *
	 * This method can be overridden to allow a logger to filter or process the
	 * log message.
	 *
	 * @param message The logged message.
	 */
	virtual void processMessage(LogMessage & message) {
	}

	/**
	 * @brief Publish a processed log message.
	 *
	 * After a log message has been propagated all the way to a root logger it
	 * has to be delivered to the final medium. This method if fort that.
	 *
	 * @param message The message to deliver.
	 */
	virtual void deliver(LogMessage & message) {
	}

private:
	/**
	 * list of possible sources of messages.
	 */
	std::list<Logger *> sources;

	/**
	 * Propagate a message to all of the forwarders.
	 *
	 * @param message The message to propagate.
	 */
	void propagateMessage(LogMessage & message);

	friend void setGlobalLogger(Logger & logger);

public:
	/**
	 * @brief Create a new logger.
	 *
	 * Instantiate a new logger with a name.
	 *
	 * The name can be used by endpoint loggers to for instance list the names
	 * of the steps the message went through (which could be for example the names
	 * of libraries and modules).
	 *
	 * @param name The name for the logger.
	 */
	Logger(const_string name);

	virtual ~Logger();

	/**
	 * @brief Log a message.
	 *
	 * Write a message to the log.
	 *
	 * @param lvl The log level for the message.
	 * @param format A printf valid format string.
	 * @param ... A sequence of arguments for the format.
	 */
	void log(LogLevel lvl, const char * format, ...)
			__attribute__ ((__format__ (__printf__, 3, 4)));

	/**
	 * @brief Log a message.
	 *
	 * Write a message to the log.
	 *
	 * This version takes a va_list instead of direct variable arguments.
	 *
	 * @param lvl The log level for the message.
	 * @param format A printf valid format string.
	 * @param va A list of arguments for the format.
	 */
	void logv(LogLevel lvl, const char * format, va_list va)
			__attribute__ ((__format__ (__printf__, 3, 0)));

	/**
	 * Shorthand for logging critical messages.
	 */
	void crit(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 2, 3))) {
		va_list args;
		va_start(args, fmt);
		logv(LogLevel::Critical, fmt, args);
		va_end(args);
	}

	/**
	 * Shorthand for logging error messages.
	 */
	void error(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 2, 3))) {
		va_list args;
		va_start(args, fmt);
		logv(LogLevel::Error, fmt, args);
		va_end(args);
	}

	/**
	 * Shorthand for logging warning messages.
	 */
	void warn(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 2, 3))) {
		va_list args;
		va_start(args, fmt);
		logv(LogLevel::Warning, fmt, args);
		va_end(args);
	}

	/**
	 * Shorthand for logging info messages.
	 */
	void info(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 2, 3))) {
		va_list args;
		va_start(args, fmt);
		logv(LogLevel::Info, fmt, args);
		va_end(args);
	}

	/**
	 * Shorthand for logging message messages.
	 */
	void message(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 2, 3))) {
		va_list args;
		va_start(args, fmt);
		logv(LogLevel::Message, fmt, args);
		va_end(args);
	}

	/**
	 * Shorthand for logging debug messages.
	 */
	void debug(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 2, 3))) {
		va_list args;
		va_start(args, fmt);
		logv(LogLevel::Debug, fmt, args);
		va_end(args);
	}

	/**
	 * @brief Add a new forwarded logger.
	 *
	 * Add another logger to the forwarding list for this logger.
	 *
	 * Note that there is a special case for the global logger. If the global
	 * logger is updated and another logger has it on it's forward list then it
	 * will replace the old logger for the new logger on the list.
	 *
	 * @param logger The new forwarded logger.
	 */
	void addForwarding(Logger & logger);

	/**
	 * @brief Remove a forward logger.
	 *
	 * Remove a logger from the list of forwarded loggers.
	 *
	 * @param logger
	 */
	void removeForwarding(Logger & logger);

	/**
	 * @brief Get the name.
	 * Get the name of this logger.
	 */
	inline const_string getName() {
		return const_string(name);
	}
};

/**
 * @ingroup logging
 * @brief Get the global logger.
 *
 * Get a reference to the global logger instance.
 *
 * The global logger can be set using the setGlobalLogger() method.
 *
 * @return The global logger.
 */
Logger & getGlobalLogger();

/**
 * @ingroup logging
 * @brief Set the global logger.
 *
 * Sets the global logger.
 *
 * The global logger is simply a convenient logger instance that can be
 * accessed with shorthand methods. Most classes in hades will use the global
 * logger.
 *
 * @param logger The logger to be used as the global logger.
 */
void setGlobalLogger(Logger & logger);

/**@name Convenience Global Logger Shorthands.
 * @{
 */

/**
 * @brief Log a critical message to the global logger.
 *
 * Log a critical message to the global logger.
 *
 * @param fmt Printf like formatting.
 * @param ... Printf arguments.
 */
void logCritical(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 1, 2)));

/**
 * @brief Log a error message to the global logger.
 *
 * Log a error message to the global logger.
 *
 * @param fmt Printf like formatting.
 * @param ... Printf arguments.
 */
void logError(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 1, 2)));

/**
 * @brief Log a warning message to the global logger.
 *
 * Log a warning message to the global logger.
 *
 * @param fmt Printf like formatting.
 * @param ... Printf arguments.
 */
void logWarning(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 1, 2)));

/**
 * @brief Log a info message to the global logger.
 *
 * Log a info message to the global logger.
 *
 * @param fmt Printf like formatting.
 * @param ... Printf arguments.
 */
void logInfo(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 1, 2)));

/**
 * @brief Log a message to the global logger.
 *
 * Log a message to the global logger.
 *
 * @param fmt Printf like formatting.
 * @param ... Printf arguments.
 */
void logMessage(const char * fmt, ...) __attribute__ ((__format__ (__printf__, 1, 2)));

/**
 * @brief Log a debug message to the global logger.
 *
 * Log a debug message to the global logger.
 *
 * This is defined as a macro so it can have an alternate zero-effect
 * definition for when NDEBUG is defined.
 *
 * @param fmt Printf like formatting.
 * @param ... Printf arguments.
 */
#ifdef	NDEBUG
#define logDebug(fmt, ...)\
	do {}while(0)
#else
#define logDebug(fmt, ...) \
	getGlobalLogger().log(LogLevel::Debug, fmt, ##__VA_ARGS__)
#endif
/**@}*/

/**
 * Intermediate logger that supports advan
 */
class ConfigurableLogFilter {

public:
	/**
	 * Object that filters messages by the names of the loggers.
	 */
	struct NameFilter {
		virtual bool match(std::vector<const_string> nameChain) const = 0;
	};

	/**
	 * Object that filters messages by value.
	 */
	struct MessageFilter {
		virtual bool match(const_string message) const = 0;
	};

	/**
	 * Objects that transforms a message.
	 */
	struct MessageTransformer {
		virtual std::string transform(const_string message) const = 0;
	};

	struct Filter {
		NameFilter * name;
		MessageFilter * message;
		MessageTransformer * transformer;
	};

	Filter * addFilter(NameFilter * nf, MessageFilter * mf, MessageTransformer * mt);

	void removeFilter(Filter * f);

	/**
	 * Creates a new name filter that matches using a simple glob style pattern.
	 *
	 * In the pattern names are separated by colons ':'.
	 *
	 * Valid special symbols are as folows:
	 * - `*`: Match anything on the current name.
	 * - `**`: Match any number of loggers between patterns. This operator
	 * cannot be used within a name.
	 *
	 * The `**` pattern can only be used inbetween colons.
	 *
	 * For example the pattern `Foo:*:Bar` matches the logger chains
	 * `Foo:One:Bar` but not `Foo:One:Two:Bar` and `Foo:Bar` Which would
	 * be matched by `Foo:**:Bar`.
	 *
	 *
	 * @param pattern The pattern to be matched against the logger chain.
	 * @return A name filter that matches based on the pattern.
	 */
	NameFilter * nameGlob(const_string pattern);

	/**
	 * Returns a message filter that matches a message based on a regular
	 * expression.
	 *
	 * The regular expression is compiled into a std::regex with the default
	 * flags.
	 *
	 * @param  regexSource The source of the regular expression.
	 * @return             A message filter that matches if the regex matches.
	 */
	MessageFilter * messageRegex(const_string regexSource);

	/**
	 * Regex based message transformer.
	 *
	 * This transformer will modify the message by replacing all sequences
	 * matched by the provided regular expression with the provided replacement
	 * string.
	 *
	 * @param  match   The match regex.
	 * @param  replace The replacement regex.
	 * @return         The new transformer.
	 */
	MessageTransformer * regexTransformer(const_string match, const_string replace);
};

/**
 * @ingroup logging
 * @brief File Logger.
 *
 * A simple logger that dumps select messages to a file.
 *
 * The messages also receive an additional timestamp and full logger path
 * prepended to them.
 */
class FileLogger: public Logger {
protected:
	/**
	 * @brief The file dumped to.
	 *
	 * The file where the log messages are written.
	 *
	 */
	FILE * file;

	/**
	 * @brief The log level.
	 *
	 * The log level up to which we write messages.
	 */
	LogLevel level;

	virtual void deliver(LogMessage & message);

public:

	/**
	 * @brief Create a new logger.
	 *
	 * Instantiate a new file logger.
	 *
	 * The file is always opened for appending.
	 *
	 * @param name The name for the logger.
	 * @param path The path to a file where to dump the log.
	 * @param level The maximum level of messages to be recorded.
	 */
	FileLogger(std::string name, const char * path, LogLevel level = LogLevel::Error)
			throw (std::exception);

	/**
	 * @brief Create a new logger.
	 *
	 * Instantiate a new file logger.
	 *
	 * @param name The name for the logger.
	 * @param file The file where to dump the log.
	 * @param level The maximum level of messages to be recorded.
	 */
	FileLogger(std::string name, FILE * file, LogLevel level = LogLevel::Error);

	/**
	 * @brief Get the current level.
	 *
	 * Get the current logging level.
	 */
	inline LogLevel getLevel() const {
		return level;
	}

	/**
	 * @brief Set the level.
	 *
	 * Set the current logging level.
	 *
	 * @param level The new level.
	 */
	inline void setLevel(LogLevel level) {
		this->level = level;
	}
};

#ifdef __unix__

/**
 * @ingroup logging
 * @brief Unix file logger.
 *
 * The unix file logger works just like the file logger but it automatically
 * colours the messages if the output file is a VT100 compatible terminal.
 *
 * This class is only available on posix builds. On other systems it is a
 * typedef of the FileLogger.
 */
class UnixTerminalLogger: public FileLogger {
	/**
	 * Weather the file is a tty.
	 */
	bool isATTY;

	virtual void deliver(LogMessage & message);

public:
	/**
	 * @brief Create a new logger.
	 *
	 * Instantiate a new file logger.
	 *
	 * The file is always opened for appending.
	 *
	 * @param name The name for the logger.
	 * @param path The path to a file where to dump the log.
	 * @param level The maximum level of messages to be recorded.
	 */
	UnixTerminalLogger(std::string name, const char * path, LogLevel level = LogLevel::Error)
			throw (std::exception);

	/**
	 * @brief Create a new logger.
	 *
	 * Instantiate a new file logger.
	 *
	 * @param name The name for the logger.
	 * @param file The file where to dump the log.
	 * @param level The maximum level of messages to be recorded.
	 */
	UnixTerminalLogger(std::string name, FILE * file, LogLevel level = LogLevel::Error);
};

#else

typedef FileLogger UnixTerminalLogger;

#endif
/**@}*/

}

#endif
