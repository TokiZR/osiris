#ifndef __OSIRIS_SETTINGS_H__
#define __OSIRIS_SETTINGS_H__ 1

#ifndef __OSIRIS_LOGGING_H__
#include <osiris/logging.h>
#endif

#include <string>
#include <exception>
#include <unordered_map>

namespace osiris {

class ParseException: public std::exception {
private:
	std::string __what;

public:
	ParseException(std::string what) _GLIBCXX_USE_NOEXCEPT : __what(what) {}

	virtual const char* what() const _GLIBCXX_USE_NOEXCEPT {
		return __what.data();
	}
};

class SimpleSettings {
private:

public:
	struct field {
		std::string key;
		std::vector<std::string> values;
		std::unordered_map<const_string, field* > children;

	private:
		unsigned depth;
		field * parent;

		friend struct SettingsParser;
	};

	std::unordered_map<const_string, field *> data;

	static SimpleSettings * parse(std::string source, unsigned tabwidth = 4) throw (ParseException *);
};

}

#endif
