/*******************************************************************************
 * stream.h
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSIRIS_STREAM_H__
#define __OSIRIS_STREAM_H__ 1

#include <stdarg.h>

#include <list>

#ifndef __OSIRIS_STRING_H__
#include <osiris/string.h>
#endif

namespace osiris {

/**
 * @addtogroup stream
 * @{
 */

/**
 * @brief In-coming data flow.
 *
 * An input stream defines the interface to an object that provides a stream
 * of binary or character data.
 *
 * Implementation wise the input stream only provides the read() method. The
 * base class also defines the non-virtual scanf() method which uses the
 * read() method to implement scanf on the stream.
 */
class InputStream {
public:
	virtual ~InputStream() {
	}

	/**
	 * @brief Read a number of bytes from the stream.
	 *
	 * Read a number of bytes from the stream.
	 *
	 * Returns the number of bytes actually read.
	 *
	 * If less than the requested number of bytes is read it means the stream
	 * might be empty.
	 *
	 * @param  buffer   The buffer where to write input data.
	 * @param  buffsize The size of the input buffer.
	 * @return          The number of bytes actually read.
	 */
	virtual ssize_t read(uint8_t * buffer, size_t buffsize) throw (exception&) = 0;

	/**
	 * @brief Scanf
	 *
	 * Performs a scanf like read formatted operation.
	 *
	 * @param fmt The input format.
	 * @param ... variable pointers corresponding to the replacement sequences
	 *            in the format.
	 *
	 * @return The number of fields successfully matched.
	 */
	ssize_t scanf(const char * fmt, ...) throw (exception&);

	/**
	 * Read a string from an input. Th string is read until a null byte is
	 * found or the provided size limit is reached.
	 *
	 * @param maxLen The maximum number of bytes to read.
	 *
	 * @return The string.
	 */
	std::string readString(size_t maxLen = SIZE_MAX) throw (exception&);

	/**
	 * Read a single character from the stream.
	 *
	 */
	int getChar() throw (exception&);

	/**
	 * Skip a number of bytes forward in the stream.
	 *
	 * This is a utility method to skip a number of bytes. It uses the seek
	 * method if the underlying stream supports it. Otherwise it reads data
	 * from the stream until the desired offset is reached.
	 *
	 * Note that in case seeking is not supported by the stream this method can
	 * be quite expensive.
	 */
	void skip(size_t offt) throw (exception&);

	/**
	 * @brief Close the stream.
	 *
	 * On many streams the underlying input is a file or socket or some other
	 * system resource that needs to be properly disposed when no longer in
	 * use.
	 *
	 * Therefore it is an error not to close a stream after using it.
	 *
	 * You can though rely on the destructor to close the stream for you.
	 */
	virtual void close() = 0;
};

/**
 * @brief Out-going data flow.
 *
 * A output stream defines a flow of bytes or characters that is outgoing to
 * some other resource in the system.
 *
 * Output streams are implemented in terms of the write() method. The provide
 * many additional convenience methods for character flow such as puts() and
 * printf().
 *
 * One big difference on output streams is that they contain the flush() method
 * (which has no analogue on the input stream). This method is used to flush
 * any buffered data into the underlying resource.
 *
 * Note that closing the stream also performs a flush operation but one may
 * want to flush the stream before closing.
 *
 * Closing the stream works just like on the InputStream.
 */
class OutputStream {
public:
	virtual ~OutputStream() {
	}

	/**
	 * @brief Write data to the stream.
	 *
	 * Write a number of bytes to the stream.
	 *
	 * @param data A pointer to a buffer containing the data to be written.
	 * @param dlength The number of bytes to be written.
	 *
	 * @return The number of bytes successfully written.
	 */
	virtual ssize_t write(const uint8_t * data, size_t dlength) throw (exception&) = 0;

	/**
	 * @brief Flush buffered data.
	 *
	 * Often streams will buffer part of the data before writing to the
	 * underlying resource so to reduce the number of external calls.
	 *
	 * This method flushes that buffered data to make sure the resource has
	 * received the most recent data.
	 */
	virtual void flush() throw (exception) = 0;

	/**
	 * @brief Write a string to a stream.
	 *
	 * Write a string to the stream.
	 * This does not send the null byte and functions just like the fputs()
	 * call.
	 *
	 * @param string The string to be written.
	 * @return The number of bytes successfully written.
	 */
	ssize_t puts(const_string string) throw (exception&);

	/**
	 * @brief Writes a single character.
	 *
	 * Writes a single character to the stream.
	 *
	 * @param ch The character.
	 * @return 1 if the character has been successfully written.
	 */
	ssize_t putchar(char ch) throw (exception&);

	/**
	 * @brief Write a formatted string.
	 *
	 * Write a printf like formatted string to the stream.
	 *
	 * @param fmt The output format.
	 * @param ... The printf arguments.
	 *
	 * @return The number of bytes written.
	 */
	ssize_t printf(const char * fmt, ...) throw (exception&);

	template<typename ... Types>
	ssize_t format(const char * str, Types ... args) {
		// TODO: Implement type safe format.
		return 0;
	}
	/**
	 * @brief Write a formatted string with a va_list.
	 *
	 * Write a printf like formatted string to the stream.
	 *
	 * This version takes a va_list instead of direct variable arguments.
	 *
	 * @param fmt The output format.
	 * @param va The printf arguments.
	 *
	 * @return The number of bytes written.
	 */
	ssize_t vprintf(const char * ftm, va_list & va) throw (std::exception&);

	/**
	 * Shorthand to write a stringfied object to a stream.
	 */
	inline void write(const Object * object) {
		object->toString(*this);
	}

	/**
	 * @brief Close the stream.
	 *
	 * Release the underlying resource so the system may reclaim it.
	 */
	virtual void close() = 0;
};

/**
 * A Output filter that copies any written data to a list of other outputs.
 */
class OutputSplitter: public OutputStream {
public:
	std::list<OutputStream *> streams;

	OutputSplitter(std::initializer_list<OutputStream*> streams) {
		this->streams = streams;
	}

	/**
	 * Return the amount of data written.
	 *
	 *
	 *
	 * @param data
	 * @param dlength
	 * @return
	 */
	ssize_t write(const uint8_t * data, size_t dlength) throw (exception&) override {
		for (OutputStream * out : streams) {
			out->write(data, dlength);
		}
	}
};

/**
 * Dump all the input data on a stream into an output stream.
 */
inline void pipe(InputStream & is, OutputStream & os) throw (exception) {
	uint8_t buffer[4096];
	ssize_t read;

	while (1) {
		read = is.read(buffer, 4096);
		if (read > 0) {
			os.write(buffer, read);
		}

		if (read < 4096) {
			break;
		}
	}
}

/**
 * @brief A input filter.
 *
 * Defines a intermediate input stream the filters the data coming from
 * another stream.
 */
class InputStreamFilter: public InputStream {
protected:
	InputStream & src;

	/**
	 * @brief Construct a stream filter.
	 *
	 * @param base The stream to be filtered.
	 */
	InputStreamFilter(InputStream & base) :
					src(base) {
	}

public:
	virtual void close() {
		src.close();
	}
};

/**
 * @brief A output filter.
 *
 * Defines a intermediate output stream the filters the data going to another
 * stream.
 */
class OutputStreamFilter: public OutputStream {
protected:
	OutputStream & dest;

	/**
	 * @brief Construct a stream filter.
	 *
	 * @param base The stream to be filtered.
	 */
	OutputStreamFilter(OutputStream & base) :
					dest(base) {
	}

public:

	virtual void close() {
		dest.close();
	}

	virtual void flush() throw (exception) {
		dest.flush();
	}
};

/**
 * @brief Where to seek from.
 *
 * Whence defines three positions to seek from on a stream.
 * This derives from the standard C API.
 */
enum class Whence {
	Start,	//!< From the start of the stream.
	End,	//!< From the end of the stream.
	Current	//!< From the current position.
};

/**
 * @brief Seekable base interface.
 *
 * Seekable defines the interface to any stream that supports seeking.
 *
 */
class Seekable {
public:

	virtual ~Seekable() {
	}

	/**
	 * @brief Seek the stream.
	 *
	 * Go to a different position on the stream.
	 *
	 * @param offset The offset from the stream position.
	 * @param w Where to take the offset from.
	 *
	 * @return The resulting stream position counting from 0 at the start.
	 */
	virtual size_t seek(ssize_t offset, Whence w) throw (exception&) = 0;

	/**
	 * @brief Get the current position.
	 *
	 * Get the current position on the stream.
	 */
	virtual size_t tell() throw (exception&) = 0;

	/**
	 * @brief Get the size of the stream.
	 *
	 * Get the total number of bytes on the stream.
	 */
	virtual size_t size() throw (exception&) = 0;
};

/**
 * A file exception.
 *
 * This exception is generated if any errors are detected while treating a file
 * input or output stream.
 *
 * The error code is one defined by the standard C library from the errno.h
 * header.
 */
class FileException: public exception {
private:
	int err_no;
public:

public:
	/**
	 * @brief Constructor.
	 *
	 * Construct a new file exception.
	 *
	 * @param error The error code.
	 */
	FileException(int error) :
					exception(""), err_no(error) {
	}

	/**
	 * @brief Get the error message.
	 *
	 * Get a localised error message for the exception.
	 */
	virtual const char* what() const noexcept {
		return strerror(err_no);
	}

	/**
	 * @brief Get the error number.
	 *
	 * Get the error number.
	 */
	inline int getErrno() {
		return err_no;
	}
};

/**
 * @brief File output stream.
 *
 * Allows data streams do be written to files.
 *
 * Also supports seeking.
 */
class FileOutputStream: public OutputStream, public Seekable, public Pollable {
private:
	FILE * file;

	bool owns;
public:

	/**
	 * @brief Create a new file output stream for a path.
	 *
	 * Instantiate a new file output stream.
	 *
	 * The stream is initialised to the provided file which is opened with the
	 * provided access mode.
	 *
	 * The access mode has the same syntax as that of fopen();
	 *
	 * @param path The file path.
	 * @param mode The file mode.
	 * @throw FileException Thrown in case an error occurs while opening the
	 * 			file.
	 */
	FileOutputStream(const_string path, const char * mode) throw (FileException);

	/**
	 * @brief Create a new file output stream for a file.
	 *
	 * Instantiate a new file output stream.
	 *
	 * This version expects an already opened file that is writable.
	 *
	 * @param file The file to create the stream for.
	 */
	FileOutputStream(FILE * file, bool owned = false);
	~FileOutputStream();

	virtual ssize_t write(const uint8_t * data, size_t dlength) throw (exception&);
	virtual void flush() throw (exception);

	virtual void close();

	virtual size_t seek(ssize_t offset, Whence w) throw (exception&);
	virtual size_t tell() throw (exception&);
	virtual size_t size() throw (exception&);

	virtual bool wouldblock(PollableEvent e);
};

/**
 * @brief File input stream.
 *
 * Allows data streams do be read from files.
 *
 * Also supports seeking.
 */
class FileInputStream: public InputStream, public Seekable, public Pollable {
private:
	FILE * file;

	/**
	 * Weather the stream own the file.
	 */
	bool owns;
public:

	/**
	 * @brief Create a new file input stream for a path.
	 *
	 * Instantiate a new file input stream.
	 *
	 * The stream is initialised to the provided file which is opened with the
	 * provided access mode.
	 *
	 * The access mode has the same syntax as that of fopen();
	 *
	 * @param path The file path.
	 * @param mode The file mode.
	 * @throw FileException Thrown in case an error occurs while opening the
	 * 			file.
	 */
	FileInputStream(const_string path, const char * mode) throw (FileException);

	/**
	 * @brief Create a new file input stream for a file.
	 *
	 * Instantiate a new file input stream.
	 *
	 * This version expects an already opened file that is readable.
	 *
	 * @param file The file to create the stream for.
	 */
	FileInputStream(FILE * file, bool owned = false);
	~FileInputStream();

	virtual ssize_t read(uint8_t * buffer, size_t buffsize) throw (exception&);

	virtual void close();

	virtual size_t seek(ssize_t offset, Whence w) throw (exception&);
	virtual size_t tell() throw (exception&);
	virtual size_t size() throw (exception&);

	virtual bool wouldblock(PollableEvent e);
};

/**
 * @brief Write to a in memory buffer.
 *
 * The byte buffer output stream is a stream that stores data written to it
 * inside a in memory buffer.
 *
 * Both close() and flush() methods have no effect on the stream.
 */
class ByteBufferOutputStream: public OutputStream {
private:
	std::vector<uint8_t> * dataBuffer;

public:
	/**
	 * @brief Constructor.
	 *
	 * Create a new byte buffer output stream.
	 *
	 * The stream is created with a provided initial buffer size.
	 * A good guess in the initial buffer size can reduce substantially the
	 * number of allocations and the overall overhead of writing to this
	 * stream.
	 *
	 * @param initialSize The initial number of bytes on the buffer.
	 */
	ByteBufferOutputStream(size_t initialSize = 256) {
		dataBuffer = new std::vector<uint8_t>();
		dataBuffer->reserve(initialSize);
	}

	/**
	 * @brief Return the data on the stream.
	 *
	 * Get the data on the stream.
	 */
	const std::vector<uint8_t> * getData() {
		return dataBuffer;
	}

	/**
	 * @brief Get the ownership of the data on the stream.
	 *
	 * Unlike getData() this method effectively transfers the ownership of the
	 * buffered data to the caller. This means the data will not be destroyed
	 * when the stream is.
	 *
	 * As a result the stream (lazily) allocates a new buffer for itself so the
	 * object that it can be re-used.
	 */
	std::vector<uint8_t> * obtainData() {
		std::vector<uint8_t> * d = dataBuffer;
		dataBuffer = NULL;
		return d;
	}

	/**
	 * @brief Get the size of the buffer.
	 *
	 * Get the number of bytes _written_ to the buffer.
	 */
	size_t getSize() {
		return dataBuffer ? dataBuffer->size() : 0;
	}

	virtual ssize_t write(const uint8_t * data, size_t dlength) throw (exception&);
	virtual void flush() throw (exception);
	virtual void close();
};

/**
 * @brief In memory readable buffer.
 *
 * This stream encapsulates a in memory buffer in a readable stream.
 *
 * The stream does not take ownership of the buffer and does not destroy it.
 */
class ByteBufferInputStream: public InputStream {
private:
	/**
	 * The buffer.
	 */
	const uint8_t * buffer;

	/**
	 * The size of the buffer.
	 */
	size_t size;

	/**
	 * The current position on the buffer.
	 */
	size_t cursor;

public:

	ByteBufferInputStream(ByteBufferOutputStream & bos) :
					ByteBufferInputStream(bos.getData()->data(), bos.getSize()) {
	}

	ByteBufferInputStream(std::vector<uint8_t> & buff) :
					ByteBufferInputStream(buff.data(), buff.size()) {
	}

	ByteBufferInputStream(const uint8_t * buffer, size_t size) :
					buffer(buffer), size(size), cursor(0) {
	}

	ByteBufferInputStream(const_string str) :
					buffer((const uint8_t *) str.data()), size(str.length()), cursor(0) {
	}

	ByteBufferInputStream() :
					ByteBufferInputStream(NULL, 0) {
	}

	virtual ssize_t read(uint8_t * buffer, size_t buffsize) throw (exception&);
	virtual void close();

	size_t bytesLeft() {
		return size - cursor;
	}
};

/**
 * Limited Input Stream Filter.
 *
 * This filter will only allow a provided number of bytes to be written to a
 * stream. Once the limit is reached the filter reports the stream to be empty
 * even if the filtered stream is not.
 *
 * Additionally when the provided limit is reached or the filter is closed a
 * callback is called.
 *
 * When the filter is closed it does *not* close the filtered stream.
 *
 * As a peculiarity when the provided limit is zero the callback will *not* be
 * called.
 */
class CappedInputStream: public InputStreamFilter {
public:
	/**
	 * @brief A callback for when the stream is finished.
	 *
	 * When the filter is closed or the limit is reached this callback will be
	 * called.
	 *
	 * @param data A provided user data.
	 *             On some systems the 'this' argument on method calls is
	 *             always the first argument such that a member function could
	 *             then be passed as a callback.
	 * @param filter The CappedInputStream that got finished.
	 */
	typedef void (*Callback)(void * data, CappedInputStream * filter);

	/**
	 * @brief Constructor.
	 *
	 * Construct a new CappedInputStream.
	 *
	 * This stream will be limited to the provided number of bytes. Once the
	 * limit is reached or the filter is closed the provided callback will be
	 * called, passing the provided user data to it.
	 *
	 * @param s The stream to be filtered.
	 * @param limit the maximum number of bytes to be read.
	 * @param cb The callback, can be NULL.
	 * @param userCbData User data passed to the provided callback.
	 */
	CappedInputStream(InputStream & s, size_t limit, Callback cb, void * userCbData) :
					InputStreamFilter(s), callback(cb), userCbData(userCbData), limit(limit) {
	}

	virtual ssize_t read(uint8_t * buffer, size_t buffsize) throw (exception&);
	virtual void close();

	inline size_t getLimit() {
		return limit;
	}

private:
	/**
	 * The callback.
	 */
	Callback callback;

	/**
	 * The user data.
	 */
	void * userCbData;

	/**
	 * The read limit.
	 *
	 * This value is decremented as the stream is read.
	 */
	size_t limit;
};

extern InputStream & stdin;
extern OutputStream & stdout;
extern OutputStream & stderr;

/** @} */

}

#endif
