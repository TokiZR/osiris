/*******************************************************************************
 * network.h
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSIRIS_NETWORK_H__
#define __OSIRIS_NETWORK_H__ 1

#include <type_traits>

#ifdef __unix__
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>

#define MAX_UNIX_SOCKET_SIZE 107

class SocketStream;

struct sockaddr_info {
	sockaddr * sa = NULL;
	socklen_t size = 0;

	sockaddr_info() :
			sa(NULL), size(0) {
	}

	sockaddr_info(sockaddr_info && other) {
		sa = other.sa;
		size = other.size;

		other.sa = NULL;
		other.size = 0;
	}

	sockaddr_info& operator =(sockaddr_info && other) {
		sa = other.sa;
		size = other.size;

		other.sa = NULL;
		other.size = 0;
		return *this;
	}

	sockaddr_info& operator =(sockaddr_info & other) {
		sa = other.sa;
		size = other.size;

		other.sa = NULL;
		other.size = 0;
		return *this;
	}

	~sockaddr_info() {
		if (sa)
			free(sa);
	}
};

#define SOCKET_DATA \
		int sockfd;\
		struct sockaddr_info sockaddr;
#endif

#ifdef _Win32
#include <windows.h>
#define SOCKET_DATA SOCKET sock;
#endif

namespace osiris {

/**
 * @addtogroup net
 * @{
 */

/**
 * Logger for network operations.
 *
 * This logger is only used when unexpected conditions are reached.
 */
extern Logger networkLogger;

/**
 * The exception thrown on socket errors.
 *
 * This exception identifies problems with network connections.
 */
class NetworkException: public exception {
public:

	/**
	 * The cause of the exception.
	 */
	enum Cause {
		/**
		 * An unknown error occurred.
		 *
		 * This is most likely caused by either a library or system bug.
		 */
		Unknown,

		/**
		 * The user does not have the permissions to open the specified socket
		 * type.
		 */
		PermissionDenied,

		/**
		 * The provided address is invalid.
		 */
		InvalidAddress,

		/**
		 * The current platform does not support the specified socket protocol
		 * or type.
		 */
		Unsupported,

		/**
		 * The selected port is already taken.
		 */
		AddrInUse,

		/**
		 * The associated system resource has been exhausted (memory, file
		 * descriptors, ports, etc).
		 */
		Exhausted,

		/**
		 * The specified remote address is unreachable.
		 */
		Unreachable,

		/**
		 * Connecting or communicating with a remote host resulted in a timeout.
		 */
		Timeout,

		/**
		 * A path to a UNIX domain socket is invalid.
		 *
		 * See the log for details.
		 */
		InvalidPath,

		/**
		 * Socket is closed.
		 */
		Closed,

		/**
		 * The socket is not connected.
		 */
		Disconnected,

		/**
		 * Connection was reset.
		 */
		Reset
	};

private:
	Cause cause;

public:

	NetworkException(Cause cause) {
		this->cause = cause;
	}

	virtual const char * what() const noexcept;
};

/**
 * A address to another machine.
 *
 * Usually this is an IP address but other network protocols may also be
 * supported.
 */
class InetAddress: public Object {
protected:
	InetAddress() {
	}
};

/**
 * Base for IP addresses.
 *
 * Base class for IPv4 and IPv6 Addresses.
 */
class InetIpAddress: public InetAddress {
protected:
	InetIpAddress() {
	}
};

/**
 * An IPv4 Address.
 */
class InetAddress4: public InetIpAddress {
private:
	uint32_t addr;

public:
	InetAddress4(const_string addr);

	InetAddress4(uint32_t addr) {
		this->addr = addr;
	}

	void setAddress(uint32_t addr) {
		this->addr = addr;
	}

	uint32_t getAddress() const {
		return addr;
	}

	virtual size_t hashValue() const;
	virtual std::string toString() const;
	virtual void toString(OutputStream & stream) const;

	virtual Object * clone() const {
		return Object::stdClone(this);
	}

	static const InetAddress4& Any();

	static const InetAddress4& Loopback();
};

/**
 * A raw IPv6 address value.
 *
 * Stored in network byte order.
 */
struct in6addr {
#ifdef __unix__
	in6addr(const in6_addr& addr) {
		memcpy(&this->addr, &addr, 16);
	}
#endif

	in6addr() {
	}

	uint8_t addr[16];
};

/**
 * A IPv6 Address.
 */
class InetAddress6: public InetIpAddress {
private:
	in6addr addr;

public:

	InetAddress6(const_string addr);

	InetAddress6(in6addr addr) {
		this->addr = addr;
	}

	void setAddress(in6addr addr) {
		this->addr = addr;
	}

	in6addr getAddress() const {
		return addr;
	}

	virtual size_t hashValue() const;
	virtual std::string toString() const;
	virtual void toString(OutputStream & stream) const;
	virtual Object * clone() const {
		return Object::stdClone(this);
	}

	static const InetAddress6& Any();

	static const InetAddress6& Loopback();
};

/**
 * A object that defines an Internet address.
 */
struct InetAddressResolution {
	std::string name;
	std::vector<const InetIpAddress *> addrs;
	uint16_t port;
};

/**
 * Lookup the ip address of a name.
 *
 * @param  name The name to look up.
 * @return      The looked up information
 */
InetAddressResolution lookupName(const_string name);

/**
 * Base class to identifiers for remote sockets.
 */
class SocketAddress: public Object {
};

/**
 * An address of an IP socket.
 */
template<typename AddressType>
struct InetSocketAddress: public SocketAddress {
	AddressType addr;
	uint16_t port;

	InetSocketAddress(const AddressType & addr, uint16_t port = 0) :
			addr(addr), port(port) {
		static_assert(std::is_convertible<AddressType&, InetAddress&>::value, "AddressType must be an InetAddress.");
	}

	virtual size_t hashValue() const {
		return this->addr.hashValue() + this->port;
	}

	virtual std::string toString() const {
		return stringFormat("%s:%hu", this->addr.toString().data(), this->port);
	}

	virtual void toString(OutputStream & stream) const {
		this->addr.toString(stream);
		stream.printf(":%hu", this->port);
	}

	virtual Object * clone() const {
		return new InetSocketAddress<AddressType>(*this);
	}
};

typedef InetSocketAddress<InetAddress4> Inet4SocketAddress;
typedef InetSocketAddress<InetAddress6> Inet6SocketAddress;

/**
 * The address of an Unix Domain Socket.
 */
class UnixSocketAddress: public SocketAddress {
private:
	std::string pathName;

public:

	UnixSocketAddress(const_string path) {
		setPath(path);
	}

	void setPath(const_string path) {
		pathName.assign(path.begin(), path.begin() + MAX_UNIX_SOCKET_SIZE);
	}

	const const_string getPath() const {
		return pathName;
	}

	virtual size_t hashValue() const;
	virtual std::string toString() const;
	virtual void toString(OutputStream & stream) const;
	virtual Object * clone() const {
		return Object::stdClone(this);
	}
};

/**
 * A Stream Socket.
 *
 * For InternetAddresses this is a TCP connection.
 */
class Socket {
private:
	SocketAddress * localAddr;
	SocketAddress * remoteAddr;

	SOCKET_DATA;
	SocketStream * sstream;

	friend class ServerSocket;

	/**
	 * Dummy constructor for server sockets.
	 */
	Socket(SocketAddress * local, SocketAddress * remote);

public:
	Socket(const SocketAddress * local) throw (NetworkException);
	~Socket();

	bool connect(const SocketAddress * addr) throw (NetworkException);

	bool isConnected() const;

	/**
	 * If the socket is either disconnected or is not a stream socket this call
	 * returns null.
	 * @return
	 */
	InputStream * getInputStream() const;
	OutputStream * getOutputStream() const;

	const SocketAddress * getLocalAddress() const {
		return localAddr;
	}

	const SocketAddress * getRemoteAddress() const {
		return remoteAddr;
	}

	void disconnect();

	void close();
};

/**
 * A Stream Server Socket.
 *
 * For InternetAddresses this is a TCP connection.
 *
 * The server socket works as an entry point for incoming connections, yet no
 * data traffics directly through it. Instead each accepted connection is bound
 * to a new system created socket.
 *
 * This actually implies a restriction on the number of connections since each
 * open connection takes a port number (limited to about 65000 of them) and a
 * socket.
 *
 * For instance on *nix any socket takes a file descriptor and by default there
 * are only 1024 file descriptors.
 */
class ServerSocket {
	SocketAddress * boundAddr;

	SOCKET_DATA;

public:

	ServerSocket(const SocketAddress * addr, uint32_t queueSize = 64) throw (NetworkException);
	~ServerSocket();

	/**
	 * Accept a client connection.
	 *
	 * This call blocks until there is an incoming connection request.
	 *
	 * @return A socket for the remote client. This socket may only be closed.
	 */
	Socket * accept() const throw (NetworkException);

	/**
	 * Close the server.
	 *
	 * Once closed the server socket may be rebound to a different address.
	 */
	void close();

	const SocketAddress * getAddress() const {
		return boundAddr;
	}
};

/**
 * Datagram Socket.
 *
 * Datagram sockets define a connectionless, block oriented connection where
 * there are no guarantees on delivery or ordering of packets.
 *
 * Data is sent one packet at a time, any sent data beyond the maximum packet
 * size is discarded.
 *
 * Any data not read in the first call to recv is also discarded.
 */
class DatagramSocket {
private:
	SocketAddress * laddr;
	SocketAddress * defaultRoute;

	SOCKET_DATA;
	SocketStream * sstream;

public:
	/**
	 * Create a new datagram socket.
	 *
	 * Create a new dtagram socket.
	 *
	 * Datagram sockets employ a much simpler packet oriented connection which
	 * does not provide any delivery or packet ordering guarantees that normal
	 * sockets have.
	 *
	 * When the connection can be trusted datagrams tend to be much faster and
	 * lightweight than connection oriented sockets.
	 *
	 * Datgram sockets also can be used to establish connections with an
	 * unlimited number of peers since they use only a single port unlike
	 * regular server sockets.
	 *
	 * @param local The local bind address.
	 */
	DatagramSocket(const SocketAddress * local) throw (NetworkException);
	~DatagramSocket();

	/**
	 * Set the default destination of datagrams.
	 *
	 * This method is used to provide a default destination for any send calls
	 * when the destination argument is not provided.
	 *
	 * @param addr The new default address, use NULL to clear.
	 */
	void setDefaultRoute(const SocketAddress * addr);

	/**
	 * Receive a message from a peer.
	 *
	 * This call reads part of a datagram packet.
	 *
	 * The number of bytes on the actual packet is returned. The number of
	 * bytes actually read is the minimum of the packet size and the size of
	 * the input buffer.
	 *
	 * If the input buffer is smaller than the size of the packet any
	 * remaining data is discarded. If the buffer is bigger than the packet
	 * the extra bytes on the buffer are left as they where before the call to
	 * recv.
	 *
	 * @param buffer Where to write the read packet data.
	 * @param size The size of the buffer.
	 * @param remote A socket address of the appropriate type where to store the
	 * 				address of the remote peer.
	 *
	 * @return The number of bytes on the packet.
	 */
	uint32_t recv(void * buffer, uint32_t * size, SocketAddress * remote) throw (NetworkException);

	/**
	 * Send data to a remote host.
	 *
	 * The data is sent in a single packet, the number of bytes written is
	 * returned. if the maximum packet size is less than the size of the
	 * buffer the extra bytes are discarded.
	 *
	 * @param  buffer A buffer containing data to be sent.
	 * @param  bsize  the size of the data buffer.
	 * @param  dest   The destination address.
	 * @return        The number of bytes written to the packet.
	 */
	uint32_t send(void * buffer, uint32_t bsize, const SocketAddress * dest = NULL)
			throw (NetworkException);

	void close();

	const SocketAddress * getAddress() const {
		return laddr;
	}
};

/**@}*/

}

#endif
