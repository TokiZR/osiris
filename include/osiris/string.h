/*******************************************************************************
 * string.h
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSIRIS_STRING_H__
#define __OSIRIS_STRING_H__ 1

#include <stddef.h>

#include <string>
#include <string.h>
#include <wchar.h>

#include <algorithm>
#include <exception>

namespace osiris {

class string_exception: public std::exception {
	virtual const char* what() const _GLIBCXX_USE_NOEXCEPT {
		return "String exception!";
	}
};

/**
 * Templated definition of strlen.
 *
 * Returns the number of characters (rather encoding units (not symbols) in a string).
 */
template <typename CharT>
inline size_t strlen(const CharT * str) {
	size_t cnt;

	for (cnt = 0; *str != 0; cnt++, str++)
		;

	return cnt;
}

template <>
inline size_t strlen<char>(const char * str){
	return ::strlen( str);
}

template <>
inline size_t strlen<wchar_t>(const wchar_t * str){
	return wcslen((const wchar_t *) str);
}


/**
 * Templated definition of memset.
 *
 * Sets each character (encoding unit) on a string to a provided value.
 *
 * Does not set null terminators.
 */
template <typename CharT>
inline CharT * memset(CharT * str, CharT value, size_t count){
	for(count++; count > 0; count--)
		*str++ = value;
}

template <>
inline char * memset<char>(char * str, char value, size_t count){
	return memset((char *) str, value, count);
}

template <>
inline wchar_t * memset<wchar_t>(wchar_t * str, wchar_t value, size_t count){
	return wmemset((wchar_t *) str, value, count);
}

struct resize_policy {
	struct Quadractic {
		size_t operator()(size_t has, size_t wants) {
			//size_t div = (wants + has - 1) / has;
			// TODO: Fix Me
			return wants;
		}
	};

	struct Linear {
		size_t operator()(size_t has, size_t wants) {
			return wants;
		}
	};
};

/**
 * Create a new formated string.
 *
 * @param fmt The base format, printf style.
 * @return The newly created string.
 */
std::string stringFormat(const char * fmt, ...);

/**
 * Compare two strings.
 */
struct cmpString {
	bool operator()(const char * __x, const char * __y) const {
		return strcmp(__x, __y) == 0;
	}
};

/**
 * Create a hash for a string.
 *
 * @param str The string to hash.
 * @return The hash for the string.
 */
struct hashString {
	size_t operator()(const char * str) const;
};

}

#include <osiris/bits/basic_string.h>
#include <osiris/bits/const_string.h>

namespace osiris {

/**
 * @brief Demangle a C++ name.
 *
 * This function takes a mangled c++ name (such as that from typeid::name())
 * and returns a demangled name.
 *
 * @param  name The name to demangle.
 */
std::string demangleName(const_string name);

}



#endif
