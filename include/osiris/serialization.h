/*******************************************************************************
 * serialization.h
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSIRIS_SERIALIZATION_H__
#define __OSIRIS_SERIALIZATION_H__ 1

#include <typeindex>

#include <stddef.h>

#include <deque>
#include <unordered_map>
#include <initializer_list>
#include <type_traits>

namespace osiris {
/* Forward declarations. */
class ISerialDef;
class SerialField;
class SerializationBackend;
class SerializationManager;
/**
 * Specialise this template to implement template serializers.
 */
template<template<typename ...> class Template, typename... Args>
class TemplateSerializer;

#define IgnoreSerializerSpecialization typedef void IgnoreSerializerSpecializationFlag

/**@addtogroup serial
 * @{
 */

/******************************************************************************
 *
 *	Type Information.
 *
 */

/**
 * Abstract descriptor of a type.
 */
class TypeDefinition: public Object {
protected:
	TypeDefinition() {
	}
public:
	/**
	 * @brief Get the Name.
	 * @details Get the name of the type.
	 */
	virtual const_string name() const = 0;

	/**
	 * @brief Weather the type is a plain class.
	 *
	 * In this context a plain class means a class that presents itself as a
	 * unit or as a primitive type and exposes no fields.
	 *
	 * An example of a non-POD plain class is a std::string or an iterator.
	 *
	 * More importantly plain classes expose no serializable fields and instead
	 * serialize as a single value.
	 *
	 * @return Weather this type is a Plain type.
	 */
	virtual bool isPlain() const = 0;

	virtual size_t hashValue() const = 0;

	virtual std::string toString() const = 0;

	virtual void toString(OutputStream & stream) const = 0;

	virtual Object * clone() const {
		/* May not be cloned */
		return NULL;
	}
};

/**
 * Definition for a template.
 *
 * Template definitions are not types. They specify what arguments a template
 * takes and what are their types.
 */
class TemplateDefinition: public Object {
protected:
	std::string __name;
	uint32_t argumentCount;

	TemplateDefinition(const_string name, uint32_t numArgs) :
			__name(name), argumentCount(numArgs) {
	}

	friend class SerializationManager;

public:
	/**
	 * @brief Get the Name.
	 * @details Get the name of the template.
	 */
	inline const_string name() const {
		return __name;
	}

	inline uint32_t getArgumentCount() const {
		return argumentCount;
	}

	virtual std::string toString() const {
		return __name;
	}

	virtual void toString(OutputStream & stream) const {
		stream.puts(__name.data());
	}

	size_t hashValue() const {
		return std::hash<std::string>()(__name);
	}

	virtual Object * clone() const {
		/* May not be cloned */
		return NULL;
	}
};

/**
 * Smaller name for a type definition.
 */
typedef class TypeDefinition type_t;

/**
 * Shorthand type for a template definition.
 */
typedef class TemplateDefinition template_t;

/**
 * A simple type is a non-template type.
 *
 * Non-template types are types that have no templated arguments.
 *
 * A type that inherits from a template instance is considered a non-template
 * type.
 */
class SimpleType: public TypeDefinition {
private:
	std::type_index type;
	std::string __name;

protected:
	friend class SerializationManager;

	SimpleType(std::type_index type) :
			type(type) {
		this->__name = demangleName(type.name());
	}
public:

	const_string name() const {
		return __name;
	}

	bool isPlain() const;

	size_t hashValue() const {
		return type.hash_code();
	}

	std::string toString() const {
		return __name;
	}

	void toString(OutputStream & stream) const {
		stream.puts(__name.data());
	}

	std::type_index getTypeIndex() {
		return type;
	}
};

/**
 * Templated Type.
 *
 * A definition for a type that is an instance of a template.
 *
 * This covers only templates whose arguments are types.
 *
 * More complex templates with values or other templates as arguments must be
 * treated separately.
 */
class TemplatedType: public TypeDefinition {
private:
	/**
	 * Template information.
	 */
	const template_t * tmplt;

	/**
	 * List of arguments.
	 */
	const type_t * * arguments;

	/**
	 * RTTI type of the template instance.
	 */
	const std::type_index instanceType;

	/**
	 * Name of the template instance.
	 */
	std::string __name;

	friend class SerializationManager;

	TemplatedType(std::type_index instanceType, const template_t * tmplt,
			std::initializer_list<const type_t *> arguments) :
			tmplt(tmplt), instanceType(instanceType) {

		this->arguments = new const type_t *[arguments.size()];
		int i = 0;
		for (const type_t * const & tp : arguments) {
			this->arguments[i++] = tp;
		}

		__name = demangleName(instanceType.name());
	}

public:
	bool isPlain() const;

	const_string name() const {
		return __name;
	}

	size_t hashValue() const {
		return instanceType.hash_code();
	}

	std::string toString() const {
		return __name;
	}

	void toString(OutputStream & stream) const {
		stream.puts(__name.data());
	}

	const type_t * const * getArguments() const {
		return arguments;
	}

	const template_t * getTemplate() const {
		return tmplt;
	}

	std::type_index getTypeIndex() {
		return instanceType;
	}
};

/**
 * Logger for serialization operations.
 */
extern Logger serializationLogger;

/******************************************************************************
 *
 * Serialization Manager.
 *
 */

/**
 * Global serialization manager.
 *
 * This object is initialise by osiris::init() or the first call to a global
 * Type Management Function such as registerClass().
 */
extern SerializationManager * serialManager;

/**
 * @brief A serialization exception.
 *
 * An exception thrown during the serialization process.
 *
 */
class SerializationException: public std::exception {
private:
	const type_t * type;
	const_string message;

public:
	SerializationException(const type_t * type, const_string message) :
			type(type), message(message) {
	}

	/**
	 * @brief The type being serialized/de-serialized.
	 *
	 * The type that was currently being serialized/de-serialized when the
	 * exception occurred.
	 */
	const type_t * getType() {
		return type;
	}

	const char * what() const noexcept {
		return message;
	}
};

/**
 * @brief Serialization manager.
 *
 * The serialization manager is an object responsible for keeping track of
 * serializable types and their dependencies.
 */
class SerializationManager {
private:
	std::unordered_map<const type_t *, ISerialDef *> defininitionMap;
	std::unordered_map<std::type_index, type_t *> typeMap;
	std::vector<SerializationBackend *> backends;
	std::unordered_multimap<const_string, template_t *> templates;

	// TODO: Add map for getting type by name.

	/**************************************************************************
	 * Type information.
	 */

	/**
	 * Get a type_t for a simple type.
	 *
	 * Returns an existing registration for a simple type or register a new one
	 * in case none existed.
	 *
	 * The user must be careful to make sure the provided type is indeed a
	 * simple type, failing to do so may break serialization for that type.
	 *
	 * @see SimpleType
	 *
	 * @param type The type.
	 */
	const type_t * getSimpleType(std::type_index type);

	/**
	 * Get a type definition for a type.
	 *
	 * @param type The type.
	 * @return The type definition if any or NULL.
	 */
	type_t * getRegisteredType(std::type_index type);

	class TypeOf {
	private:
		const type_t * type;

		/**
		 * Get non-templated type.
		 */
		template<class Type>
		const type_t * getType(Type & tp) {
			return serialManager->getSimpleType(typeid(Type));
		}

		template<typename T>
		struct getInstance {

			template<typename Ti>
			Ti * instantiate(typename Ti::IgnoreSerializerSpecializationFlag *) {
				return NULL;
			}

			template<typename Ti>
			Ti * instantiate(...) {
				return new Ti();
			}

			operator ISerialDef *() {
				static_assert(std::is_convertible< T *, ISerialDef * >::value, "Specialization of TemplateSerializer must inherit from ISerialDef.");
				return instantiate<T>(0);
			}
		};

		template<template<typename ...> class C, typename ...Args>
		const type_t * getType(C<Args...> & v) {
			const template_t * tmplt = serialManager->getTemaplate(typeid(C<Args...>), sizeof...(Args));
			type_t * tp = serialManager->getRegisteredType(typeid(C<Args...>));

			if(!tp) {
				tp = new TemplatedType(typeid(C<Args...>), tmplt, {(getType(* (Args *) NULL))...});
				serialManager->typeMap.insert(std::pair<std::type_index, type_t *>(typeid(C<Args...>), tp));

				/* If the type was not registered neither was it's serializer */
				ISerialDef * sd = getInstance< TemplateSerializer<C, Args...> >();
				if(sd) {
					serialManager->defininitionMap.insert(std::pair<const type_t *, ISerialDef *>(tp, sd));
				}
			}
			return tp;
		}

		const type_t * getType() {
			return type;
		}

	public:
		template<typename Type>
		TypeOf(Type && val) : type(getType(val)) {
		}
		operator const type_t * () {
			return type;
		}
	};

public:
	/**
	 * @brief Register a new serializable class.
	 *
	 * A serializable class is a definition of an object that can be serialized
	 * using the Osiris serialization system.
	 *
	 * The object defines the serializable members and optional custom
	 * serialization/de-serialization methods.
	 *
	 * @param classInfo The serializable class descriptor.
	 */
	void registerClass(ISerialDef & classInfo);

	/**
	 * @brief Get serialization information.
	 *
	 * Get the serialization definition for a type.
	 *
	 * @param type The type.
	 * @return [description]
	 */
	ISerialDef * getSerialDef(const type_t * type);

	/**
	 * @brief Add a back-end to the list of know back-ends.
	 *
	 * Add a new back-end to the list of known serialization back-ends.
	 *
	 * @param backend The new backend.
	 */
	void registerBackend(SerializationBackend * backend);

	/**
	 * @brief fetch a backend by name.
	 *
	 * Search for a backend based on it's name.
	 *
	 * @param backendName The name of the backend.
	 * @return The backend if found or NULL.
	 */
	const SerializationBackend * getBackend(const_string backendName);

	/**
	 * @brief Get the default back-end.
	 *
	 * Get the default serialization back-end.
	 *
	 * On any regular installation the default backend is the built in binary
	 * backend.
	 */
	const SerializationBackend * getDefaultBackend();

	/**
	 * @brief Get the backend list.
	 *
	 * Get a list containing all the back-ends.
	 *
	 * The first backend of this list is the _default_ backend used in the
	 * function serializeToStream().
	 */
	const std::vector<SerializationBackend *> getBackendList() const;

	/**
	 * Get the type of an expression.
	 *
	 * @param v The expression value, cannot be void.
	 */
	template<typename T>
	const type_t * typeOf(T && v) {
		return TypeOf(v);
	}

	/**
	 * Get a template definition based in a templated type and it's number of
	 * arguments.
	 *
	 * @param type The template instance type.
	 * @param num_args The number of type arguments the template takes.
	 */
	const template_t * getTemaplate(std::type_index type, unsigned num_args);

	void dumpTypeInfo();
};

/**
 * @name Type Management Global Functions.
 * @{
 */

/**
 * Register a class with the serialManager.
 *
 * This method exists to ensure static initialisation order and should be used
 * on statically run code. Elsewhere the direct call to the serialManger can be
 * used.
 *
 * @param classInfo The class to register.
 */
void registerClass(ISerialDef & classInfo);

/**
 * Get the serial definition of a type.
 *
 * The serial definition provides information about how to serialize a type.
 *
 * @param type The type to get the serial def for.
 */
ISerialDef * getSerialDef(const type_t * type);

template<typename T>
const type_t * typeOf(T && v) {
	if (!serialManager) {
		serialManager = new SerializationManager();
	}
	return serialManager->typeOf(v);
}

template<typename T>
const type_t * typeOf() {
	if (!serialManager) {
		serialManager = new SerializationManager();
	}
	return serialManager->typeOf(*(T *) NULL);
}

/**
 * @brief Add a back-end to the list of know back-ends.
 *
 * This function is for static initialisation.
 *
 * @param backend The new backend.
 */
void registerBackend(SerializationBackend * backend);

/** @} */

/**
 * @name Global serialization shorthands.
 * @{
 */

/**
 * @brief Write a serialized object to a stream.
 *
 * Write an object to a stream using the default serializer.
 *
 * @param stream The stream to use.
 * @param type The type of the object.
 * @param object The source object.
 */
void serializeToStream(OutputStream & stream, const type_t * type,
		void * object) throw (SerializationException &);

/**
 * @brief Write a serialized object to a stream.
 *
 * Write an object to a stream using the default serializer.
 *
 * This version will only work if the type of the object is already registered
 * with the serialization manager, or if it's a simple type.
 *
 * @param stream The stream to use.
 * @param object The object to serialize.
 */
template<typename Type>
void serializeToStream(OutputStream & stream, Type & object)
		throw (SerializationException &) {
	serializeToStream(stream, typeOf(object), (void *) &object);
}

/**
 * @brief Read a serialized object from a stream.
 *
 * Read an object from a stream using the default de-serializer.
 *
 * @param stream The stream to read from.
 * @param type The type of the object.
 * @param object The object.
 */
void deserializeFromStream(InputStream & stream, const type_t * type,
		void * object) throw (SerializationException &);

/**
 * @brief Read a serialized object from a stream.
 *
 * Read an object from a stream using the default de-serializer.
 * This version will only work if the type of the object is already registered
 * with the serialization manager, or if it's a simple type.
 *
 * @param stream The stream to read from.
 * @param object The object.
 */
template<typename Type>
void deserializeFromStream(InputStream & stream, Type & object)
		throw (SerializationException &) {
	deserializeFromStream(stream, typeOf(object),
			(void *) &object);
}

/** @} */

/******************************************************************************
 *
 * Serialization Process
 *
 */

/**
 * @brief Type of a field.
 *
 * The type of a serialized field.
 *
 * This type is with respect to the definition of primitive serializer
 * types and are as follows.
 */
enum class SerialType
	: unsigned char {
		Unset, //!< The field has not been initialised yet.
	Flat, //!< The field contains plain binary data.
	Array, //!< The field is an array.
	Object //!< The field is an object containing more named fields.
};

inline const_string toString(SerialType type) {
	switch (type) {
		case SerialType::Unset:
			return "Unset";
			break;
		case SerialType::Flat:
			return "Flat";
			break;
		case SerialType::Array:
			return "Array";
			break;
		case SerialType::Object:
			return "Object";
			break;
	}

	return "Invalid Type";
}

/**
 * @brief Serialization Context.
 *
 * Abstract base class for a serialization context.
 *
 * A serialization context is an object that manages the serialization process
 * of another object.
 *
 * It is usually exposed to the custom serializer method for a given object.
 *
 * Serialization context are interfaces to different serialization backends
 * such as binary and Json.
 */
class SerializationContext {
public:
	virtual ~SerializationContext() {
	}

	/**
	 * @brief Write a serialized object to a stream.
	 *
	 * Write an object to a stream using this serialization context.
	 *
	 * @param stream The stream to use.
	 * @param type The type of the object.
	 * @param object The source object.
	 */
	void serializeToStream(OutputStream & stream, const type_t * type,
			void * object) throw (SerializationException &);

	/**
	 * @brief Write a serialized object to a stream.
	 *
	 * Write an object to a stream using this serialization context.
	 *
	 * This version will only work if the type of the object is already registered
	 * with the serialization manager, or if it's a simple type.
	 *
	 * @param stream The stream to use.
	 * @param object The object to serialize.
	 */
	template<typename Type>
	void serializeToStream(OutputStream & stream, Type & object)
			throw (SerializationException &) {
		serializeToStream(stream, typeOf(object),
				(void *) &object);
	}

	/**
	 * Begin writing an object to a stream.
	 *
	 * The same stream shall be used to write all the object data until a
	 * finish() call is issued.
	 *
	 * @param stream The stream to write the object to.
	 */
	virtual void begin(OutputStream & stream) = 0;

	/**
	 * @brief Write a value to the context.
	 *
	 * Write the value of this field to the context.
	 *
	 * This call is only usable if the context type is set to SerialType::Unset
	 * or SerialType::Flat.
	 *
	 * This should only be called once.
	 *
	 * @param data The data to set for this field.
	 * @param len The number of bytes in  the data.
	 */
	virtual void writeValue(uint8_t * data, uint32_t len) = 0;

	/**
	 * @brief Write an object as a SerialType::Flat field value.
	 *
	 * Write to the current field an object of the provided type.
	 *
	 * The object is written as a SerialType::Flat value.
	 *
	 * @param type The type of the object.
	 * @param object A pointer to the object.
	 */
	virtual void writeSerializable(const type_t * type, void * object)
			throw (SerializationException&) = 0;

	/**
	 * @brief Get a stream for writing a flat field.
	 *
	 * Request a stream to write Flat data to.
	 *
	 * On some backends it is necessary to inform the length of the field
	 * before writing it.
	 *
	 * @param length The total number of bytes that will be written to the
	 * 			stream. Any less that this will be zero-filled. Any extra data
	 * 			will be discarded.
	 * @return The stream.
	 */
	virtual OutputStream & getFieldStream(uint32_t length) = 0;

	/**
	 * @brief Start writing a sub-object record.
	 *
	 * Start the recording of a sub object.
	 *
	 * This call can only be issued if the context type is SerialType::None or
	 * SerialType::Object.
	 */
	virtual void beginObject() = 0;

	/**
	 * @brief Finish a object record.
	 *
	 * This call end an object record.
	 *
	 * It may only be called at any time there is an opened object record.
	 *
	 * If a field has been created but not set it will be written as a empty
	 * object of the field's type. If the type of the field has not been set
	 * the field will be ignored.
	 *
	 */
	virtual void endObject() = 0;

	/**
	 * @brief Add a new field.
	 *
	 * Create anew field with a given name, the name may not collide with any
	 * previous name on the same object. In case it does a warning is emitted
	 * on the log and this calls is ignored.
	 *
	 * This call may only be made if the current primitive is a unclosed
	 * object.
	 *
	 * @param name The field's name.
	 */
	virtual void addField(const_string name) = 0;

	/**
	 * @brief Begin a new array value.
	 *
	 * Start a new array record.
	 *
	 * Arrays contain a numbered sequence of records of any type.
	 *
	 * Arrays behave is if they were simple fields but multiple calls of field
	 * setting functions simply append more data to the array.
	 *
	 * An array may only be finalised by a matching endArray() call or an
	 * endObject call that matches an enclosing field.
	 *
	 * With respect to the size parameter it defines the total size of an
	 * array. Not all serializers might enforce it but it is recommended that
	 * it be provided.
	 *
	 * Serializers that do enforce pre-set array sizes will ignore any extra
	 * elements and fill any remaining elements with appropriate NULL values.
	 *
	 * @param size The number of elements on the array.
	 */
	virtual void beginArray(unsigned size) = 0;

	/**
	 * @brief Finish an array record.
	 *
	 * Finalises the innermost array record.
	 */
	virtual void endArray() = 0;

	/**
	 * @brief Finish writing the serialized object.
	 *
	 * This shall only be called by the user after he is done writing an object
	 * to the context.
	 */
	virtual void finish() = 0;
};

/**
 * @brief De-serialization context.
 *
 * Abstract base class for a de-serialization context.
 *
 * A de-serialization context has exactly the opposite semantics that of a
 * SerializationContext. It contains methods to read data from already
 * serialized objects.
 *
 * At any point in time the de-serialization context lies in some level of the
 * serialized object. We call that point a _Node_ in the sub-object tree and we
 * have that:
 *
 * - The current node has a type.
 * - If that type is SerialType::Object then it has child nodes.
 * - If the type is SerialType::Array then it contains a list of sub-objects
 * 		with the said type.
 * - If the type is SerialType::Flat it contains some raw data.
 *
 */
class DeserializationContext {
protected:
public:
	virtual ~DeserializationContext() {
	}

	/**
	 * @brief Read a serialized object from a stream.
	 *
	 * Read an object from a stream using this de-serialization context.
	 *
	 * @param stream The stream to read from.
	 * @param type The type of the object.
	 * @param object The object.
	 */
	void deserializeFromStream(InputStream & stream, const type_t * type,
			void * object) throw (SerializationException &);

	/**
	 * @brief Read a serialized object from a stream.
	 *
	 * Read an object from a stream using this de-serialization context.
	 * This version will only work if the type of the object is already registered
	 * with the serialization manager, or if it's a simple type.
	 *
	 * @param stream The stream to read from.
	 * @param object The object.
	 */
	template<typename Type>
	void deserializeFromStream(InputStream & stream, Type & object)
			throw (SerializationException &) {
		deserializeFromStream(stream, typeOf(object),
				(void *) &object);
	}

	/**
	 * Begin the de-serialization of an object.
	 *
	 * The object is read form the provided stream.
	 *
	 * The stream is further used until a call to begin is issued again.
	 *
	 * @param stream The stream to read from.
	 */
	virtual void begin(InputStream & stream) = 0;

	/**
	 * @brief Get the type of the current node.
	 *
	 * Depending on the type of the node different methods will be available
	 * for extracting the serialized data.
	 */
	virtual SerialType getType() = 0;

	/**
	 * @brief Return the name of the current field.
	 *
	 * If we are on a sub object node then we stand at a field which has a
	 * name. This name can be queried with this function.
	 */
	virtual const_string getFieldName() = 0;

	/**
	 * @brief Get the size of the current field's data.
	 *
	 * Get the size of the current Flat field.
	 */
	virtual uint32_t getFieldSize() = 0;

	/**
	 * @brief Get the data on the current field.
	 *
	 * Get the binary data for the current field.
	 *
	 * This call fails if the current field is not Flat.
	 *
	 * @param buffer The buffer where to store the field data.
	 * @param bufferSize The size of the buffer.
	 * @return The number of bytes actually written on the buffer.
	 */
	virtual size_t readField(void * buffer, uint32_t bufferSize) = 0;

	/**
	 * @brief Get a input stream for the current field.
	 *
	 * Return a stream containing the data on the current field.
	 */
	virtual InputStream & getFieldStream() = 0;

	/**
	 * @brief Get the type of a serialized field.
	 *
	 * Get the stored type for a serialized field.
	 *
	 * Not all back-ends store type information on the serialized data.
	 *
	 * In case the field is not a serialized field or it does not store type
	 * information NULL is returned.
	 */
	virtual type_t * getFieldType() = 0;

	/**
	 * @brief De-serialize the current field.
	 *
	 * Attempt to de-serialize the value of the current field to the provided
	 * type.
	 *
	 * This call may be issued on any field type since different back-ends can
	 * morph the field type during serialization (for instance Json has the
	 * tendency to morph serialized blobs into objects).
	 *
	 * If the de-serialization process fails an exception is thrown.
	 *
	 * @param type The type of the serialized field.
	 * @param field A pointer to a memory position that is to hold the
	 *             de-serialized type.
	 */
	virtual void deserializeField(const type_t * type, void * field)
			throw (SerializationException&) = 0;

	/**
	 * @brief Enter a node's children.
	 *
	 * If the current node is an object this navigates to it's first
	 * child.
	 */
	virtual void enterObject() = 0;

	/**
	 * @brief Navigate to the parent object.
	 *
	 * When the current node is a field in an object this navigates to the
	 * parent object.
	 */
	virtual void leaveObject() = 0;

	/**
	 * @brief Get the number of elements in the array.
	 *
	 * Returns the number of elements in the serialized array.
	 */
	virtual unsigned getArrayLength() = 0;

	/**
	 * @brief Navigate to the first array element.
	 *
	 * Enter the array.
	 *
	 * Each element on the array can be read with the field reading methods and
	 * once a field is read the context automatically skips to the next.
	 */
	virtual void enterArray() = 0;

	/**
	 * @brief Leave the array elements.
	 *
	 * Navigate back to the parent field where the array was specified.
	 */
	virtual void leaveArray() = 0;
};

/**
 * @brief A serialization back-end.
 *
 * Abstract base class for a specification of a serialization back-end.
 *
 * the methods this class defines are used to obtain information and create
 * contexts for serialization and de-serialization of objects.
 *
 */
class SerializationBackend {
public:
	virtual ~SerializationBackend() {
	}

	/**
	 * @brief The name of the back-end.
	 *
	 * Get the name for this serialization back-end.
	 */
	virtual const_string getName() const = 0;

	/**
	 * @brief Create a new serialization context.
	 */
	virtual SerializationContext * getSerializationContext() const = 0;

	/**
	 * @brief Create a new de-serialization context.
	 */
	virtual DeserializationContext * getDeserializationContext() const = 0;
};

/******************************************************************************
 *
 * Serializable Object Info.
 *
 */

/**
 * @brief A serializable class.
 *
 * An object that defines the serializable members of an object as well as it's
 * serialization process.
 */
class ISerialDef {
public:
	virtual ~ISerialDef() {
	}

	/**
	 * @brief Get the type that corresponds to this definition.
	 *
	 * Get the type that is to be serialized by this definition.
	 */
	virtual const type_t * getType() const = 0;

	/**
	 * @brief Get the fields that are serializable in this class.
	 *
	 * Get a list of the fields that this object is to serialize.
	 */
	virtual const std::vector<SerialField *> * getFields() const {
		return NULL;
	}

	/**
	 * @brief serialize this object.
	 *
	 * This method is called to serialize the type.
	 *
	 * Serialization proceeds as described in the serialization documentation.
	 *
	 * @param ctxt The serialization context.
	 */
	virtual void serialize(SerializationContext & ctxt, void * source) const
			throw (SerializationException&);

	/**
	 * @brief De-serialize an object.
	 *
	 * This method is called to de-serialize the object.
	 * @param ctxt The De-serialization context.
	 */
	virtual void deserialize(DeserializationContext & ctxt, void * dest) const
			throw (SerializationException&);

protected:
	/**
	 * @brief Construct an instance of the serializable type.
	 *
	 * In some occasions it may be necessary to intialise an instance of the
	 * type (such as when the is an element of an array).
	 *
	 * @param field A pointer to a uninitialised memory area set to hold the an
	 * instance of the type to be de-serialized according to this class.
	 */
	virtual void constructInstance(void * field) const = 0;

	friend class SerialField;

	ISerialDef() {
	}
};

/**
 * Describes the properties of a serializable field in a serializable class.
 *
 * Each field is identified by it's type, name and offset within the containing
 * class.
 *
 * Instead of using serial fields directly one may also use the field
 * definition macros provided for convenience.
 */
class SerialField {
private:
	const type_t * type;
	const_string name;
	size_t offset;

public:
	/**
	 * @brief Construct a serializable field descriptor.
	 *
	 * Construct a serializable field descriptor.
	 *
	 * There are a series of macros that can be used to simplify this
	 * construction.
	 *
	 * @param type The type of the field.
	 * @param name The name of the field.
	 * @param offset The byte offset of he field in the class/struct.
	 */
	SerialField(const type_t * type, const_string name, size_t offset) :
			type(type), name(name), offset(offset) {
	}

	/**
	 * Get the name of the field.
	 *
	 * et the name of this field.
	 */
	const_string getName() const {
		return name;
	}

	/**
	 * @brief Get the field's byte offset.
	 *
	 * Get the offset within the containing class or struct for this field.
	 *
	 * This offset can be obtained from offsetof().
	 */
	size_t getOffset() const {
		return offset;
	}

	/**
	 * @brief Get the field's type.
	 *
	 * Get the type for the field.
	 */
	const type_t * getType() const {
		return type;
	}
};

/**
 * @name Field Definition Macros
 * @{
 */

/**
 * @brief Construct a simple type.
 *
 * Construct a definition for a field whose type is a simple type.
 *
 * @see osiris::SimpleType
 */
#define SimpleSerialField(klass, type, name) new SerialField(typeOf<type>(), #name, offsetof(klass, name))

/** @} */

/**
 * @brief A serializer.
 *
 * A function that can be called to serialize an object.
 *
 * This function can call the default serializer if it intends only to prepare
 * the object.
 */
typedef void (*Serializer)(SerializationContext &, void *);

/**
 * @brief A de-serializer.
 *
 * A function that can be called to de-serialize an object.
 *
 * This function can call the default de-serializer if intends only to finish
 * restoring the object after de-serialization.
 */
typedef void (*Deserializer)(DeserializationContext &, void *);

/**
 * @brief Simple Serializable Class.
 *
 * A descriptor of a serializable class.
 *
 * This is to be used for non-template classes and structs.
 */
template<typename Class, Serializer __Serializer = (Serializer) NULL,
		Deserializer __Deserializer = (Deserializer) NULL>
class SimpleSerialClassDef: public ISerialDef {
private:
	std::vector<SerialField *> fields;

	const type_t * type;

	const_string name;

public:
	SimpleSerialClassDef(std::initializer_list<SerialField *> fields) :
			type(typeOf<Class>()), name(type->name()) {
		registerClass(*this);

		for (SerialField * f : fields) {
			addField(f);
		}
	}

	const type_t * getType() const {
		return type;
	}

	const std::vector<SerialField *> * getFields() const {
		return &fields;
	}

	/**
	 * @brief Add a field.
	 *
	 * A a field to the list of serialized fields.
	 *
	 * @param field The field to add.
	 */
	void addField(SerialField* field) {
		fields.push_back(field);
	}

private:
	void serialize(SerializationContext & ctxt, void * source) const
			throw (SerializationException&) {
		if (__Serializer != 0) {
			__Serializer(ctxt, source);
		} else {
			ISerialDef::serialize(ctxt, source);
		}
	}

	void deserialize(DeserializationContext & ctxt, void * dest) const
			throw (SerializationException&) {
		if (__Deserializer != 0) {
			__Deserializer(ctxt, dest);
		} else {
			ISerialDef::deserialize(ctxt, dest);
		}
	}

	void constructInstance(void * field) const {
		*((Class *) field) = Class();
	}
};

/**
 * @brief This marks classes that can be serialized.
 *
 * This only provides a convenient method for getting the classes Type
 * Information.
 *
 * There is no need for a class to inherit from Serializable for it to be
 * indeed serializable.
 *
 */
template<class Class>
class Serializable {
public:

	/**
	 * @brief Get the serializable class description.
	 *
	 *
	 *
	 * @details [long description]
	 * @return [description]
	 */
	static ISerialDef * getSerializable() {
		return osiris::getSerialDef(typeOf<Class>());
	}
};

template<template<typename ...> class Template, typename... Args>
class TemplateSerializer : public ISerialDef {
public:
	IgnoreSerializerSpecialization;
};

/**
 * Serial definition for vector classes.
 */
template<class MemberType, typename Allocator>
class TemplateSerializer<std::vector, MemberType, Allocator> : public ISerialDef {

	const type_t * type;

public:
	TemplateSerializer() {
		type = typeOf<std::vector<MemberType, Allocator>>();
		registerClass(*this);
	}

	const type_t * getType() const {
		return type;
	}

private:
	void serialize(SerializationContext & ctxt, void * source) const
			throw (SerializationException&) {
		std::vector<MemberType, Allocator> & vec = *(std::vector<MemberType, Allocator> *) source;
		ctxt.beginArray(vec.size());
		for (MemberType & m : vec) {
			ctxt.writeSerializable(typeOf<MemberType>(), &m);
		}
		ctxt.endArray();
	}

	void deserialize(DeserializationContext & ctxt, void * dest) const
			throw (SerializationException&) {
		std::vector<MemberType, Allocator> & vec = *(std::vector<MemberType, Allocator> *) dest;
		ctxt.enterArray();
		vec.reserve(ctxt.getArrayLength());

		for (unsigned i = 0; i < vec.capacity(); i++) {
			vec.emplace_back();
			ctxt.deserializeField(typeOf<MemberType>(), &vec.back());
		}

		ctxt.leaveArray();
	}

	void constructInstance(void * field) const {
		*((MemberType *) field) = MemberType();
	}
};

/**
 * Serial definition for deque classes.
 */
template<class MemberType, typename Allocator>
class TemplateSerializer<std::deque, MemberType, Allocator> : public ISerialDef {

	const type_t * type;

public:
	TemplateSerializer() {
		type = typeOf<std::deque<MemberType, Allocator>>();
		registerClass(*this);
	}

	const type_t * getType() const {
		return type;
	}

private:
	void serialize(SerializationContext & ctxt, void * source) const
			throw (SerializationException&) {
		std::deque<MemberType, Allocator> & vec = *(std::deque<MemberType, Allocator> *) source;
		ctxt.beginArray(vec.size());
		for (MemberType & m : vec) {
			ctxt.writeSerializable(typeOf<MemberType>(), &m);
		}
		ctxt.endArray();
	}

	void deserialize(DeserializationContext & ctxt, void * dest) const
			throw (SerializationException&) {
		std::deque<MemberType, Allocator> & vec = *(std::deque<MemberType, Allocator> *) dest;
		ctxt.enterArray();

		uint32_t alen = ctxt.getArrayLength();

		for (unsigned i = 0; i < alen; i++) {
			vec.emplace_back();
			ctxt.deserializeField(typeOf<MemberType>(), &vec.back());
		}

		ctxt.leaveArray();
	}

	void constructInstance(void * field) const {
		*((MemberType *) field) = MemberType();
	}
};

/**@}*/

}

#endif
