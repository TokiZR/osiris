#ifndef __OSIRIS_PRIVATE_BASIC_STRING_H__
#define __OSIRIS_PRIVATE_BASIC_STRING_H__

#include <osiris/bits/ostring.h>

#include <stdexcept>

namespace osiris {

/**
 * Basic String class.
 *
 * This class represents copy on write, reference counted strings.
 *
 * @tparam CharT A character Type
 * @tparam ResizePolicy A member of struct resize_policy which defines how
 *         memory should be handled when a string needs to be resized.
 */
template<typename CharT, typename ResizePolicy = resize_policy::Quadractic>
class basic_string {
private:
	typedef internal::ostring<CharT> ostring;

	/**
	 * Shared internal string.
	 *
	 * This has to be mutable because we may get this pointer from a const
	 * string. This is one less safety check but here we compromise
	 * maintainability a bit so people don't have to compromise performance.
	 *
	 * TODO: __str should never be null since an empty string must still have a
	 * null byte.
	 */
	mutable ostring * __str;

	/**************************************************************************
	 *
	 * OString Utils.
	 *
	 */

	void unref() {
		if (__str)
			__str->unref();
	}

public:
	class CharProxy {
		basic_string & s;
		size_t index;

		CharProxy(basic_string & bs, size_t index) :
				s(bs), index(index) {
		}

		friend class basic_string;
		template<typename >
		friend class normal_iterator;

	public:
		operator CharT() const {
			return s.__str->viewData()[index];
		}

		CharT & operator =(CharT && c) throw (string_exception &) {
			if (s.__str->isShared()) {
				s.__str = s.__str->dup();
			}

			CharT & ref = s.__str->editData()[index];
			ref = c;

			return c;
		}
	};

	template<typename _ItCharT, int Dir = 1>
	class base_iterator: public std::iterator<std::random_access_iterator_tag, _ItCharT, ptrdiff_t,
			_ItCharT*, _ItCharT&> {
	private:

		size_t high() const {
			return bs.length();
		}

		size_t low() const {
			return 0;
		}

		void move(ssize_t amount) {
			if (amount > 0)
				offset = std::min(offset + amount, high());
			else if (amount < 0) {
				if ((size_t) -amount > offset)
					offset = 0;
				else
					offset = offset + amount;
			}
		}

	public:
		base_iterator<_ItCharT, Dir> & operator ++() {
			move(1);
			return *this;
		}

		base_iterator<_ItCharT, Dir> operator++(int) {
			base_iterator<_ItCharT, Dir> cp(*this);
			move(1);
			return cp;
		}

		base_iterator<_ItCharT, Dir> & operator --() {
			move(-1);
			return *this;
		}

		base_iterator<_ItCharT, Dir> operator--(int) {
			base_iterator<_ItCharT, Dir> cp(*this);
			move(-1);
			return cp;
		}

		base_iterator<_ItCharT, Dir> operator +(ssize_t offt) {
			base_iterator<_ItCharT, Dir> cp(*this);
			cp.move(offt);
			return cp;
		}

		base_iterator<_ItCharT, Dir> operator -(ssize_t offt) {
			base_iterator<_ItCharT, Dir> cp(*this);
			cp.move(-offt);
			return cp;
		}

		base_iterator<_ItCharT, Dir> & operator +=(ssize_t offt) {
			move(offt);
			return *this;
		}

		base_iterator<_ItCharT, Dir> & operator -=(ssize_t offt) {
			move(-offt);
			return *this;
		}

		bool operator ==(const base_iterator<_ItCharT, Dir> && it) const {
			if (&it.bs != &bs)
				return false;
			return it.offset == offset;
		}
		bool operator ==(const base_iterator<_ItCharT, Dir> & it) const {
			if (&it.bs != &bs)
				return false;
			return it.offset == offset;
		}

		bool operator !=(const base_iterator<_ItCharT, Dir> && it) const {
			if (&it.bs != &bs)
				return true;
			return !(it == *this);
		}
		bool operator !=(const base_iterator<_ItCharT, Dir> & it) const {
			if (&it.bs != &bs)
				return true;
			return !(it == *this);
		}

		bool operator >(const base_iterator<_ItCharT, Dir> && it) const {
			return offset > it.offset;
		}
		bool operator >(const base_iterator<_ItCharT, Dir> & it) const {
			return offset > it.offset;
		}

		bool operator <(const base_iterator<_ItCharT, Dir> && it) const {
			return offset < it.offset;
		}
		bool operator <(const base_iterator<_ItCharT, Dir> & it) const {
			return offset < it.offset;
		}

		bool operator >=(const base_iterator<_ItCharT, Dir> && it) const {
			return offset >= it.offset;
		}
		bool operator >=(const base_iterator<_ItCharT, Dir> & it) const {
			return offset >= it.offset;
		}

		bool operator <=(const base_iterator<_ItCharT, Dir> && it) const {
			return offset <= it.offset;
		}
		bool operator <=(const base_iterator<_ItCharT, Dir> & it) const {
			return offset <= it.offset;
		}

		ptrdiff_t operator -(const base_iterator<_ItCharT, Dir> && it) const {
			return (ssize_t) offset - (ssize_t) it.offset;
		}
		ptrdiff_t operator -(const base_iterator<_ItCharT, Dir> & it) const {
			return (ssize_t) offset - (ssize_t) it.offset;
		}

	protected:
		typedef typename std::conditional<std::is_const<_ItCharT>::value, const basic_string, basic_string>::type StringType;

		StringType & bs;
		size_t offset;

		base_iterator(StringType & bs, size_t offset) :
				bs(bs), offset(offset) {
		}

		template<typename, typename >
		friend class basic_string;
	};

	template<typename _ItCharT, int Dir = 1>
	class const_iterator: public base_iterator<_ItCharT, Dir> {
	public:
		using base_iterator<_ItCharT, Dir>::base_iterator;

		const _ItCharT operator *() const {
			return Dir > 0 ? this->bs[this->offset] : this->bs[this->high() - this->offset - 1];
		}

		template<typename, typename >
		friend class basic_string;
	};

	template<typename _ItCharT, int Dir = 1>
	class normal_iterator: public base_iterator<_ItCharT, Dir> {
	public:
		using base_iterator<_ItCharT, Dir>::base_iterator;
		CharProxy operator *() {
			return CharProxy(this->bs, Dir > 0 ? this->offset : this->high() - this->offset - 1);
		}

		const _ItCharT operator *() const {
			return Dir > 0 ? this->bs[this->offset] : this->bs[this->high() - this->offset - 1];
		}

		template<typename, typename >
		friend class basic_string;
	};

	typedef normal_iterator<CharT, 1> iterator;

	typedef normal_iterator<CharT, -1> riterator;
	typedef const_iterator<const CharT, 1> citerator;
	typedef const_iterator<const CharT, -1> criterator;

	/**************************************************************************
	 *
	 * Constructors
	 *
	 */

	basic_string() :
			__str(nullptr) {
	}

	~basic_string() {
		unref();
	}

	basic_string(const basic_string & str) {
		__str = str.__str;
		__str->ref();
	}

	basic_string(basic_string && str) {
		__str = str.__str;
		str.__str = ostring::fromConstant("");
	}

	basic_string(const CharT * s, bool constant = false) {
		if (constant)
			__str = ostring::fromConstant(s);
		else
			__str = ostring::fromData(s);
	}

	basic_string(unsigned count, CharT ch) {
		__str = ostring::fromChar(ch, count);
	}

	basic_string(std::initializer_list<CharT> ilist) {
		__str = ostring::withStorage(ilist.size());
		__str->length = ilist.size();

		size_t i = 0;
		CharT * data = __str->editData();

		for (CharT c : ilist) {
			data[i++] = c;
		}
	}

	basic_string(std::basic_string<CharT> & s) {
		__str = ostring::fromData(s.data());
	}

	/**************************************************************************
	 *
	 * Assignment
	 *
	 */

	/**
	 * @name Assignment
	 * @{
	 */

	basic_string & operator=(const basic_string & str) {
		unref();

		__str = str.__str;
		__str->ref();

		return *this;
	}

	basic_string & operator=(basic_string && str) {
		unref();
		__str = str.__str;
		str.__str = nullptr;

		return *this;
	}

	basic_string & operator=(const CharT * s) {
		unref();

		__str = ostring::fromData(s);

		return *this;
	}

	basic_string & operator=(CharT ch) {
		unref();

		__str = ostring::fromChar(ch);

		return *this;
	}

	basic_string & operator=(std::initializer_list<CharT> ilist) {
		unref();

		__str = ostring::withStorage(ilist.size());
		__str->length = ilist.size();

		size_t i = 0;
		CharT * data = __str->editData();

		for (CharT c : ilist) {
			data[i++] = c;
		}

		return *this;
	}

	basic_string & operator=(std::string & s) {
		unref();

		__str = ostring::fromData(s.data());

		return *this;
	}

	basic_string & operator=(std::string && s) {
		unref();

		__str = ostring::fromData(s.data());

		return *this;
	}

	/**
	 * Assign to this object a string made up of the repetition of a character.
	 *
	 * Complexity O(n)
	 *
	 * @param count The number of repetitions.
	 * @param ch The charactert to use.
	 * @return A reference to this object.
	 */
	basic_string& assign(size_t count, CharT ch) {
		unref();
		__str = ostring::fromChar(ch, count);

		return *this;
	}
	/**
	 * Assign to this object another basic string.
	 *
	 * Complexity: O(1)
	 *
	 * @param str The string to be assigned.
	 *
	 * @return A reference to this object.
	 */
	basic_string& assign(const basic_string& str) {
		unref();

		__str = str.__str;
		__str->ref();
		return *this;
	}

	/**
	 * Assign to this object a substring of another string.
	 *
	 * Complexity: O(count)
	 *
	 * @param str The string to extract the substring from.
	 * @param pos The starting position of the substring.
	 * @param count The number of characters to extract from the string.
	 *
	 * @return A reference to this object.
	 */
	basic_string& assign(const basic_string& str, size_t pos, size_t count = -1) {
		unref();

		if (count == -1 || count > str.length() - pos)
			count = str.length() - pos;

		__str = ostring::withStorage(count);

		CharT * data = __str->editData();
		CharT * view = str.data();
		for (size_t i = 0; i < count; i++) {
			data[i] = view[pos + i];
		}

		return *this;
	}

	/**
	 * Move Assignment.
	 *
	 * This method is called when the argument's lifetime would end after the call ended.
	 *
	 * Complexity: O(1)
	 *
	 * @param str The string to be assigned to.
	 *
	 * @return A refewrence to this object.
	 */
	basic_string& assign(basic_string&& str) {
		unref();

		__str = str.__str;
		str.__str = nullptr;
		return *this;
	}

	/**
	 * Assign this string to the first count characters of target string.
	 *
	 * Complexity: O(count)
	 *
	 * @param s The string to extract characters from.
	 * @param count The number of characters to copy from the string.
	 *
	 * @return A reference to this object.
	 */
	basic_string& assign(const CharT* s, size_t count) {
		unref();

		size_t slen = osiris::strlen(s);

		if (count == -1 || count > slen)
			count = slen;

		__str = ostring::withStorage(count);

		CharT * data = __str->editData();
		for (size_t i = 0; i < count; i++) {
			data[i] = s[i];
		}
		return *this;
	}

	/**
	 * Assign this object to a standard C string.
	 *
	 * If the string is constant then it is not copied. This is particularly
	 * good on applications where a significant number of strings are in
	 * constant memory.
	 *
	 * Complexity:
	 * * O(n) String not constant
	 * * O(1) Constant String
	 *
	 * @param s The string to be assigned to.
	 * @param constant Weather the string is guaranteed to be constant over the lifetime of this
	 * object and any copies of it.
	 *
	 * @return A reference to this object.
	 */
	basic_string& assign(const CharT* s, bool constant = false) {
		unref();

		if (constant)
			__str = ostring::fromConstant(s);
		else
			__str = ostring::fromData(s);
		return *this;
	}

	/**
	 * Copy characters to this string using the provided iterators.
	 *
	 * @param first Iterator referencing the first character to copy.
	 * @param last Iterator referencing the position after the last character to copy.
	 *
	 * @return A reference to this object.
	 */
	template<class InputIt>
	basic_string& assign(InputIt first, InputIt last) {
		unref();

		size_t len = last - first;

		__str = ostring::withStorage(len);

		CharT * data = __str->editData();
		for (size_t i = 0; first != last; first++, i++) {
			data[i] = *first;
		}
		return *this;
	}

	/**
	 * Assign a list of characters to this string.
	 *
	 * @param ilist The list of characters to be assigned to.
	 *
	 * @return A reference to this object.
	 */
	basic_string& assign(std::initializer_list<CharT> ilist) {
		unref();

		__str = ostring::withStorage(ilist.size());
		__str->length = ilist.size();

		size_t i = 0;
		CharT * data = __str->editData();

		for (CharT c : ilist) {
			data[i++] = c;
		}
		return *this;
	}
	/**@}*/

	/**************************************************************************
	 *
	 * String Access Methods
	 *
	 */

	/**
	 * @name Element Access
	 * @{
	 */

	/**
	 * Get a reference to the character at a given position.
	 *
	 * Does not perform bounds checking.
	 *
	 * @param index The index on the string.
	 *
	 * @return Proxy object to the referenced character.
	 */
	CharProxy operator [](size_t index) {
		return CharProxy(*this, index);
	}

	/**
	 * Get the value of the character at a given position.
	 *
	 * Does not perform bounds checking.
	 *
	 * @param index The index of the character.
	 *
	 * @return The value of the character.
	 */
	CharT operator [](size_t index) const {
		return __str->viewData()[index];
	}

	/**
	 * Get a reference to the character at a given position.
	 *
	 * Does perform bounds checking.
	 *
	 * @param index The index on the string.
	 *
	 * @return Proxy object to the referenced character.
	 */
	CharProxy at(size_t index) throw (std::out_of_range &) {
		if (!__str || index > __str->length)
			throw std::out_of_range("Index out of range.");
		return CharProxy(this, index);
	}

	/**
	 * Get the value of the character at a given position.
	 *
	 * Does perform bounds checking.
	 *
	 * @param index The index of the character.
	 *
	 * @return The value of the character.
	 */
	CharT at(size_t index) const throw (std::out_of_range &) {
		if (!__str || index > __str->length)
			throw std::out_of_range("Index out of range.");
		return __str->viewData()[index];
	}

	/**
	 * Get a pointer to the character data of this string.
	 *
	 * The data is not modifiable since it may be shared.
	 */
	const CharT * data() const {
		return __str->viewData();
	}

	/**
	 * Get a pointer to the character data of this string.
	 *
	 * The data may be modified for as long as the storage of this object is not modified. Note
	 * that if the data was shared with any other string object it is first duplicated.
	 *
	 * Using this pointer after the storage has been modifies leads to undefined behaviour.
	 */
	CharT * edit() {
		if (!__str) return nullptr;

		// Edit automatically duplicates the data if it was shared.
		if (__str->isShared()) {
			__str = __str->dup();
		}
		return __str->editData();
	}

	/**
	 * Return a reference to the first character.
	 *
	 * Does not perform bounds checking.
	 */
	CharProxy front() {
		return operator[](0);
	}

	/**
	 * Return the value of the first character.
	 *
	 * Does not perform bounds checking.
	 */
	CharT front() const {
		return operator[](0);
	}

	/**
	 * Return a reference to the last character.
	 *
	 * Does not perform bounds checking.
	 */
	CharProxy back() {
		return operator[](length() - 1);
	}

	/**
	 * Return the value of the last character.
	 *
	 * Does not perform bounds checking.
	 */
	CharT back() const {
		return operator[](length() - 1);
	}

	/**@}*/

	/**************************************************************************
	 *
	 * Iterators
	 *
	 */

	/**
	 * @name Iterators
	 * @{
	 */

	iterator begin() {
		return iterator(*this, 0);
	}

	citerator begin() const {
		return citerator(*this, 0);
	}

	citerator cbegin() const {
		return citerator(*this, 0);
	}

	iterator end() {
		return iterator(*this, length());
	}

	citerator end() const {
		return citerator(*this, length());
	}

	citerator cend() const {
		return citerator(*this, length());
	}

	riterator rbegin() {
		return riterator(*this, 0);
	}

	criterator rbegin() const {
		return criterator(*this, 0);
	}

	criterator crbegin() const {
		return criterator(*this, 0);
	}

	riterator rend() {
		return riterator(*this, length());
	}

	criterator rend() const {
		return criterator(*this, length());
	}

	criterator crend() const {
		return criterator(*this, length());
	}

	/**@}*/

	/**
	 * Returns the number of characters on the string.
	 *
	 * There is no guarantee this number matches the actual number of rendered characters, since
	 * there are multi-character sequences up to UTF-16, and even on UTF-32 there still exist
	 * combining characters and zero-width characters.
	 */
	size_t length() const {
		return __str->length;
	}

	/**
	 * Return the maximum number of characters this string can hold without
	 * increasing it's storage size.
	 */
	size_t capacity() const {
		return (__str->size - 1);
	}

	/**
	 * @name Comparisons.
	 * @{
	 */

#define comparison_op(op, code) \
	template<typename RP>\
	bool operator op(basic_string<CharT, RP> & bs) {\
		code\
	}\
	\
	template<typename RP>\
	bool operator op(basic_string<CharT, RP> && bs) {\
		code\
	}\

	comparison_op(!=, {
				if(__str == bs.__str) return false;
				else if (!__str || !bs.__str) return true;
				return strcmp(__str->viewData(), bs.__str->viewData()) != 0;
			});

	comparison_op (==, {
				if(__str == bs.__str) return true;
				else if (!__str || !bs.__str) return false;
				return strcmp(__str->viewData(), bs.__str->viewData()) == 0;
			});

	comparison_op(>, {
				if(!__str && bs.__str) return false;
				else if(__str && !bs.__str) return true;
				return strcmp(__str->viewData(), bs.__str->viewData()) > 0;
			});

	comparison_op(<, {
				if(!__str && bs.__str) return true;
				else if(__str && !bs.__str) return false;
				return strcmp(__str->viewData(), bs.__str->viewData()) < 0;
			});

	comparison_op(>=, {
				if(!__str) {
					return bs.__str ? true : false;
				} else if(!bs.__str) return true;
				return strcmp(__str->viewData(), bs.__str->viewData()) >= 0;
			});

	comparison_op(<=, {
				if(!bs.__str) {
					return __str ? true : false;
				} else if(!__str) return true;
				return strcmp(__str->viewData(), bs.__str->viewData()) <= 0;
			});

	/**@}*/

};

/**************************************************************************************************
 *
 * Useful typedefs
 *
 */

/**
 * Octet String.
 *
 * This string should be used for byte-sized encodings (ASCII, ISO/IEC 8859-*,
 * etc), or for UTF-8.
 *
 * These encodings are all compatible with most libraries, which expect null
 * terminated strings.
 */
typedef basic_string<char> string;

/**
 * Wide String.
 *
 * This string is to be used with C wide characters (UTF-16 on Windows and
 * UTF-32 on most *nix).
 *
 * Wide strings are claimed to be simpler than UTF-8, but that is not true
 * because of Surrogate pairs, non-printable characters, etc (which mean there
 * is no easy indexed access).
 *
 * On average (for actual files in disk) UTF-16 is shown to result in bigger
 * sizes than equivalent UTF-8 (even for languages on the higher code points).
 *
 * For in-memory storage of user-printable strings, UTF-16 may be slightly more
 * efficient.
 */
typedef basic_string<wchar_t> wstring;

}

#endif
