#ifndef __OSIRIS_PRIVATE_CONST_STRING_H__
#define __OSIRIS_PRIVATE_CONST_STRING_H__ 1

namespace osiris {

/**
 * A const string is a wrapper for a constant C string literal.
 *
 * This object is immutable and has very lightweight copying since it only
 * holds a pointer and the cached string size.
 *
 * It can also be used for any string that is know to be constant for as long
 * as this object or copies of it are in use.
 */
class const_string {
private:
	const char * str;
	size_t len;
	public:

	/**
	 * Iterator for the const_string object.
	 */
	class iterator: public std::iterator<std::random_access_iterator_tag, char, ptrdiff_t, char*,
			char&> {
	public:

		iterator & operator ++() {
			if (offset < cs.len)
				offset++;
			return *this;
		}

		iterator operator++(int) {
			iterator cp(*this);

			if (offset < cs.len)
				offset++;
			return cp;
		}

		iterator & operator --() {
			if (offset > 0)
				offset--;
			return *this;
		}

		iterator operator--(int) {
			iterator cp(*this);
			if (offset > 0)
				offset--;
			return cp;
		}

		char operator *() {
			return cs.str[offset];
		}

		iterator operator +(ssize_t offt) {
			iterator cp(*this);
			cp.offset = std::min(cs.len, offset + offt);
			return cp;
		}

		iterator operator -(ssize_t offt) {
			iterator cp(*this);
			cp.offset = std::max((ssize_t) 0, (ssize_t) offset - offt);
			return cp;
		}

		iterator & operator +=(ssize_t offt) {
			offset = std::min(cs.len, offset + offt);
			return *this;
		}

		iterator & operator -=(ssize_t offt) {
			offset = std::max((ssize_t) 0, (ssize_t) offset - offt);
			return *this;
		}

		bool operator ==(const iterator it) const {
			if (it.cs.str != cs.str)
				return false;
			return it.offset == offset;
		}

		bool operator !=(const iterator it) const {
			if (it.cs.str != cs.str)
				return true;
			return !(it == *this);
		}

		bool operator >(const iterator it) const {
			return offset > it.offset;
		}
		bool operator <(const iterator it) const {
			return offset < it.offset;
		}
		bool operator >=(const iterator it) const {
			return offset >= it.offset;
		}
		bool operator <=(const iterator it) const {
			return offset <= it.offset;
		}

		ptrdiff_t operator -(const iterator it) const {
			return (ssize_t) offset - (ssize_t) it.offset;
		}

	private:
		const const_string & cs;
		size_t offset;

		iterator(const const_string & cs, size_t offset) :
				cs(cs), offset(offset) {
		}

		friend class const_string;
	};

	/**
	 * Return the length of the string.
	 */
	size_t length() const {
		return len;
	}

	char at(unsigned index) const {
		return str[index];
	}

	char operator[](unsigned index) const {
		return str[index];
	}

	const char * data() const {
		return str;
	}

	const iterator cbegin() const {
		return iterator(*this, 0L);
	}

	const iterator cend() const {
		return iterator(*this, len);
	}

	iterator begin() const {
		return iterator(*this, 0L);
	}

	iterator end() const {
		return iterator(*this, len);
	}

	const_string() :
			const_string(nullptr) {
	}

	const_string(const char * str) {
		this->str = str;
		len = str ? strlen(str) : 0;
	}

	const_string(const std::string s) {
		this->str = s.data();
		len = s.size();
	}

	bool operator ==(const const_string & cs) const {
		if (str == cs.str)
			return true;
		return strcmp(str, cs.str) == 0;
	}

	bool operator ==(const char * s) const {
		if (str == s)
			return true;
		return strcmp(str, s) == 0;
	}

	bool operator ==(const std::string & s) const {
		if (str == s.data())
			return true;
		return s == str;
	}

	bool operator !=(const const_string & cs) const {
		return strcmp(str, cs.str) != 0;
	}

	bool operator !=(const char * s) const {
		return strcmp(str, s) != 0;
	}

	bool operator !=(const std::string & s) const {
		return s != str;
	}

	std::string toString() const {
		return std::string(str);
	}

	operator const char *() const {
		return str;
	}
};

}

namespace std {

template<>
struct hash<osiris::const_string> {
	size_t operator()(osiris::const_string str) const {
		return osiris::hashString()(str);
	}
};

}

#endif
