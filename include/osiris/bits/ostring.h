#ifndef __OSIRIS_PRIVATE_OSTRING_H__
#define __OSIRIS_PRIVATE_OSTRING_H__ 1

namespace osiris {

namespace internal {

template <typename CharT>
struct ostring {
#if __SIZEOF_SIZE_T__ == 4
		size_t refs :31;
		bool constant :1;
#elif __SIZEOF_SIZE_T__ == 8
		size_t refs:63;
		bool constant: 1;
#else
#error Unsupported size_t size! To support this architecture please submit a patch.
#endif

		size_t length;
		size_t size;

		union {
			CharT data[0];
			const CharT * constData;
		};

		static ostring * fromConstant(const CharT * data) {
			ostring * os = (ostring *) malloc(sizeof(ostring));
			os->constData = data;
			os->constant = true;
			os->refs = 1;
			os->length = osiris::strlen(data);
			os->size = os->length;

			return os;
		}

		static ostring * fromData(const CharT * data) {
			size_t dataSize = osiris::strlen(data) + 1;
			size_t size = byte_size(dataSize);

			ostring * os = (ostring *) malloc(size);
			os->constant = false;
			os->refs = 1;
			os->length = dataSize - 1;
			os->size = os->length;

			memcpy(os->data, data, dataSize * sizeof(CharT));

			return os;
		}

		static ostring * fromChar(CharT character = CharT(), unsigned count = 1) {
			size_t size = byte_size((size_t) count);

			ostring * os = (ostring *) malloc(size);
			os->constant = false;
			os->refs = 1;
			os->length = count;
			os->size = count;

			osiris::memset(os->data, character, count);

			os->data[os->length] = 0;

			return os;
		}

		static ostring * withStorage(size_t storage) {
			size_t size = byte_size(storage);

			ostring * os = (ostring *) malloc(size);
			os->constant = false;
			os->refs = 1;
			os->length = 0;
			os->size = storage;

			os->data[os->length] = 0;

			return os;
		}

		/**
		 * @brief Return a pointer to the data held by the string.
		 *
		 * If the string points to constant memory then a pointer to that memory is returned.
		 *
		 * @return The data of the string (non-editable).
		 */
		const CharT * viewData() const {
			if (constant) {
				return constData;
			} else {
				return data;
			}
		}

		/**
		 * @brief Return an editable pointer to the data held by the string.
		 *
		 * If the data cannot be edited (either because it is shared or because it is constant)
		 * this function returns NULL.
		 *
		 * The caller should check the string on itself and duplicate it if necessary before
		 * calling this function.
		 *
		 * @return A pointer to the data held by this string.
		 */
		CharT * editData() {
			if (constant || refs > 1) {
				return nullptr;
			} else {
				return data;
			}
		}

		/**
		 * @brief Duplicate this string.
		 *
		 * In case this string holds a pointer to constant data the duplicate will instead contain
		 * a copy of that data.
		 *
		 * @return  A new string with the same data.
		 */
		ostring * dup() {
			ostring * cpy = (ostring*) malloc(byte_size(constant));

			if (constant) {
				memcpy((void *) cpy, (void *) this, sizeof(*this) - sizeof(const CharT *));
				memcpy((void *) cpy->data, (void *) this->viewData(), sizeof(CharT) * (size + 1));
			} else {
				memcpy((void *) cpy, (void *) this, byte_size());
			}

			// Only these fields need updating because everything else was memcpy'd
			cpy->constant = false;
			cpy->refs = 1;

			this->unref();

			return cpy;
		}

		/**
		 * @brief Resize the string.
		 *
		 * Since the string object itself holds the data it may be moved during expansion or
		 * retraction.
		 *
		 * Additionally if the data is constant a new string of the requested size is allocated
		 * containing a copy of that data. If this is the case the previous reference is
		 * unreferenced to prevent leaking memory.
		 *
		 * In any case the caller must replace it's reference to the string with the value returned.
		 *
		 * @param chars The new size for the string.
		 *
		 * @return An updated pointer to the string.
		 */
		ostring * resize(size_t chars) {
			ostring * cpy;

			if (constant || refs > 1) {
				cpy = (ostring *) malloc(byte_size(chars));
				memcpy((void *) cpy, (void *) this, sizeof(this) - sizeof(const CharT *));
				memcpy((void *) cpy->data, (void *) this->data, sizeof(CharT) * chars);

				cpy->constant = false;
				cpy->refs = 1;

				this->unref();
			} else {
				cpy = (ostring *) realloc((void *) this, byte_size(chars));
			}

			cpy->size = chars;
			if (cpy->length > cpy->size) {
				cpy->length = cpy->size;
			}

			// Ensure trailing null.
			cpy->data[cpy->length] = 0;

			return cpy;
		}

		/**
		 * Calculate the size of this string, or the size it would have where it not a pointer to a
		 * constant.
		 *
		 * @param forceNonConst Weather or not to simulate non-constness during calculation.
		 *
		 * @return The size.
		 */
		inline size_t byte_size(bool forceNonConst = false) {
			return (constant && !forceNonConst) ?
												  sizeof(ostring) :
												  sizeof(ostring) + sizeof(CharT) * (size + 1) - sizeof(const CharT *);
		}

		/**
		 * Calculate the size of a non-constant string with a given number of
		 * character storage.
		 *
		 * @param size The number of characters the string is able to hold
		 * 				(should include the null byte).
		 */
		static inline size_t byte_size(size_t size) {
			return sizeof(ostring) + sizeof(CharT) * size - sizeof(const CharT *);
		}

		/**
		 * Increase the reference count.
		 */
		inline void ref() {
			refs++;
		}

		/**
		 * Decrease the reference count.
		 *
		 * If the resulting count is bellow 0 the string is destroyed.
		 */
		inline void unref() {
			refs--;
			if (refs <= 0) {
				free(this);
			}
		}

		inline bool isShared() {
			return refs > 1 || constant;
		}
	};

}
}

#endif
