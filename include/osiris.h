/*******************************************************************************
 * osiris.h
 * Osiris - C++ foundation libraries for interactive applications.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSIRIS_H__
#define __OSIRIS_H__ 1

/**
 * @file
 *
 * Main osiris header.
 *
 * To use osiris one may include this header and any specific headers to non-core functionality.
 *
 */

#include <osiris/exception.h>
#include <osiris/string.h>
#include <osiris/poll.h>
#include <osiris/object.h>
#include <osiris/stream.h>
#include <osiris/logging.h>

/**
 * @brief Base namespace for Osiris.
 *
 * This namespace contains all types and functions for the Osiris library.
 */
namespace osiris {

/**
 * This logger is used by osiris for it's logging necessities.
 */
extern Logger osirisLogger;

/**
 * Initialise Osiris.
 *
 * Must be called prior to using osiris.
 */
void init();

}

#endif
