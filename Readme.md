# Osiris

Osiris is a set of helpful libraries for C++.

It focuses on bringing functionalities common on languages such as java
(like streams and logging) into a compact package and with nimble source code.

The main alternative to Osiris would be some of the boost libraries. Osiris is
more lightweight and not as metaprogramming focused as boost.

## Modules

Currently osiris has a few modules where it packs utilities for common C++ tasks. These are:

 - String Classes/Utilities (WIP);
 - An Event/Callback system;
 - Core Object definitions;
 - Streams;
 - Logging;
 - Networking (WIP).
